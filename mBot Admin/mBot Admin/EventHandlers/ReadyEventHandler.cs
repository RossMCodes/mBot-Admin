﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using mBot_Admin.Services.AdministrationService;
using mBot_Admin.ConfigService;

namespace mBot_Admin.EventHandlers
{
    public class ReadyEventHandler
    {
        private DiscordSocketClient client;

        public async Task Ready()
        {
            client.Ready += async () =>
            {
                await new Config().Init(client);

                await new NotificationService().OnReady(client);

                
                //await new DiscordBotsDotOrgService().PostStats(client);

            };
        }

        public async Task JoinedGuild()
        {
            client.JoinedGuild += async (SocketGuild JoinedGuild) =>
            {

                await new NotificationService().OnJoinGuild(client, JoinedGuild);

                //await new DiscordBotsDotOrgService().PostStats(client);

            };
        }

    }
}
