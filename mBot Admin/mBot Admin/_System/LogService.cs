﻿using mBot_Admin.ConfigService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin._System
{
    public class SysLog
    {
        public static string Path
            = @"BotSystem\Logs\SysLog.log";

        public static async Task Write(string msg)
        {
            File.AppendAllText(Path, msg);
        }
        public static async Task WriteLine(string msg)
        {
            File.AppendAllText(Path, msg + Environment.NewLine);
        }
    }

    public class SesLog
    {
        public static string Path()
        {
            return SessionConfigs.SessionLogPath;
        }

        public static async Task Write(string msg)
        {
            File.AppendAllText(Path(), msg);
        }
        public static async Task WriteLine(string msg)
        {
            File.AppendAllText(Path(), msg + Environment.NewLine);
        }
    }

    public class ErrLog
    {
        public static string Path
            = @"BotSystem\Logs\ErrLog.log";

        public static async Task Write(string msg)
        {
            File.AppendAllText(Path, msg);
        }
        public static async Task WriteLine(string msg)
        {
            File.AppendAllText(Path, msg + Environment.NewLine);
        }
    }
}
