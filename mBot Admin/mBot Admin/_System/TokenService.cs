﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin._System
{
    public class TokenService
    {
        public static string Token()
        {
            string token = null;
            if (File.Exists(@"BotSystem\Sys\Token.token"))
            {
                StreamReader tokenreader = new StreamReader(@"BotSystem\Sys\Token.token");
                token = tokenreader.ReadLine();
                tokenreader.Close();
                return token;
            }
            else
            {
                ErrorHandling.Rollbar.Report("Bot Token Not Found", ErrorHandling.ErrorLevel.Critical);
                ErrorHandling.Sentry.Report("Bot Token Not Found", ErrorHandling.ErrorLevel.Critical);
                return token;
            }
        }

        public static string TextMagicToken()
        {
            string token = null;
            if (File.Exists(@"BotSystem\Sys\TextMagicToken.token"))
            {
                StreamReader tokenreader = new StreamReader(@"BotSystem\Sys\TextMagicToken.token");
                token = tokenreader.ReadLine();
                tokenreader.Close();
                return token;
            }
            else
            {
                ErrorHandling.Rollbar.Report("TextMagic Token Not Found", ErrorHandling.ErrorLevel.Warning);
                ErrorHandling.Sentry.Report("TextMagic Token Not Found", ErrorHandling.ErrorLevel.Warning);
                return token;
            }
        }
    }
}
