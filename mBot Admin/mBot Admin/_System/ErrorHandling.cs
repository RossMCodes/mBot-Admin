﻿using mBot_Admin.ConfigService;
using Newtonsoft.Json;
using SharpRaven;
using SharpRaven.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin._System
{
    public class ErrorHandling
    {
        public class ErrorEvent
        {
            public ErrorEvent()
            {

            }

            public ErrorEvent(string StrEvent)
            {
                this.Event = new Exception(StrEvent);
            }
            public ErrorEvent(string StrEvent, ErrorLevel ObjLevel)
            {
                this.Event = new Exception(StrEvent);
                this.Level = ObjLevel;
            }
            public ErrorEvent(string StrEvent, ErrorLevel ObjLevel, Breadcrumb ObjBreadcrumb)
            {
                this.Event = new Exception(StrEvent);
                this.Level = ObjLevel;
                this.Breadcrumb = ObjBreadcrumb;
            }

            public ErrorEvent(Exception Exc)
            {
                this.Event = Exc;
            }
            public ErrorEvent(Exception Exc, ErrorLevel ObjLevel)
            {
                this.Event = Exc;
                this.Level = ObjLevel;
            }
            public ErrorEvent(Exception Exc, ErrorLevel ObjLevel, Breadcrumb ObjBreadcrumb)
            {
                this.Event = Exc;
                this.Level = ObjLevel;
                this.Breadcrumb = ObjBreadcrumb;
            }

            public ErrorLevel? Level;
            public Exception Event;
            public Breadcrumb Breadcrumb;
        }

        public class Rollbar
        {
            public Rollbar()
            {
                Init();
            }
            public static void Init()
            {
                var RollConfig = new RollbarDotNet.RollbarConfig
                {
                    AccessToken = new ErrorHandlerTokenStore(true).RollbarToken,
                    Environment = Environment(),
                };
                RollbarDotNet.Rollbar.Init(RollConfig);
            }

            public static void Report(ErrorEvent Event)
            {
                try
                {
                    RollbarDotNet.Rollbar.Report(Event.Event, Event.Level?.ToRollbar() ?? RollbarDotNet.ErrorLevel.Error);
                }
                catch
                {

                }
            }
            public static void Report(string msg)
            {
                try
                {
                    RollbarDotNet.Rollbar.Report(msg);
                }
                catch
                {

                }
            }
            public static void Report(string msg, ErrorLevel level)
            {
                try
                {
                    var severity = level.ToRollbar();
                    RollbarDotNet.Rollbar.Report(msg, severity);
                }
                catch
                {

                }
            }
            public static void Report(Exception e)
            {
                try
                {
                    RollbarDotNet.Rollbar.Report(e);

                }
                catch
                {

                }
            }
            public static void Report(Exception e, ErrorLevel level)
            {
                try
                {
                    var severity = level.ToRollbar();
                    RollbarDotNet.Rollbar.Report(e, severity);
                }
                catch
                {

                }
            }
        }

        public class Sentry
        {
            public static void Init()
            {
                var rClient = new RavenClient(new ErrorHandlerTokenStore(true).SentryDSN);
                rClient.Environment = Environment();
                Sentry.Client = rClient;
            }

            public static RavenClient Client;

            public static void Report(ErrorEvent Event)
            {
                var sevent = new SentryEvent(Event.Event);
                sevent.Level = Event.Level?.ToSentry() ?? SharpRaven.Data.ErrorLevel.Error;
                Client.Capture(sevent);
            }
            public static void Report(string msg)
            {
                var smsg = new SentryMessage(msg);
                var sevent = new SentryEvent(smsg);
                Client.Capture(sevent);

            }
            public static void Report(string msg, ErrorLevel level)
            {
                var severity = level.ToSentry();
                var smsg = new SentryMessage(msg, severity);
                var sevent = new SentryEvent(smsg);
                Client.Capture(sevent);

            }
            public static void Report(Exception e)
            {
                var sevent = new SentryEvent(e);
                Client.Capture(sevent);
            }
            public static void Report(Exception e, ErrorLevel level)
            {
                var severity = level.ToSentry();
                var sevent = new SentryEvent(e);
                sevent.Level = severity;
                Client.Capture(sevent);
            }
            
        }

        public enum ErrorLevel
        {
            Critical,
            Error,
            Warning,
            Info,
            Debug
        }

        public static string Environment()
        {
            if (File.Exists(@"BotSystem\Sys\Rollbar\IsProd.true"))
                return "production";
            else
                return "beta";
        }
    }

    public static class ErrorHandlingExtensions
    {
        public static SharpRaven.Data.ErrorLevel ToSentry(this ErrorHandling.ErrorLevel Level)
        {
            
            if (Level == ErrorHandling.ErrorLevel.Critical)
                return SharpRaven.Data.ErrorLevel.Fatal;
            else if (Level == ErrorHandling.ErrorLevel.Error)
                return SharpRaven.Data.ErrorLevel.Error;
            else if (Level == ErrorHandling.ErrorLevel.Warning)
                return SharpRaven.Data.ErrorLevel.Warning;
            else if (Level == ErrorHandling.ErrorLevel.Error)
                return SharpRaven.Data.ErrorLevel.Info;
            else if (Level == ErrorHandling.ErrorLevel.Debug)
                return SharpRaven.Data.ErrorLevel.Debug;
            else
                return SharpRaven.Data.ErrorLevel.Error;
        }
        public static RollbarDotNet.ErrorLevel ToRollbar(this ErrorHandling.ErrorLevel Level)
        {
            if (Level == ErrorHandling.ErrorLevel.Critical)
                return RollbarDotNet.ErrorLevel.Critical;
            else if (Level == ErrorHandling.ErrorLevel.Error)
                return RollbarDotNet.ErrorLevel.Error;
            else if (Level == ErrorHandling.ErrorLevel.Warning)
                return RollbarDotNet.ErrorLevel.Warning;
            else if (Level == ErrorHandling.ErrorLevel.Error)
                return RollbarDotNet.ErrorLevel.Info;
            else if (Level == ErrorHandling.ErrorLevel.Debug)
                return RollbarDotNet.ErrorLevel.Debug;
            else
                return RollbarDotNet.ErrorLevel.Error;
        }

        public static void Report(this ErrorHandling.ErrorEvent Event)
        {
            ErrorHandling.Rollbar.Report(Event);
            ErrorHandling.Sentry.Report(Event);
        }
        public static void RollbarReport(this ErrorHandling.ErrorEvent Event)
        {
            ErrorHandling.Rollbar.Report(Event);
        }
        public static void SentryReport(this ErrorHandling.ErrorEvent Event)
        {
            ErrorHandling.Sentry.Report(Event);
        }
    }

    public class ErrorHandlerTokenStore
    {
        public ErrorHandlerTokenStore(bool Load = false)
        {
            if (Load)
            {
                if (!File.Exists(PathStrings.Authentication.ErrorHandlerTokenStore))
                {
                    var tstore = new ErrorHandlerTokenStore(false);
                    File.WriteAllText(PathStrings.Authentication.ErrorHandlerTokenStore, JsonConvert.SerializeObject(tstore));
                    throw new System.Exception("Error Handling Token Store Not Found, Blank One Created");
                }
                string json = File.ReadAllText(PathStrings.Authentication.ErrorHandlerTokenStore);
                var store = JsonConvert.DeserializeObject<ErrorHandlerTokenStore>(json);
                this.RollbarToken = store.RollbarToken;
                this.SentryDSN = store.SentryDSN;
            }
            else
            {
                this.RollbarToken = null;
                this.SentryDSN = null;
            }
        }

        public string RollbarToken;
        public string SentryDSN;
    }
}
