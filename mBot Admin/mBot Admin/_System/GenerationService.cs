﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin._System
{
    public class GenerationService
    {
        public static string BasicAlphaNumericCharSet
            = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        public static string GenRandomString(int chars, string charset = null)
        {
            charset = charset ?? BasicAlphaNumericCharSet;
            Random rand = new Random();
            string str = new string(Enumerable.Repeat(charset, chars)
                  .Select(s => s[rand.Next(s.Length)]).ToArray());
            return str;
        }
    }
}
