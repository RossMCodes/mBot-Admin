﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin._System.ExtensionMethods
{
    public static class List_String_Extension
    {
        /// <summary>
        /// Turns a List<string> into a string with all entries seperated by a provided seperator.
        /// </summary>
        /// <param name="List">The list to extract data from</param>
        /// <param name="seperator">A char seperator. '\n' works, and is the default</param>
        /// <returns>A string representation of the list, seperated by the provided seperator.</returns>
        public static string JoinAll(this List<string> List, char seperator = '\n')
        {
            StringBuilder builder = new StringBuilder();
            foreach(string s in List)
            {
                builder.Append(seperator + s);
            }
            string rtn = builder.ToString();
            rtn = rtn.Trim();
            rtn = rtn.Trim(seperator);
            rtn = rtn.Trim();
            return rtn;
        }
    }
}
