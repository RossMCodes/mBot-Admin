﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace mBot_Admin.ExtensionMethods
{
    public static class UriExtension
    {
        public static Uri AttachParameters(this Uri uri, Dictionary<string, string> parameters)
        {
            // https://stackoverflow.com/a/43452635/7484229
            var stringBuilder = new StringBuilder();
            string str = "?";

            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                string key = HttpUtility.UrlEncode(parameter.Key);
                string value = HttpUtility.UrlEncode(parameter.Value);
                stringBuilder.Append(str + key + "=" + value);
                str = "&";
            }
            return new Uri(uri + stringBuilder.ToString());
        }
    }
}
