﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.ExtensionMethods
{
    public static class StringExtension
    {
        public static string ToAlphanumeric(this string input)
        {
            string str = "";
            string allowedChars = ""
                + "abcdefghijklmnopqrstuvwxyz" 
                + "ABCDEFGHIJKLMNOPQRSTUVWXYZ" 
                + "1234567890"
                ;
            foreach (char c in input)
            {
                if (allowedChars.Contains(c))
                {
                    str = str + c;
                }
            }
            return str;
        }

        public static string GetPrefix(this string input, int NumberOfChars)
        {
            IEnumerable<char> remainingChars = input.Take(NumberOfChars);
            string str = "";
            foreach(char c in remainingChars)
            {
                str = str + c;
            }
            return str;
        }
    }
}
