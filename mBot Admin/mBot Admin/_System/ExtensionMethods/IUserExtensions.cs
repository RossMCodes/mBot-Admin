﻿using Discord;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static mBot_Admin.ConfigService.Config;

namespace mBot_Admin._System.ExtensionMethods
{
    public static class IUserExtensions
    {
        /// <summary>
        /// Returns true if the user is a Bot Owner
        /// </summary>
        /// <param name="user">The user to scan</param>
        /// <returns>True if user is an Owner</returns>
        public static bool IsOwner(this IUser user)
        {
            if (Owners.IsOwner(user))
                return true;
            else
                return false;
        }
    }
}
