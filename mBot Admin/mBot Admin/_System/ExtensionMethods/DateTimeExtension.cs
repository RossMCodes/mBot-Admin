﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin._System.ExtensionMethods
{
    public static class DateTimeExtension
    {
        public static string ToISO(this DateTime DT)
        {
            return DT.ToString("yyyy-MM-dd HH:mm.ss");
        }
        public static string ToFileNameFormat(this DateTime DT)
        {
            return DT.ToString("yyyy-MM-dd_HH-mm-ss");
        }
    }
}
