﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.ModerationObjects
{
    public class LogObject
    {
        public CaseType CaseType;
        public ulong GuildId;
        public ulong SysId;
        public int CaseNo;
        public string User;
        public ulong UserId;
        public string UserAvatarUrl;
        public string Mod;
        public ulong ModId;
        public string Reason;
        public ulong MessageId;
        public ulong ModChanId;
    }

    public static class LogObjectExtensions
    {
        public static void Save(this LogObject Log)
        {
            Directory.CreateDirectory($@"BotSystem\Data\Mod\{Log.GuildId}");
            Directory.CreateDirectory($@"BotSystem\Data\Mod\{Log.GuildId}");
            Directory.CreateDirectory($@"BotSystem\Data\Mod\_All\{Log.SysId}");
            string json = JsonConvert.SerializeObject(Log);
            File.WriteAllText($@"BotSystem\Data\Mod\{Log.GuildId}\{Log.CaseNo}.json", json);
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{Log.SysId}\data.json", json);
        }

        public static LogObject Load(this LogObject Log, ulong SysId)
        {
            string json = File.ReadAllText($@"BotSystem\Data\Mod\_All\{SysId}\data.json");
            Log = JsonConvert.DeserializeObject<LogObject>(json);
            return Log;
        }
    }

    public enum CaseType
    {
        Prune,
        Warn,
        Kick,
        Ban,
        SetModChannel,
    }
}
