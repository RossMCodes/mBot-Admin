﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.SearchObjects
{
    public class UrbanObjects
    {
        public class List
        {
            public string definition { get; set; }
            public string permalink { get; set; }
            public int thumbs_up { get; set; }
            public string author { get; set; }
            public string word { get; set; }
            public int defid { get; set; }
            public string current_vote { get; set; }
            public string example { get; set; }
            public int thumbs_down { get; set; }
        }

        public class RootObject
        {
            public List<string> tags { get; set; }
            public string result_type { get; set; }
            public List<List> list { get; set; }
            public List<string> sounds { get; set; }
        }
    }
}
