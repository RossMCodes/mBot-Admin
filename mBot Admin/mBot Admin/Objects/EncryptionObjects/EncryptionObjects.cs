﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.EncryptionObjects
{
    public class AesObjects
    {
        public class Config
        {
            public Config()
            {

            }
            public Config(Aes aes)
            {
                this.Key = Convert.ToBase64String(aes.Key);
                this.IV = Convert.ToBase64String(aes.IV);
            }
            public string Key;
            public string IV;
            public Aes CreateAes()
            {
                Aes aes = Aes.Create();
                aes.Key = Convert.FromBase64String(this.Key);
                aes.IV = Convert.FromBase64String(this.IV);
                return aes;
            }
        }
    }
}
