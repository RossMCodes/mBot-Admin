﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.FanFictionObjects
{
    public class FanFictionDotNetObjects
    {
        public class FanFicObject
        {
            public string Title;
            public AuthorObject Author;
            public string Url;

            public string Rating;
            public string Language;
            public string Genre;
            public string Pairings;
            public int Chapters;
            public Int64 Words;
            public int Reviews;
            public int Favs;
            public int Follows;
            public /*DateTime*/ string Updated;
            public /*DateTime*/ string Published;
            public int Id;

            public string Summary;
        }

        public class AuthorObject
        {
            public string Author;
            public string AuthorUrl;
        }
    }
}
