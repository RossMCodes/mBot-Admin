﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mBot_Admin.Services.FanFictionService;
using HtmlAgilityPack;
using mBot_Admin.Objects.DateTimeObjects;

namespace mBot_Admin.Objects.FanFictionObjects
{
    public class AO3Objects
    {
        public class FanFicObject
        {
            public FanFicObject(AO3Service.GetNodes.NodeContainer NodeContainer)
            {
                this.Title = Parse(NodeContainer.Title, "Title Not Found");
                this.AuthorName = Parse(NodeContainer.Author, "Author Not Found");
                this.AuthorUrl = "http://archiveofourown.org" + ParseHref(NodeContainer.Author, "/users/ Author URL Not Found");
                this.Summary = ParseList(NodeContainer.Summary, "Summary Not Found");
                this.Notes = ParseList(NodeContainer.Notes, "Notes Not Found");
                this.Url = NodeContainer.Url;
                this.Id = NodeContainer.Id;

                this.Rating = Parse(NodeContainer.Rating, "Rating Not Found");
                this.Warnings = ParseList(NodeContainer.Warnings, "Warnings Not Found");
                this.Categories = ParseList(NodeContainer.Categories, "Categories Not Found");
                this.Fandoms = ParseList(NodeContainer.Fandoms, "Fandoms Not Found");
                this.Relationships = ParseList(NodeContainer.Relationships, "Relationships Not Found");
                this.Characters = ParseList(NodeContainer.Characters, "Characters Not Found");
                this.AdditionalTags = ParseList(NodeContainer.AdditionalTags, "No Additional Tags Found");
                this.Language = Parse(NodeContainer.Language, "Language Not Found");
                this.Series = Parse(NodeContainer.Series, "Series Not Found");

                this.StatsDict = ParseDict(NodeContainer.Stats.StatsDict);
                this.Stats = new StatsObject(this.StatsDict);
            }

            public string Parse(HtmlNodeCollection Nodes, string ErrorMessage)
            {
                try
                {
                    return Nodes.FirstOrDefault().InnerText ?? ErrorMessage;
                }
                catch
                {
                    return ErrorMessage;
                }
            }

            public string ParseHref(HtmlNodeCollection Nodes, string ErrorMessage)
            {
                try
                {
                    return Nodes.FirstOrDefault().Attributes["href"].Value ?? ErrorMessage;
                }
                catch
                {
                    return ErrorMessage;
                }
            }

            public List<string> ParseList(HtmlNodeCollection Nodes, string ErrorMessage)
            {
                List<string> List = new List<string>();
                try
                {
                    
                    foreach (HtmlNode n in Nodes)
                    {
                        string tmp = n.InnerText.Replace("&amp;", "&").Trim();
                        List.Add(tmp);
                    }
                    if (List.Count == 0)
                        List.Add(ErrorMessage);
                    return List;
                }
                catch
                {
                    List.Add(ErrorMessage);
                    return List;
                }
                
            }

            public Dictionary<string, string> ParseDict(Dictionary<HtmlNode, HtmlNode> NodesDict)
            {
                Dictionary<string, string> Dict = new Dictionary<string, string>();
                foreach (KeyValuePair<HtmlNode, HtmlNode> kvp in NodesDict)
                {
                    Dict.Add(kvp.Key.InnerText, kvp.Value.InnerText);
                }
                return Dict;
            }

            public string Title;
            public string AuthorName;
            public string AuthorUrl;
            public List<string> Summary;
            public List<string> Notes;
            public string Url;
            public int Id;


            public string Rating;
            public List<string> Warnings;
            public List<string> Categories;
            public List<string> Fandoms;
            public List<string> Relationships;
            public List<string> Characters;
            public List<string> AdditionalTags;
            public string Language;
            public string Series;
            public StatsObject Stats;

            public Dictionary<string, string> StatsDict;

        }

        public class StatsObject
        {
            public StatsObject(Dictionary<string, string> StatsDictIn)
            {   
                this.StatsDict = StatsDictIn;
                this.Published = new AO3DateObject(TryGet("Published:", StatsDictIn: StatsDictIn));
                this.Updated = new AO3DateObject(TryGet("Updated:", "Error fetching Updated date", StatsDictIn: StatsDictIn));
                this.Words = TryGet("Words:", StatsDictIn: StatsDictIn);
                this.Chapters = TryGet("Chapters:", "0", StatsDictIn: StatsDictIn);
                this.Kudos = TryGet("Kudos:", StatsDictIn: StatsDictIn);
                this.Bookmarks = TryGet("Bookmarks:", StatsDictIn: StatsDictIn);
                this.Hits = TryGet("Hits:", StatsDictIn: StatsDictIn);
            }
            public string TryGet(string Key, string ErrorMsgIn = null, bool ThrowOnError = false, Dictionary<string, string> StatsDictIn = null)
            {
                var Dict = StatsDictIn ?? StatsDict;
                if (Dict == null)
                    throw new Exception($"Error: StatsDict is null");
                string tmp = null;
                bool found = Dict.TryGetValue(Key, out tmp);
                string ErrorMsg = ErrorMsgIn ?? $"An Unknown Error occurred while trying to extract the following data: \"`{Key}`\"";
                if (!found && ThrowOnError)
                    throw new Exception(ErrorMsg);
                return (tmp ?? ErrorMsg).Trim();
            }
            public AO3DateObject Published;
            public AO3DateObject Updated;
            public string Words;
            public string Chapters;
            public string Kudos;
            public string Bookmarks;
            public string Hits;
            public Dictionary<string, string> StatsDict;
        }

        public class AO3DateObject
        {
            public AO3DateObject(string IsoOrError)
            {
                //this.Msg = IsoOrError;
                this.DateObject = null;
                try
                {
                    this.DateObject = new DateObject(IsoOrError);
                    this.Msg = $"{DateObject.Day.Day} {DateObject.Month.MonthOfYear}, {DateObject.Year}";
                }
                catch
                {
                    Msg = IsoOrError;
                }
            }
            public DateObject DateObject;
            public string Msg;
        }
    }
}
