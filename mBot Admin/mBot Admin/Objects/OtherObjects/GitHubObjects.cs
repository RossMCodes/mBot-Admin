﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using mBot_Admin.ConfigService;
using mBot_Admin.Objects.ConfigObjects;

namespace mBot_Admin.Objects.OtherObjects.GitHubObjects
{
    class GitHubObjects
    {
    }

    public class GistObjects
    {
        public class OutBoundGistObjects
        {
            public class File
            {
                public string content { get; set; }
            }
            public class RootObject
            {
                //Constructors
                public RootObject()
                {

                }
                public RootObject(GistCreationObjects.RootObject GistCreationObject)
                {
                    var Files = new Dictionary<string, OutBoundGistObjects.File>();
                    foreach (GistCreationObjects.File f in GistCreationObject.Files)
                    {
                        OutBoundGistObjects.File file = new OutBoundGistObjects.File
                        {
                            content = f.FileContent
                        };
                        Files.Add(f.FileName, file);
                    }
                    this.files = Files;
                    this.description = GistCreationObject.Description;
                    this.@public = GistCreationObject.IsPublic;
                }

                //Contents
                public string description { get; set; }
                public bool @public { get; set; }
                public Dictionary<string, File> files { get; set; }
                //List of files, <string filename, File{string content}>
            }
        }

        public class InBoundGistObjects
        {
            public class Owner
            {
                public string login { get; set; }
                public int id { get; set; }
                public string avatar_url { get; set; }
                public string gravatar_id { get; set; }
                public string url { get; set; }
                public string html_url { get; set; }
                public string followers_url { get; set; }
                public string following_url { get; set; }
                public string gists_url { get; set; }
                public string starred_url { get; set; }
                public string subscriptions_url { get; set; }
                public string organizations_url { get; set; }
                public string repos_url { get; set; }
                public string events_url { get; set; }
                public string received_events_url { get; set; }
                public string type { get; set; }
                public bool site_admin { get; set; }
            }

            public class File
            {
                public int size { get; set; }
                public string raw_url { get; set; }
                public string type { get; set; }
                public bool truncated { get; set; }
                public string language { get; set; }
            }

            public class Files
            {
                public Dictionary<string, File> files { get; set; }
                //Dictionary<string fname, File file>;
            }

            public class RootObject
            {
                public string url { get; set; }
                public string forks_url { get; set; }
                public string commits_url { get; set; }
                public string id { get; set; }
                public string description { get; set; }
                public bool @public { get; set; }
                public Owner owner { get; set; }
                public object user { get; set; }
                public Files files { get; set; }
                public bool truncated { get; set; }
                public int comments { get; set; }
                public string comments_url { get; set; }
                public string html_url { get; set; }
                public string git_pull_url { get; set; }
                public string git_push_url { get; set; }
                public DateTime created_at { get; set; }
                public DateTime updated_at { get; set; }
            }
        }

        public class GistCreationObjects
        {
            public class File
            {
                public string FileName { get; set; }
                public string FileContent { get; set; }
            }
            public class RootObject
            {
                public List<File> Files { get; set; }
                public string Description { get; set; }
                public bool IsPublic { get; set; }
            }
        }
    }

    public class GitHubCredentialsObject
    {
        public GitHubCredentialsObject(bool blank = true)
        {
            if (blank == false)
            {
                if (!File.Exists(PathStrings.Authentication.GitHubCredentials))
                {
                    File.WriteAllText(PathStrings.Authentication.GitHubCredentials, JsonConvert.SerializeObject(new GitHubAuth()));
                    throw new System.Exception("GitHub Authentication File Missing Or Blank!");
                }
                    
                var jsonin = File.ReadAllText(PathStrings.Authentication.GitHubCredentials);
                var CredsIn = JsonConvert.DeserializeObject<GitHubAuth>(jsonin);
                this.Username = CredsIn.Username;
                this.PasswordOrAccessToken = CredsIn.PasswordOrAccessToken;
                this.AuthString = Username + ":" + PasswordOrAccessToken;
            }
        }
        public string Username { get; set; }
        public string PasswordOrAccessToken { get; set; }
        public string AuthString { get; set; }
    }
}
