﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mBot_Admin.ConfigService;

namespace mBot_Admin.Objects.OtherObjects.BotListObjects
{
    public class BotListObjects
    {
        public class TokenStore
        {
            public TokenStore(bool blank = true)
            {
                if (blank == false)
                {
                    if (!File.Exists(PathStrings.Authentication.BotListsTokenStore))
                    {
                        var tjson = JsonConvert.SerializeObject(new TokenStore());
                        File.WriteAllText(PathStrings.Authentication.BotListsTokenStore, tjson);
                        throw new System.Exception("Bot List Token Store is blank or missing!");
                    }
                    var json = File.ReadAllText(PathStrings.Authentication.BotListsTokenStore);
                    var obj = JsonConvert.DeserializeObject<TokenStore>(json);
                    this.DiscordBotsDotOrgToken = obj.DiscordBotsDotOrgToken;
                }
            }

            public string DiscordBotsDotOrgToken { get; set; }
        }
    }
}
