﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.MemeObjects
{
    public class MemeObjects
    {
        public class Owner
        {
            public ulong Id { get; set; }
            public string Username { get; set; }
            public int Discrim { get; set; }
        }

        public class Meme
        {
            public Owner Owner { get; set; }
            public string Filename { get; set; }
            public int FileID { get; set; }
            public string Path { get; set; }
            public int RandomAccessedCount { get; set; }
            public int RequestedAccessCount { get; set; }
        }

        public class RootObject
        {
            public int count { get; set; }
            public List<Meme> memes { get; set; }
            public List<Meme> pendingMemes { get; set; }
            public List<Meme> pendingAdminMemes { get; set; }
            public int lastId { get; set; }
            public int lastAdminId { get; set; }
        }
    }
}
