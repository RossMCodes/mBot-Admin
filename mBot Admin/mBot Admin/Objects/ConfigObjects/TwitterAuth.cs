﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace mBot_Admin.Objects.ConfigObjects
{
    public class TwitterAuth
    {
        public enum AuthAccount
        {
            mBot,
            NNA,
        }
        public TwitterAuth(AuthAccount acct)
        {
            string json = string.Empty;
            StreamReader reader = new StreamReader(@"BotSystem\Sys\twitter.auth.json");
            json = reader.ReadToEnd();
            RootObject root = JsonConvert.DeserializeObject<RootObject>(json);
            TwitterAuthTemplate auth = SelectTwitterAuthAcct(root, acct);
            
            this.ConsumerKey = auth.ConsumerKey;
            this.ConsumerSecret = auth.ConsumerSecret;
            this.userAccessSecret = auth.userAccessSecret;
            this.userAccessToken = auth.userAccessToken;
            reader.Close();
            reader.Dispose();
            
        }
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string userAccessToken { get; set; }
        public string userAccessSecret { get; set; }

        TwitterAuthTemplate SelectTwitterAuthAcct(RootObject root, AuthAccount acct)
        {
            switch (acct)
            {
                case AuthAccount.mBot:
                    return root.mBot;
                case AuthAccount.NNA:
                    return root.NNA;

                default:
                    return root.mBot;
            }
        }
    }

    public class TwitterAuthTemplate
    {
        public string ConsumerKey { get; set; }
        public string ConsumerSecret { get; set; }
        public string userAccessToken { get; set; }
        public string userAccessSecret { get; set; }
    }

    public class RootObject
    {
        public TwitterAuthTemplate NNA { get; set; }
        public TwitterAuthTemplate mBot { get; set; }
    }

    
}


/*
{
"NNA":{
  "ConsumerKey": "",
  "ConsumerSecret": "",
  "userAccessToken": "",
  "userAccessSecret": ""
},
"mBot":{
  "ConsumerKey": "",
  "ConsumerSecret": "",
  "userAccessToken": "",
  "userAccessSecret": ""
}
}
*/
