﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.ConfigObjects
{
    public class GitHubAuth
    {
        public string Username { get; set; }
        public string PasswordOrAccessToken { get; set; }
    }
}
