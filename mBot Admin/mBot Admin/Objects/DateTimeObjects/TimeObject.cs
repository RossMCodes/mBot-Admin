﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.DateTimeObjects
{
    public class TimeObject
    {
        public TimeObject(DateTime DT, bool InclTS = true, bool InclDT = false)
        {
            TimeSpan TS = DT.TimeOfDay;

            if (InclDT)
                this.DateTime = DT;
            else
                this.DateTime = null;

            if (InclTS)
                this.TimeOfDay = TS;
            else
                this.TimeOfDay = null;



            this.Hour12 = Convert.ToInt32(DT.ToString("hh"));
            this.Hour24 = DT.Hour;
            this.AMPM = new AMPMObject(DT);

            this.Minute = DT.Minute;

            this.LongTime = DT.ToLongTimeString();
            this.ShortTime = DT.ToShortTimeString();
        }
        public TimeObject(TimeSpan TS, bool InclTS = true, bool InclDT = false)
        {
            DateTime DT = new System.DateTime(0, 0, 0, TS.Hours, TS.Minutes, TS.Seconds);

            if (InclDT)
                this.DateTime = DT;
            else
                this.DateTime = null;

            if (InclTS)
                this.TimeOfDay = TS;
            else
                this.TimeOfDay = null;

            

            this.Hour12 = Convert.ToInt32(DT.ToString("hh"));
            this.Hour24 = DT.Hour;
            this.AMPM = new AMPMObject(DT);

            this.Minute = DT.Minute;

            this.LongTime = DT.ToLongTimeString();
            this.ShortTime = DT.ToShortTimeString();
        }

        public DateTime? DateTime;
        public TimeSpan? TimeOfDay;

        public int Hour12;
        public int Hour24;
        public AMPMObject AMPM;

        public int Minute;

        public string LongTime;
        public string ShortTime;

        
    }

    public class AMPMObject
    {
        public AMPMObject(DateTime DT)
        {
            this.Enum = CreateAMPMEnum(DT.ToString("t"));
            this.ShortString = DT.ToString("t");
            this.LongString = DT.ToString("tt");
        }

        public string ShortString;
        public string LongString;
        public AMPMEnum Enum;

        public AMPMEnum CreateAMPMEnum(String str)
        {
            switch (str)
            {
                case "A":
                    return AMPMEnum.AM;
                case "AM":
                    return AMPMEnum.AM;
                case "P":
                    return AMPMEnum.PM;
                case "PM":
                    return AMPMEnum.PM;
                default:
                    return AMPMEnum.UnknownOrError;
            }

        }
        public enum AMPMEnum
        {
            AM,
            PM,
            UnknownOrError
        }
    }
}
