﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.DateTimeObjects
{
    public class DateTimeObject
    {
        public DateTimeObject(DateTime DT, DateTimeObjectInclusions InclIn = null)
        {
            DateTimeObjectInclusions Incl = InclIn ?? new DateTimeObjectInclusions();
            if (Incl.DT_Root)
                this.DateTime = DT;
            else
                this.DateTime = null;

            if (Incl.TS_Root)
                this.TimeSpan = DT.TimeOfDay;
            else
                this.TimeSpan = null;

            this.Date = new DateObject(DT, Incl.DT_Date);
            this.Time = new TimeObject(DT, Incl.TS_Time, Incl.DT_Time);
        }

        public DateTime? DateTime;
        public TimeSpan? TimeSpan;
        public DateObject Date;
        public TimeObject Time;
    }

    public class DateTimeObjectInclusions
    {
        public DateTimeObjectInclusions()
        {
            this.TS_Root = true;
            this.TS_Time = false;
            this.DT_Root = true;
            this.DT_Date = false;
            this.DT_Time = false;
        }
        public bool TS_Root;
        public bool TS_Time;
        public bool DT_Root;
        public bool DT_Date;
        public bool DT_Time;
    }
}
