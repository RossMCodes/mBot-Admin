﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Objects.DateTimeObjects
{
    public class DateObject
    {
        public DateObject(string iso, bool InclDT = false)
        {
            this.Iso = iso;
            string[] split = iso.Split('-');
            int[] i = Array.ConvertAll(split, p => Convert.ToInt32(p));
            DateTime DT = new System.DateTime(i[0], i[1], i[2]);

            if (InclDT)
                this.DateTime = DT;
            else
                this.DateTime = null;

            this.DateTime = DT;

            this.Day = new DayObject(DT);
            this.Month = new MonthObject(DT);
            this.Year = DT.Year;

            this.LongDate = DT.ToLongDateString();
            this.ShortDate = DT.ToShortDateString();
        }
        public DateObject(DateTime DT, bool InclDT = false)
        {
            if (InclDT)
                this.DateTime = DT;
            else
                this.DateTime = null;

            this.Day = new DayObject(DT);
            this.Month = new MonthObject(DT);
            this.Year = DT.Year;

            this.LongDate = DT.ToLongDateString();
            this.ShortDate = DT.ToShortDateString();
        }

        public DateTime? DateTime;
        public string Iso;

        public DayObject Day;
        public MonthObject Month;
        public int Year;

        public string LongDate;
        public string ShortDate;
    }

    public class DayOfYearObject
    {
        public DayOfYearObject(DateTime DT, bool InclDT = true)
        {
            if (InclDT)
                this.DateTime = DT;
            else
                this.DateTime = null;

            this.Day = new DayObject(DT);
            this.Month = new MonthObject(DT);
        }

        public DateTime? DateTime;

        public DayObject Day;
        public MonthObject Month;
    }

    public class DayObject
    {
        public DayObject(DateTime DT, bool InclDT = false)
        {
            this.Day = DT.Day;
            this.DayOfWeek = DT.DayOfWeek.ToString();
            this.DayOfYear = DT.DayOfYear;

            if (InclDT)
                this.DateTime = DT;
            else
                this.DateTime = null;
        }

        public DateTime? DateTime;

        public int Day;
        public string DayOfWeek;
        public int DayOfYear;
    }
    public class MonthObject
    {
        public MonthObject(DateTime DT, bool InclDT = false)
        {
            this.Month = DT.Month;
            this.MonthOfYear = DT.ToString("MMMM");

            if (InclDT)
                this.DateTime = DT;
            else
                this.DateTime = null;
        }

        public DateTime? DateTime;

        public int Month;
        public string MonthOfYear;
    }

    
}
