﻿using Discord;
using mBot_Admin._System.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.ConfigService
{
    public static class PathStrings
    {
        public static string ConfigJson { get; }
                = @"BotSystem\Sys\config.json";

        public static string APITokens { get; }
                = @"BotSystem\API\tokens.protected.json";
        public static string APIMasterToken { get; }
                = @"BotSystem\API\APIMasterToken.token";

        public static string LoginConfigs { get; }
                = @"BotSystem\Data\LoginConfigs.protected.json";

        public static string MobileAddressBook { get; }
                = @"BotSystem\Data\MobileAddressBook.protected.json";

        public static string DMEveryoneConfig { get; }
                = @"BotSystem\Data\MobileAddressBook.protected.json";

        public static class Authentication
        {
            public static string GitHubCredentials { get; set; } 
                = @"BotSystem\Sys\github.auth.json";

            public static string BotListsTokenStore { get; set; }
                = @"BotSystem\Sys\BotLists.tokenstore.json";

            public static string ErrorHandlerTokenStore { get; set; }
                = @"BotSystem\Sys\ErrorHandlers.tokenstore.json";
        }

        public static class Memes
        {
            public static string ConfigPath { get; }
                = @"BotSystem\Data\Memes\memes.json";
        }

        public class UserConfigs
        {
            public string OwOUserConfig(ulong userId)
            {
                return $@"BotSystem\Intergrations\OwO\Users\{userId}.OwOConfig";
            }
            public string OwOUserLogs(ulong userId)
            {
                return $@"BotSystem\Intergrations\OwO\Logs\{userId}.log";
            }
            public string RLMEUserConfig(ulong userId)
            {
                return $@"BotSystem\Intergrations\RLME\Users\{userId}.RLMEConfig";
            }
            public string RLMEUserLogs(ulong userId)
            {
                return $@"BotSystem\Intergrations\RLME\Logs\{userId}.log";
            }
        }
        public class RLME
        {
            public static string RLMainConf { get; }
                = @"BotSystem\Intergrations\RLConf.json";

        }
    }
}
