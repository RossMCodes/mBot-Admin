﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mBot_Admin.Objects.OtherObjects.BotListObjects;

namespace mBot_Admin.ConfigService
{
    public class CredentialStrings
    {
        public string DiscordBotsDotOrgToken()
        {
            var obj = new BotListObjects.TokenStore(false);
            return obj.DiscordBotsDotOrgToken;
        }
    }
}
