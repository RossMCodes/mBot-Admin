﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.ConfigService
{
    public class Config
    {
        public async Task Init(DiscordSocketClient client)
        {
            await ReloadApplication(client);
            Owners = new Objects.Owners(Application.Owner);
            SessionInfo = new Objects.SessionInfo();
            Client = client;
        }

        public static IApplication Application;
        public async Task<IApplication> ReloadApplication(DiscordSocketClient client)
        {
            var application = await client.GetApplicationInfoAsync();
            Application = application;
            return application;
        }

        public static Objects.Owners Owners;
        public static Objects.SessionInfo SessionInfo;

        public static DiscordSocketClient Client;

        public class Objects
        {
            public class Owners
            {
                public Owners()
                {
                    Init();
                    AddOwner(Config.Application.Owner);
                }
                public Owners(IUser AppOwner)
                {
                    Init();
                    AddOwner(AppOwner);
                }

                public void Init()
                {
                    List = new List<IUser>();
                }

                public void AddOwner(IUser User)
                {
                    List.Add(User);
                }
                public void AddOwner(IUser[] Users)
                {
                    foreach (IUser u in Users)
                    {
                        List.Add(u);
                    }
                }
                public void AddOwner(DiscordSocketClient client, ulong UsrId)
                {
                    AddOwner(client.GetUser(UsrId) as IUser);
                }
                public void AddOwner(DiscordSocketClient client, ulong[] UsrId)
                {
                    foreach (ulong l in UsrId)
                    {
                        AddOwner(client.GetUser(l) as IUser);
                    }
                }

                public List<IUser> List;
                public bool IsOwner(ulong UserId)
                {
                    foreach (IUser u in List)
                    {
                        if (u.Id == UserId)
                        {
                            return true;
                        }
                    }
                    return false;
                }
                public bool IsOwner(IUser User)
                {
                    foreach (IUser u in List)
                    {
                        if (u.Id == User.Id)
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
            public class SessionInfo
            {
                public SessionInfo()
                {
                    DateTime dt = DateTime.Now;
                    DateTime = dt;
                    DateTimeSortable = dt.ToString("yyyy-MM-dd t HH-mm-ss tt");
                }
                public string DateTimeSortable;
                public DateTime DateTime;
            }
        }
        
    }
}
