﻿using mBot_Admin._System.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.ConfigService
{
    public class SessionConfigs
    {
        public static void Init()
        {
            SessionConfigs.RunDateTime = DateTime.Now;
            SessionConfigs.SessionDir = $@"BotSystem\Session\{SessionConfigs.RunDateTime.ToFileNameFormat()}";
            SessionConfigs.SessionLogPath = $@"BotSystem\Logs\Session\{SessionConfigs.RunDateTime.ToFileNameFormat()}.session.log";
        }
        public static DateTime RunDateTime;
        public static string SessionDir;
        public static string SessionLogPath;
    }
}
