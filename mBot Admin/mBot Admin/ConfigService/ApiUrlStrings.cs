﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.ConfigService
{
    public static class ApiUrlStrings
    {
        public static class BotLists
        {
            public static string DiscordBotsDotOrg { get; set; }
                = "https://discordbots.org/api/bots/284196779044503553/stats";
        }

        public class Intergrations
        {
            public static class OwO
            {
                public static string UploadFileEndpoint { get; set; }
                    = "https://api.awau.moe/upload/pomf";
                public static string ShortenUrlEndpoint { get; set; }
                    = "https://api.awau.moe/shorten/polr";
            }

            public static class RLME
            {
                public static string UploadFileEndpoint { get; set; }
                    = "https://api.ratelimited.me/api/upload/pomf";
                public static string UploadFileAlphaEndpoint { get; set; }
                    = "https://ratelimited.me/testingarea/php-api/upload/pomf.php";
                //Asa said use the Alpha Endpint until the official API is released, but I'm putting a failover just in case

            }
        }

        
    }
}
