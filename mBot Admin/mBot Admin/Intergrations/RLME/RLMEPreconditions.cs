﻿using Discord;
using Discord.Commands;
using mBot_Admin.ConfigService;
using mBot_Admin.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Intergrations.RLME
{
    public class RequireRLMEAccount : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (File.Exists(new PathStrings.UserConfigs().RLMEUserConfig(Context.User.Id)))
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: You have not configured an RLME Account! Get started with `/rlme set token [YOUR_TOKEN_HERE]`." + "\n"
                    + "Don't worry! Your token is stored encrypted using a different key and salt for every user!" + "\n"
                    + "Note: Tokens can only be set in DMs for security purposes.");
            }

        }
    }

    public class RequireRLMEStaff : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (RLMEConfig.StaticSav.StaffIds.Contains(Context.User.Id))
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: You are not registered as a member of the RLME Staff within mBot.");
            }

        }
    }

    public class RequireRLMELT : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if ((Context.User as IGuildUser).RoleIds.Contains(RLMEConfig.StaticSav.LtRoleId))
            {
                return PreconditionResult.FromSuccess();
            }
            if (Config.Owners.IsOwner(Context.User))
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: You are not registered as a member of the RLME LT Staff");
            }
        }
    }

    /// <summary>
    /// Proceed if the guild is either RLME Main or RLME Internal
    /// </summary>
    public class RequireRLMEGuild : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (Context.Guild.Id == RLMEConfig.StaticSav.PublicGuildId || Context.Guild.Id == RLMEConfig.StaticSav.InternalGuildId)
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: This isn't RLME!");
            }

        }
    }

    /// <summary>
    /// Proceed if the guild is RLME Internal
    /// </summary>
    public class RequireRLMEInternalGuild : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (Context.Guild.Id == RLMEConfig.StaticSav.InternalGuildId)
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: This isn't RLME Internal!");
            }

        }
    }

    /// <summary>
    /// Proceed if the guild is RLME Main
    /// </summary>
    public class RequireRLMEMainGuild : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (Context.Guild.Id == RLMEConfig.StaticSav.PublicGuildId)
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: This isn't RLME!");
            }

        }
    }
}
