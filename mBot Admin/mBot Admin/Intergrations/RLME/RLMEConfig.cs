﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mBot_Admin.ConfigService;
using System.IO;
using Newtonsoft.Json;

namespace mBot_Admin.Intergrations.RLME
{
    /// <summary>
    /// Config Class, including the static current config.
    /// </summary>
    public class RLMEConfig
    {
        public static async Task Load()
        {
            if (!File.Exists(PathStrings.RLME.RLMainConf))
            {
                var ttconf = new RLMEConfSav()
                {
                    AsaId = 367747734373007362,
                    HasRoleId = 363509277521870850,
                    InternalGuildId = 373757257286156302,
                    LtRoleId = 368783801020579841,
                    PublicGuildId = 363508876164726795,
                    StaffIds = new List<ulong>
                    {
                        267098766232911882,     // marens101#4158
                        367747734373007362      // ThatOneWeeb#1600 (Asa)
                    },
                    ModLogChanID = 363509296790765568
                };
                File.WriteAllText(PathStrings.RLME.RLMainConf, JsonConvert.SerializeObject(ttconf));
            }
            string json = File.ReadAllText(PathStrings.RLME.RLMainConf);
            RLMEConfSav tconf = JsonConvert.DeserializeObject<RLMEConfSav>(json);
            StaticSav = tconf;
            LastRefreshAt = DateTime.Now;
        }

        public static async Task Save()
        {
            File.WriteAllText(PathStrings.RLME.RLMainConf, JsonConvert.SerializeObject(StaticSav));
        }

        public static RLMEConfSav StaticSav;
        public static bool HasLoaded
            = false;
        public static DateTime LastRefreshAt;
    }

    /// <summary>
    /// The Config Object. NOT the static config, that is within RLMEConfig. This is a save file.
    /// </summary>
    public class RLMEConfSav
    {
        public ulong PublicGuildId;
        public ulong InternalGuildId;
        public List<ulong> StaffIds;
        public ulong HasRoleId;
        public ulong AsaId;
        public ulong LtRoleId;
        public ulong ModLogChanID;
    }
}
