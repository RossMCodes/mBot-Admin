﻿using Discord;
using Discord.Commands;
using mBot_Admin.ConfigService;
using mBot_Admin.ExtensionMethods;
using mBot_Admin.Objects.EncryptionObjects;
using mBot_Admin.Services.EncryptionService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Intergrations.RLME
{
    public class RLMEService
    {
        public async Task SetToken(ICommandContext Context, IUser user, string token)
        {
            string ConfigPath = new PathStrings.UserConfigs().RLMEUserConfig(user.Id);
            if (!File.Exists(ConfigPath))
            {
                RLMEObjects.Config tconf = new RLMEObjects().CreateDefaultConfiguration(user);
                await SaveConf(tconf);
            }
            RLMEObjects.Config conf = new RLMEObjects.Config(user);
            AesObjects.Config aesConfig;
            string EncryptedToken = new EncryptionService().AesEncrypt(token, out aesConfig);
            conf.AesConf = aesConfig;
            conf.EncryptedApiKey = EncryptedToken;
            await SaveConf(conf);
            await Context.Channel.SendMessageAsync("Your token has been set and encrypted successfully. Your encrypted token is: " + conf.EncryptedApiKey + "\n"
                + "If you ever feel the need to delete your token and all configuration, you can run \"`/rlme reset`\" to delete them from our servers immediately");
        }

        public async Task SetDefaultUploader(ICommandContext Context, IUser user, string newLink)
        {
            string ConfigPath = new PathStrings.UserConfigs().RLMEUserConfig(user.Id);
            RLMEObjects.Config conf = new RLMEObjects.Config(user);
            conf.UploadUrl = newLink;
            await SaveConf(conf);
            await Context.Channel.SendMessageAsync("Your new default file uploader is: `" + conf.UploadUrl + "`");
        }

        public async Task UploadFile(ICommandContext Context, string path, string fname)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                string result = await UploadFileAsync(fs, Context.User, fname);
                await Context.Channel.SendMessageAsync(result);
            }
        }

        public string StripHTTP(string baseUrl)
        {
            string t1 = baseUrl.Replace("https://", "");
            string t2 = t1.Replace("http://", "");
            return t2;
        }

        private string GetToken(IUser user)
        {
            if (!Config.Owners.IsOwner(user))
                throw new System.Exception("Error: You aren't an admin!");
            string ConfigPath = new PathStrings.UserConfigs().RLMEUserConfig(user.Id);
            if (!File.Exists(ConfigPath))
            {
                throw new System.Exception("You haven't set a token!");
            }
            RLMEObjects.Config conf = new RLMEObjects.Config(user);
            string DecryptedToken = new EncryptionService().AesDecrypt(conf.EncryptedApiKey, conf.AesConf);
            return DecryptedToken;
        }

        private string GetToken(IUser user, out RLMEObjects.Config conf)
        {
            string ConfigPath = new PathStrings.UserConfigs().RLMEUserConfig(user.Id);
            if (!File.Exists(ConfigPath))
            {
                throw new System.Exception("You haven't set a token!");
            }
            conf = new RLMEObjects.Config(user);
            string DecryptedToken = new EncryptionService().AesDecrypt(conf.EncryptedApiKey, conf.AesConf);
            return DecryptedToken;
        }

        public async Task SaveConf(RLMEObjects.Config conf)
        {
            string ConfigPath = new PathStrings.UserConfigs().RLMEUserConfig(conf.UserId);
            string json = JsonConvert.SerializeObject(conf);
            File.WriteAllText(ConfigPath, json);
        }

        public async Task WriteLog(RLMEObjects.UploadedFile Log)
        {
            await InitLog(Log.User);
            string LogPath = new PathStrings.UserConfigs().RLMEUserLogs(Log.User.Id);
            string LogContents;
            if (Log.StatusCode == HttpStatusCode.OK)
            {
                LogContents = $"{Log.TimeStamp_Iso}".PadRight(28) + " | "
                + $"UPLOAD {Log.StatusCode}".PadRight(31) + " | "
                + $"{Log.InputFileName.PadRight(27)} => {Log.ResultUrl}" + Environment.NewLine
                ;
            }
            else
            {
                LogContents = $"{Log.TimeStamp_Iso}".PadRight(28) + " | "
                + $"SHORTEN {Log.StatusCode}".PadRight(31) + " | "
                + $"{Log.InputFileName.PadRight(27)} | {Log.ErrorReason}" + Environment.NewLine
                ;
            }
            File.AppendAllText(LogPath, LogContents);
        }

        public async Task InitLog(IUser User)
        {
            string LogPath = new PathStrings.UserConfigs().RLMEUserLogs(User.Id);
            if (!File.Exists(LogPath))
            {
                File.WriteAllText(LogPath,
                    "IF SUCCESS: {TIMESTAMP_UTC}".PadRight(28) + " | "
                    + "{SHORTEN/UPLOAD} {SUCCESS_CODE}".PadRight(31) + " | "
                    + "{INPUT_FILE_NAME/INPUT_URL}".PadRight(27) + " => { RESULT_URL}" + Environment.NewLine
                    + "IF ERROR: {TIMESTAMP_UTC}".PadRight(28) + " | "
                    + "{SHORTEN/UPLOAD} {ERROR_CODE}".PadRight(31) + " | "
                    + "{INPUT_FILE_NAME/INPUT_URL}".PadRight(27) + " | {ERROR_RESPONSE}" + Environment.NewLine
                    );
            }
        }



        public async Task<string> UploadFileAsync(Stream s, string filename, IUser user, string EndpointIn = null)
        {
            string Endpoint = EndpointIn ?? ApiUrlStrings.Intergrations.RLME.UploadFileEndpoint;
            var dl = s.Length - s.Position;
            if (dl > 20 * 1024 * 1024 || dl < 0)
                throw new ArgumentException("The data needs to be less than 20MiB and greather than 0B long.");

            var b64data = new byte[8];
            var rnd = new Random();
            for (var i = 0; i < b64data.Length; i++)
                b64data[i] = (byte)rnd.Next();

            RLMEObjects.Config conf;

            var queryStrings = new Dictionary<string, string>
            {
                ["key"] = GetToken(user, out conf)
            };

            var baseUri = new Uri(Endpoint);
            var uri = baseUri.AttachParameters(queryStrings);

            var req = new HttpRequestMessage(HttpMethod.Post, uri);
            var mpd = new MultipartFormDataContent(string.Concat("---upload-", Convert.ToBase64String(b64data), "---"));

            var bdata = new byte[dl];
            await s.ReadAsync(bdata, 0, bdata.Length);

            var fn = Path.GetFileName(filename);
            var sc = new ByteArrayContent(bdata);
            sc.Headers.ContentType = new MediaTypeHeaderValue(MimeTypeMap.GetMimeType(Path.GetExtension(fn)));

            mpd.Add(sc, "files[]", string.Concat("\"", fn.ToLower(), "\""));

            req.Content = mpd;

            HttpClient client = new HttpClient();

            byte[] sha1 = SHA1.Create().ComputeHash(bdata);
            string sha1str = BitConverter.ToString(sha1);


            var res = await client.SendAsync(req);

            var json = await res.Content.ReadAsStringAsync();

            

            if (res.StatusCode == HttpStatusCode.OK)
            {
                var tfn = JsonConvert.DeserializeObject<RLMEObjects.UploadResponse.RootObject>(json);
                string configUrl = StripHTTP(conf.UploadUrl);
                if (!configUrl.EndsWith("/"))
                    configUrl = configUrl + "/";
                string text = tfn.files.FirstOrDefault().url;
                string customRepl = text.Replace(configUrl, text);
                string snipHttps = StripHTTP(customRepl);
                string final = "https://" + configUrl + snipHttps;

                RLMEObjects.UploadedFile Log = new RLMEObjects.UploadedFile()
                {
                    InputFileName = fn,
                    StatusCode = res.StatusCode,
                    ResultUrl = final,
                    User = user,
                    TimeStamp_Iso = DateTime.UtcNow.ToString(),
                };
                await WriteLog(Log);
                return final;
            }
            else
            {
                if(Endpoint == ApiUrlStrings.Intergrations.RLME.UploadFileEndpoint)
                {
                    string error = await res.Content.ReadAsStringAsync();
                    RLMEObjects.UploadedFile Log = new RLMEObjects.UploadedFile
                    {
                        InputFileName = fn,
                        ErrorReason = error,
                        User = user,
                        StatusCode = res.StatusCode,
                        TimeStamp_Iso = DateTime.UtcNow.ToString()
                    };
                    await WriteLog(Log);
                    return $"The API returned an error {Convert.ToInt32(res.StatusCode)}: {res.StatusCode}" + "\n"
                        + $"Error Message: \"`{error}`\"";
                }
                else
                {
                    string response = await UploadFileAsync(s, filename, user, ApiUrlStrings.Intergrations.RLME.UploadFileEndpoint);
                    return response;
                }
            }

        }



        public Task<string> UploadFileAsync(FileStream fs, IUser user, string fname) =>
            this.UploadFileAsync(fs, fname, user);
    }
}
