﻿using Discord;
using mBot_Admin.ConfigService;
using mBot_Admin.Objects.EncryptionObjects;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Intergrations.RLME
{
    public class RLMEObjects
    {
        public class Config
        {
            public Config()
            {

            }
            public Config(IUser user)
            {
                string ConfigPath = new PathStrings.UserConfigs().RLMEUserConfig(user.Id);
                if (!File.Exists(ConfigPath))
                    throw new System.Exception("Error: You have not set up your RLME credentials");
                string jsonObj = File.ReadAllText(ConfigPath);
                Config conf = JsonConvert.DeserializeObject<Config>(jsonObj);
                this.UserId = conf.UserId;
                this.EncryptedApiKey = conf.EncryptedApiKey;
                this.AesConf = conf.AesConf;
                this.UploadUrl = conf.UploadUrl;
            }
            public ulong UserId;
            public string EncryptedApiKey;
            public AesObjects.Config AesConf;
            public string UploadUrl;
        }
        public Config CreateDefaultConfiguration(IUser user)
        {
            Config conf = new Config
            {
                UserId = user.Id,
                EncryptedApiKey = null,
                AesConf = null,
                UploadUrl = "https://ratelimited.me/",
            };
            return conf;
        }

        public class UploadedFile
        {
            public IUser User;
            public string InputFileName;
            public string ResultUrl;
            public string TimeStamp_Iso;
            public HttpStatusCode StatusCode;
            public string ErrorReason;
        }

        public class UploadResponse
        {
            public class File
            {
                public bool success { get; set; }
                public int errorcode { get; set; }
                public string description { get; set; }
                public string hash { get; set; }
                public string name { get; set; }
                public string url { get; set; }
                public int? size { get; set; }
            }

            public class RootObject
            {
                public bool success { get; set; }
                public List<File> files { get; set; }
            }
        }
    }
}
