﻿using Discord;
using Discord.Commands;
using mBot_Admin.ConfigService;
using mBot_Admin.ExtensionMethods;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Intergrations.RLME
{
    [Group("RLME")]
    public class RLMEModule : ModuleBase
    {
        //[Command()]
        //[Priority(0)]
        public async Task IndexCmd([Remainder] string CatchAll)
        {
            await ReplyAsync("RATELIMITED (or RLME) is a POMF-Compatible file host. The main website is at https://ratelimited.me, and the invite to the Discord Server can be found there. You can sign up at https://ratelimited.me/signup.");
        }

        [Group("set")]
        public class Set : ModuleBase
        {
            [Command()]
            public async Task ListSetCmds()
            {
                await ReplyAsync("Availiable subcommands under `/RLME Set`: \"token\", \"t\", \"uploader\", \"u\"");
            }


            [Command("Token")]
            [Alias("t")]
            [Priority(1)]
            public async Task SetToken(string input)
            {
                if (Context.Channel is IDMChannel)
                    await new RLMEService().SetToken(Context, Context.User, input);
                else
                {
                    var x = await ReplyAsync(Context.User.Mention + " Uhhh... For security, we only allow tokens to be set from within DM channels. Sorry!");
                    try
                    {
                        await Context.Message.DeleteAsync();
                    }
                    catch
                    {
                        await x.ModifyAsync(m => m.Content =
                            "Uhhh...For security, we only allow tokens to be set from within DM channels. Sorry!" + "\n"
                            + "Also, it appears I don't have the permissions to delete your message containing your token. You might want to get rid of that!");
                    }
                }
            }

            [Command("DefaultUploader")]
            [RequireRLMEAccount]
            [Alias("u", "uploader", "baseupload")]
            [Priority(1)]
            public async Task SetUpload(string input)
            {
                await new RLMEService().SetDefaultUploader(Context, Context.User, input);
            }
        }

        [Command("Reset")]
        [RequireRLMEAccount]
        [Priority(1)]
        public async Task Reset()
        {
            File.Delete(new PathStrings.UserConfigs().RLMEUserConfig(Context.User.Id));
            await ReplyAsync("Your RLME Account and all configuration has been removed from our servers");
        }

        [Command("Colour")] //Go Aussie Spelling!
        [Alias("Color")]    //Fine, americans can have their special spelling *sigh*.
        [RequireRLMEMainGuild]
        [RequireRLMEStaff]
        public async Task ApplyColourRole(string HexIn, IUser user)
        {
            if (!(HexIn.TrimStart('#').Length == 6))
                throw new System.Exception("Not a valid hex code.");
            IGuild g = Context.Guild;
            var AllRoles = g.Roles;
            string hex;
            if (HexIn.StartsWith("#"))
                hex = HexIn.TrimStart('#').ToUpper();
            else
                hex = HexIn.ToUpper();
            IRole role = AllRoles.FirstOrDefault(f => f.Name == $"Colour: #{hex}");
            if (role == null)
            {
                var c = System.Drawing.ColorTranslator.FromHtml("#" + hex);
                Color colour = new Color(c.R, c.G, c.B);
                role = await Context.Guild.CreateRoleAsync($"Colour: #{hex}", color: colour);
                try
                {
                    IRole hasRole = AllRoles.FirstOrDefault(r => r.Id == RLMEConfig.StaticSav.HasRoleId);
                    if (hasRole == null)
                        throw new Exception("Could not locate the `Has Token` role.");
                    await role.ModifyAsync(d => d.Position = (hasRole.Position + 1));
                }
                catch(Exception e)
                {
                    await ReplyAsync("An error occured while attempting to hoist the new role above the `Has Token` role." + "\n"
                        + "Error Details: " + e.Message);
                }
                
            }

            await (user as IGuildUser).AddRoleAsync(role);
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("has")]
        [RequireRLMEStaff]
        [RequireRLMEGuild]
        [Priority(1)]
        public async Task SetUserHasTokenRole(IUser user)
        {
            var g = await Context.Client.GetGuildAsync(RLMEConfig.StaticSav.PublicGuildId);
            IRole role = g.GetRole(RLMEConfig.StaticSav.HasRoleId);
            await (user as IGuildUser).AddRoleAsync(role);
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("GetUser")]
        [RequireRLMEStaff]
        public async Task GetUser(IUser user)
        {
            var g = await Context.Client.GetGuildAsync(RLMEConfig.StaticSav.PublicGuildId);
            var usr = await g.GetUserAsync(user.Id);
            if (usr == null)
            {
                await ReplyAsync("That user wasn't found within the main RLME guild.");
            }
            else
            {
                await ReplyAsync("That user is in the RLME Main Guild!");
            }
        }

        [Command("AddStaff")]
        [RequireRLMELT]
        [Priority(1)]
        public async Task AddStaff(IUser user)
        {
            RLMEConfig.StaticSav.StaffIds.Add(user.Id);
            await RLMEConfig.Save();
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("RemStaff")]
        [RequireRLMELT]
        [Priority(1)]
        public async Task RemoveStaff(IUser user)
        {
            RLMEConfig.StaticSav.StaffIds.Remove(user.Id);
            await RLMEConfig.Save();
            await ReplyAsync("<:ok1:331993694867554305> Done!");
        }

        [Command("upload")]
        [Alias("u")]
        [RequireRLMEAccount]
        [Priority(1)]
        public async Task Upload(/*string url = null*/)
        {
            var attachment = Context.Message.Attachments.FirstOrDefault();
            if (attachment.Size > 20 * 1024 * 1024)
                throw new Exception("File Too Large: Must be smaller than 20mb");

            using (WebClient client = new WebClient())
            {
                byte[] data = client.DownloadData(attachment.Url);
                string hash = Convert.ToBase64String(MD5.Create().ComputeHash(data));
                string alphaHash = hash.ToAlphanumeric();
                string shortHash = alphaHash.GetPrefix(6);
                var splitName = attachment.Filename.Split('.');
                string path = $@"BotSystem\Intergrations\RLME\Temp\{shortHash}.{splitName[splitName.Length - 1]}";
                File.WriteAllBytes(path, data);
                await new RLMEService().UploadFile(Context, path, attachment.Filename);
                File.Delete(path);
            }
        }
    }
}
