﻿using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using mBot_Admin.ConfigService;

namespace mBot_Admin.Services.OnlineStatsService
{
    public class DiscordBotsDotOrgService
    {
        public async Task PostStats(DiscordSocketClient client)
        {
            var webclient = new HttpClient();
            var content = new StringContent($"{{ \"server_count\": {client.Guilds.Count}}}", Encoding.UTF8, "application/json");
            
            webclient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(new CredentialStrings().DiscordBotsDotOrgToken());
            HttpResponseMessage response = await webclient.PostAsync(ApiUrlStrings.BotLists.DiscordBotsDotOrg, content);
            
        }
    }
}
