﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using mBot_Admin.Objects.MusicObjects;

namespace mBot_Admin.Services.MusicService
{
    class MusicService
    {
        public static async Task<YTInfo> GetInfo(string url)
        {
            if (url.ToLower().Contains("youtube.com"))
            {
                var tup = await GetInfoFromYouTube(url);
                YTInfo rtn = new YTInfo
                {
                    Title = tup.Item1,
                    Duration = tup.Item2
                };
                return rtn;
            }
            else
            {
                throw new Exception("Video URL not supported!");
            }
        }

        private static async Task<Tuple<string, string>> GetInfoFromYouTube(string url)
        {
            TaskCompletionSource<Tuple<string, string>> tcs = new TaskCompletionSource<Tuple<string, string>>();

            new Thread(() => {
                string title;
                string duration;

                //youtube-dl.exe
                Process youtubedl;

                //Get Video Title
                ProcessStartInfo youtubedlGetTitle = new ProcessStartInfo()
                {
                    FileName = "youtube-dl",
                    Arguments = $"-s -e --get-duration {url}",
                    CreateNoWindow = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false
                };
                youtubedl = Process.Start(youtubedlGetTitle);
                youtubedl.WaitForExit();
                //Read Title
                string line1 = youtubedl.StandardOutput.ReadLine() ?? "No Title Found";
                string line2 = youtubedl.StandardOutput.ReadLine() ?? "0";






                tcs.SetResult(new Tuple<string, string>(line1, line2));
            }).Start();

            Tuple<string, string> result = await tcs.Task;
            if (result == null)
                throw new Exception("youtube-dl.exe failed to receive title!");

            return result;
        }




    }
}
