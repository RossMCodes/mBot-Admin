﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

using mBot_Admin.ConfigService;

namespace mBot_Admin.Services.SetupService
{
    class SetupFileService
    {
        public async Task SetupFiles()
        {
            await SetupDecide();
        }

        public async Task SetupSession()
        {
            File.Create(SessionConfigs.SessionLogPath);

            
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            string shortcutAddress = SessionConfigs.SessionDir + @"\sessionLog.lnk";
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutAddress);
            shortcut.Description = "Shortcut to Session Log";
            shortcut.TargetPath = SessionConfigs.SessionLogPath;
            shortcut.Save();
        }

        public async Task SetupDecide()
        {
            if (!File.Exists(@"BotSystem\Res\Decide\do.png"))
            {
                string imgurl = "https://s3-ap-southeast-2.amazonaws.com/static.marens101.com/AdminBot/Res/Decide/do.png";
                WebClient client = new WebClient();
                byte[] data = client.DownloadData(imgurl);
                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var yourImage = System.Drawing.Image.FromStream(mem))
                    {
                        yourImage.Save(@"BotSystem\Res\Decide\do.png", System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
            }
            if (!File.Exists(@"BotSystem\Res\Decide\hell.png"))
            {
                string imgurl = "https://s3-ap-southeast-2.amazonaws.com/static.marens101.com/AdminBot/Res/Decide/hell.png";
                WebClient client = new WebClient();
                byte[] data = client.DownloadData(imgurl);
                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var yourImage = System.Drawing.Image.FromStream(mem))
                    {
                        yourImage.Save(@"BotSystem\Res\Decide\hell.png", System.Drawing.Imaging.ImageFormat.Png);
                    }
                }
            }
        }
    }
}
