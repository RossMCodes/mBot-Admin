﻿using mBot_Admin.ConfigService;
using mBot_Admin.Modules.SystemModule;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Services.SetupService
{
    public class SetupFolderService
    {
        public async Task SetupFolders()
        {
            await SetupRoot();
            await SetupData();
            await SetupRes();
            await SetupLogs();
            await SetupIntergrations();
            await SetupMemes();
        }

        public async Task SetupRoot()
        {
            Directory.CreateDirectory(@"BotSystem");
            Directory.CreateDirectory(@"BotSystem\Sys");
            Directory.CreateDirectory(@"BotSystem\Sys\Rollbar");
            Directory.CreateDirectory(@"BotSystem\Data");
            Directory.CreateDirectory(@"BotSystem\Res");
            Directory.CreateDirectory(@"BotSystem\Logs");
            Directory.CreateDirectory(@"BotSystem\Intergrations");
            Directory.CreateDirectory(@"BotSystem\API");
        }

        public async Task SetupData()
        {
            Directory.CreateDirectory(@"BotSystem\Data\BurningForest");
            Directory.CreateDirectory(@"BotSystem\Data\BurningForest\Users");
            Directory.CreateDirectory(@"BotSystem\Data\Srv");
            Directory.CreateDirectory(@"BotSystem\Data\Srv\adminApprove");
            Directory.CreateDirectory(@"BotSystem\Data\Mod");
            Directory.CreateDirectory(@"BotSystem\Data\Mod\_All");
            Directory.CreateDirectory(@"BotSystem\Data\Starboard");
            Directory.CreateDirectory(@"BotSystem\Data\MemeWar");
            Directory.CreateDirectory($@"BotSystem\Data\Tags");
            Directory.CreateDirectory($@"BotSystem\Data\Tags");
        }
        public async Task SetupRes()
        {
            Directory.CreateDirectory(@"BotSystem\Res\Decide");
        }
        public async Task SetupLogs()
        {
            Directory.CreateDirectory(@"BotSystem\Logs\Events");
            Directory.CreateDirectory(@"BotSystem\Logs\Error");
            Directory.CreateDirectory(@"BotSystem\Logs\Session");
            Directory.CreateDirectory(SessionConfigs.SessionDir);
        }
        public async Task SetupIntergrations()
        {
            var Intergrations = new SetupIntergrationsService();
            await Intergrations.SetupOwO();
            await Intergrations.SetupRLME();
        }

        public class SetupIntergrationsService
        {
            public async Task SetupOwO()
            {
                Directory.CreateDirectory(@"BotSystem\Intergrations\OwO");
                Directory.CreateDirectory(@"BotSystem\Intergrations\OwO\Users");
                Directory.CreateDirectory(@"BotSystem\Intergrations\OwO\Logs");
                Directory.CreateDirectory(@"BotSystem\Intergrations\OwO\Temp");
            }
            public async Task SetupRLME()
            {
                Directory.CreateDirectory(@"BotSystem\Intergrations\RLME");
                Directory.CreateDirectory(@"BotSystem\Intergrations\RLME\Users");
                Directory.CreateDirectory(@"BotSystem\Intergrations\RLME\Logs");
                Directory.CreateDirectory(@"BotSystem\Intergrations\RLME\Temp");
            }
        }

        public async Task SetupMemes()
        {
            Directory.CreateDirectory(@"BotSystem\Res\Memes");
            Directory.CreateDirectory(@"BotSystem\Res\Memes\Pend");
            Directory.CreateDirectory(@"BotSystem\Res\Memes\Pend\Sent");
            Directory.CreateDirectory(@"BotSystem\Data\Memes");
        }
    }
}
