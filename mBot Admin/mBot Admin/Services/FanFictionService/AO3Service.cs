﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using mBot_Admin.Objects.FanFictionObjects;
using mBot_Admin.Templates.EmbedTemplates;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System.Web;
using mBot_Admin.Objects.DateTimeObjects;

namespace mBot_Admin.Services.FanFictionService
{
    public class AO3Service
    {
        public async Task GetFanfic(ICommandContext Context, int id, bool truncate)
        {
            var FanFicObj = await GetFFData(id, Context);
            await Context.Channel.TriggerTypingAsync();
            var builder = await CreateEmbed(Context, FanFicObj, truncate);
            await Context.Channel.TriggerTypingAsync();
            await Context.Channel.SendMessageAsync("", false, builder.Build());

        }

        public async Task<AO3Objects.FanFicObject> GetFFData(int id, ICommandContext Context)
        {
            string Url = $"https://archiveofourown.org/works/{id}";
            await Context.Channel.TriggerTypingAsync();
            var doc = await GetHtmlDocument(Url);
            await Context.Channel.TriggerTypingAsync();
            var NodeContainer = new GetNodes().CollectNodes(doc, id, Url);
            await Context.Channel.TriggerTypingAsync();
            var FanFic = new AO3Objects.FanFicObject(NodeContainer);
            await Context.Channel.TriggerTypingAsync();
            return FanFic;
        }

        public class GetNodes
        {
            public HtmlNodeCollection Get(HtmlDocument doc, string XPath)
            {
                var nodes = doc.DocumentNode.SelectNodes(XPath);
                return nodes;
            }

            public static class XPaths
            {
                public static string Collection(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[@*]/ul/li/a";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[@*]/ul/li/a";
                    }
                }

                public static string Title()
                {
                    return "//*[@id=\"workskin\"]/div[1]/h2";
                }

                public static string Author()
                {
                    return "//*[@id=\"workskin\"]/div[1]/h3/a";
                }

                public static string Summary()
                {
                    return "//*[@id=\"workskin\"]/div[1]/div/blockquote/p";
                }

                public static string Notes()
                {
                    return "//*[@id=\"workskin\"]/div[1]/div[2]/blockquote";
                }

                public static string Rating(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[1]/ul/li/a";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[1]/ul/li/a";
                    }
                }     

                public static string Warnings(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[2]/ul/li[*]/a";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[2]/ul/li[*]/a";
                    }
                }  

                public static string Categories(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[3]/ul/li[*]/a";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[3]/ul/li[*]/a";
                    }
                }

                public static string Fandoms(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[4]/ul/li/a";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[4]/ul/li/a";
                    }
                }

                public static string Relationships(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[5]/ul/li[*]/a";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[5]/ul/li[*]/a";
                    }
                }

                public static string Characters(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[6]/ul/li[*]/a";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[5]/ul/li[1]/a";
                    }
                }

                public static string AdditionalTags(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[7]/ul/li[*]/a";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[7]/ul/li[*]/a";
                    }
                }

                public static string Language(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[8]";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[8]";
                    }
                }

                public static string Series(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[9]/span/span[2]";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[9]/span/span[2]";
                        //Unconfirmed guess
                    }
                }

                public static string StatsKeys(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[10]/dl/dt[@*]";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[9]/dl/dt[@*]";
                    }
                }

                public static string StatsValues(v v)
                {
                    if (v == v.v1)
                    {
                        return "//*[@id=\"main\"]/div[2]/dl/dd[10]/dl/dd[@*]";
                    }
                    else
                    {
                        return "//*[@id=\"main\"]/div[2]/div[1]/dl/dd[9]/dl/dd[@*]";
                    }
                }

                public enum v
                {
                    v1,
                    v2,
                    
                }
            }

            public NodeContainer CollectNodes(HtmlDocument doc, int id, string url)
            {

                NodeContainer nodes = new NodeContainer();

                nodes.Title = Get(doc, XPaths.Title());
                nodes.Author = Get(doc, XPaths.Author());
                nodes.Summary = Get(doc, XPaths.Summary());
                nodes.Notes = Get(doc, XPaths.Summary());
                nodes.Url = url;
                nodes.Id = id;
                
                nodes.Rating = Get(doc, XPaths.Rating(XPaths.v.v1)) ?? Get(doc, XPaths.Rating(XPaths.v.v2));
                nodes.Warnings = Get(doc, XPaths.Warnings(XPaths.v.v1)) ?? Get(doc, XPaths.Warnings(XPaths.v.v2));
                nodes.Categories = Get(doc, XPaths.Categories(XPaths.v.v1)) ?? Get(doc, XPaths.Categories(XPaths.v.v2));
                nodes.Fandoms = Get(doc, XPaths.Fandoms(XPaths.v.v1)) ?? Get(doc, XPaths.Fandoms(XPaths.v.v2));
                nodes.Relationships = Get(doc, XPaths.Relationships(XPaths.v.v1)) ?? Get(doc, XPaths.Relationships(XPaths.v.v2));
                nodes.Characters = Get(doc, XPaths.Characters(XPaths.v.v1)) ?? Get(doc, XPaths.Characters(XPaths.v.v2));
                nodes.AdditionalTags = Get(doc, XPaths.AdditionalTags(XPaths.v.v1)) ?? Get(doc, XPaths.AdditionalTags(XPaths.v.v2));
                nodes.Language = Get(doc, XPaths.Language(XPaths.v.v1)) ?? Get(doc, XPaths.Language(XPaths.v.v2));
                nodes.Series = Get(doc, XPaths.Series(XPaths.v.v1)) ?? Get(doc, XPaths.Series(XPaths.v.v2));
                nodes.Stats.Keys = Get(doc, XPaths.StatsKeys(XPaths.v.v1)) ?? Get(doc, XPaths.StatsKeys(XPaths.v.v2));
                nodes.Stats.Values = Get(doc, XPaths.StatsValues(XPaths.v.v1)) ?? Get(doc, XPaths.StatsValues(XPaths.v.v2));
                nodes.Stats.StatsPairs = new NodeContainer.StatsNode().CreateStatsPairs(nodes.Stats.Keys, nodes.Stats.Values);
                nodes.Stats.StatsDict = new NodeContainer.StatsNode().CreateStatsDict(nodes.Stats.StatsPairs);
                
                return nodes;
            }

            public class NodeContainer
            {
                public HtmlNodeCollection Title;
                public HtmlNodeCollection Author;
                public HtmlNodeCollection Summary;
                public HtmlNodeCollection Notes;
                public string Url;
                public int Id;

                public HtmlNodeCollection Rating;
                public HtmlNodeCollection Warnings;
                public HtmlNodeCollection Categories;
                public HtmlNodeCollection Fandoms;
                public HtmlNodeCollection Relationships;
                public HtmlNodeCollection Characters;
                public HtmlNodeCollection AdditionalTags;
                public HtmlNodeCollection Language;
                public HtmlNodeCollection Series;
                public StatsNode Stats;
                public struct StatsNode
                {
                    public Dictionary<HtmlNode, HtmlNode> CreateStatsDict(HtmlNodeCollection Keys, HtmlNodeCollection Values)
                    {
                        var Pair = new KeyValuePair<HtmlNodeCollection, HtmlNodeCollection>(Keys, Values);
                        return new Transfom().CompoundStats(Pair);
                    }
                    public Dictionary<HtmlNode, HtmlNode> CreateStatsDict(KeyValuePair<HtmlNodeCollection, HtmlNodeCollection> Pair)
                    {
                        return new Transfom().CompoundStats(Pair);
                    }
                    public KeyValuePair<HtmlNodeCollection, HtmlNodeCollection> CreateStatsPairs(HtmlNodeCollection KeysIn, HtmlNodeCollection ValuesIn)
                    {
                        HtmlNodeCollection KeySet = KeysIn;
                        HtmlNodeCollection ValueSet = ValuesIn;
                        return new KeyValuePair<HtmlNodeCollection, HtmlNodeCollection>(KeySet, ValueSet);
                    }

                    public Dictionary<HtmlNode, HtmlNode> StatsDict;
                    public KeyValuePair<HtmlNodeCollection, HtmlNodeCollection> StatsPairs;
                    public HtmlNodeCollection Keys;
                    public HtmlNodeCollection Values;
                }
            }
        }

        public class Transfom
        {
            public Dictionary<object, object> Compound(KeyValuePair<IEnumerable<object>, IEnumerable<object>> Pair)
            {
                var Pairs = Pair.Key.Zip(Pair.Value, (k, v) => new KeyValuePair<object, object>(k, v));
                var Dict = Pairs.ToDictionary(x => x.Key, x => x.Value);
                return Dict;
            }
            public Dictionary<HtmlNode, HtmlNode> CompoundStats(KeyValuePair<HtmlNodeCollection, HtmlNodeCollection> Pair)
            {
                var Pairs = Pair.Key.Zip(Pair.Value, (k, v) => new KeyValuePair<HtmlNode, HtmlNode>(k, v));
                var Dict = Pairs.ToDictionary(x => x.Key, x => x.Value);
                return Dict;
            }
        }

        public async Task<HtmlDocument> GetHtmlDocument(string url)
        {
            HtmlWeb web = new HtmlWeb()
            {
                PreRequest = request =>
                {
                    // Make any changes to the request object that will be used.
                    request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                    return true;
                }
            };
            var doc = await Task.Factory.StartNew(() => web.Load(url));
            return doc;
        }

        public async Task<EmbedBuilder> CreateEmbed(ICommandContext Context, AO3Objects.FanFicObject Fic, bool truncate)
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var Author = new Templates.EmbedTemplates.EmbedAuthor().DefaultEmbedAuthor(application);
            var builder = new EmbedBuilder()
            {
                Footer = new EmbedFooterBuilder()
                {
                    IconUrl = "http://blog.commarts.wisc.edu/wp-content/uploads/2010/04/logo.png",
                    Text = "All works are property of their respective owners"
                },
                Author = Author,
                Color = new Color(151, 1, 30),
                Timestamp = DateTime.UtcNow,
                Url = Fic.Url,
                Title = Fic.Title,
            };
            bool truncd = false;
            builder = AddSummary(builder, Fic, truncate, 200, truncd, out truncd);
            builder = AddNotes(builder, Fic, truncate, 200, truncd, out truncd);
            builder.AddInlineField("Fandom", ParseList(Fic.Fandoms));
            builder.AddInlineField("Author", $"[{Fic.AuthorName}]({Fic.AuthorUrl})");
            builder.AddInlineField("Categories", ParseList(Fic.Categories));
            builder.AddInlineField("Warnings", ParseList(Fic.Warnings));
            builder.AddInlineField("Rating", Fic.Rating);
            builder.AddInlineField("Language", Fic.Language);
            builder.AddInlineField("Characters", ParseList(Fic.Characters));
            builder.AddInlineField("Relationships", ParseList(Fic.Relationships));
            builder.AddInlineField("Additional Tags", ParseList(Fic.AdditionalTags));
            builder.AddInlineField("Words", Fic.Stats.Words);
            builder.AddInlineField("Published", Fic.Stats.Published.Msg);
            builder.AddInlineField("Updated", Fic.Stats.Updated.Msg);
            builder.AddInlineField("Kudos", Fic.Stats.Kudos);
            builder.AddInlineField("Hits", Fic.Stats.Hits);
            builder.AddInlineField("Bookmarks", Fic.Stats.Bookmarks);
            builder.AddInlineField("Chapters", Fic.Stats.Chapters);

            builder = TruncdNotice(builder, truncd, 200);

            return builder;
        }

        public EmbedBuilder AddSummary(EmbedBuilder em, AO3Objects.FanFicObject Fic, bool TruncActive, int TruncAmnt, bool truncdin, out bool truncdout)
        {
            truncdout = truncdin;
            string Sum = ParseParagraph(Fic.Summary);
            bool trunc = (TruncActive && Sum.Length > TruncAmnt);
            if (trunc)
            {
                em.Description = Snip(Sum, TruncAmnt);
                truncdout = true;
            }
            else
            {
                em.Description = Sum;
            }
            return em;
        }
        public EmbedBuilder AddNotes(EmbedBuilder em, AO3Objects.FanFicObject Fic, bool TruncActive, int TruncAmnt, bool truncdin, out bool truncdout)
        {
            truncdout = truncdin;
            string Notes = ParseParagraph(Fic.Notes);
            bool trunc = (TruncActive && Notes.Length > TruncAmnt);
            if (trunc)
            {
                em.AddInlineField("Notes", Snip(Notes, TruncAmnt));
                truncdout = true;
            }
            else
            {
                em.AddInlineField("Notes", Notes);
            }
            return em;
        }
        public EmbedBuilder TruncdNotice(EmbedBuilder b, bool truncd, int truncAmnt)
        {
            if (truncd)
                b.AddField("Truncation Notice", 
                    "Some text was truncated because it was over " + truncAmnt + " characters." + "\n"
                    + "To avoid truncation, append \"`false`\" to the end of the fetch command." + "\n"
                    + "Like this: \"`/AO3 [ID_Or_URL] false`\"");

            return b;
        }
        public string ParseList(List<string> List)
        {
            string output = "";
            string temp = "";
            foreach (string s in List)
            {
                temp = output + s.Trim() + ", ";
                output = temp;
            }
            output = temp.TrimEnd(' ', ',');
            return output;
        }
        public string ParseParagraph(List<string> List)
        {
            string output = "";
            string temp = "";
            foreach (string s in List)
            {
                temp = output + s.Trim() + "\n";
                output = temp;
            }
            output = temp.TrimEnd(' ', '\n');
            return output;
        }
        public string Snip(string input, int maxlength)
        {
            if (input.Length > maxlength)
            {
                string output = input.Remove(maxlength, input.Length - maxlength);
                return output + "... [truncated]";
            }
            return input;
        }


    }
}
