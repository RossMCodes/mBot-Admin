﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using mBot_Admin.Objects.FanFictionObjects;
using mBot_Admin.Templates.EmbedTemplates;
using HtmlAgilityPack;
using Newtonsoft.Json;
using System.Web;

//Scraping: https://www.youtube.com/watch?v=4cPPD-MFadQ

namespace mBot_Admin.Services.FanFictionService
{
    public class FanFictionDotNetService
    {
        public async Task GetFanfic(ICommandContext Context, int id, bool truncate)
        {
            var FanFicObj = await GetFFData(id, Context);



            var builder = await CreateFFNETEmbed(Context, FanFicObj, truncate);
            var embed = builder.Build();
            //File.WriteAllText($@"nodes-ffnet-s{id}.json", json);
            await Context.Channel.SendMessageAsync("", embed: embed);
        }

        public async Task<EmbedBuilder> CreateFFNETEmbed(ICommandContext Context, FanFictionDotNetObjects.FanFicObject FicObject, bool truncate)
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var Author = new Templates.EmbedTemplates.EmbedAuthor().DefaultEmbedAuthor(application);
            EmbedBuilder builder = new EmbedBuilder()
            {
                Author = Author,
                Footer = new EmbedFooterBuilder()
                {
                    IconUrl = "http://blog.commarts.wisc.edu/wp-content/uploads/2010/04/logo.png",
                    Text = "All works are property of their respective owners"
                },
                Title = FicObject.Title,
                Color = Color.Purple,
                Url = FicObject.Url,
                Timestamp = DateTime.Now.ToUniversalTime(),
                //ThumbnailUrl = "https://pbs.twimg.com/profile_images/438218261390311424/IE-ehflB_400x400.png",
            };
            bool truncd = false;
            builder = AddSummary(builder, FicObject.Summary, truncate, 200, truncd, out truncd);

            builder.AddInlineField("Words", FicObject.Words);
            builder.AddInlineField("Faves", FicObject.Favs);
            builder.AddInlineField("Reviews", FicObject.Reviews);
            builder.AddInlineField("Author", $"[{FicObject.Author.Author}]({FicObject.Author.AuthorUrl})");
            builder.AddInlineField("Chapters", FicObject.Chapters);
            builder.AddInlineField("Follows", FicObject.Follows);
            builder.AddInlineField("Rating", FicObject.Rating);
            builder.AddInlineField("Genre", FicObject.Genre);
            builder.AddInlineField("Language", FicObject.Language);
            builder.AddInlineField("Ships", FicObject.Pairings);
            builder.AddInlineField("Published", FicObject.Published);
            builder.AddInlineField("Updated", FicObject.Updated);

            builder.AddInlineField("Id", FicObject.Id);

            builder = TruncdNotice(builder, truncd, 200);

            //builder.AddInlineField("", FicObject);
            return builder;
        }

        public string Snip(string input, int maxlength)
        {
            if (input.Length > maxlength)
            {
                string output = input.Remove(maxlength, input.Length - maxlength);
                return output + "... [truncated]";
            }
            return input;
        }
        public EmbedBuilder AddSummary(EmbedBuilder em, string Summary, bool TruncActive, int TruncAmnt, bool truncdin, out bool truncdout)
        {
            truncdout = truncdin;
            bool trunc = (TruncActive && Summary.Length > TruncAmnt);
            if (trunc)
            {
                em.Description = Snip(Summary, TruncAmnt);
                truncdout = true;
            }
            else
            {
                em.Description = Summary;
            }
            return em;
        }
        public EmbedBuilder TruncdNotice(EmbedBuilder b, bool truncd, int truncAmnt)
        {
            if (truncd)
                b.AddField("Truncation Notice",
                    "Some text was truncated because it was over " + truncAmnt + " characters." + "\n"
                    + "To avoid truncation, append \"`false`\" to the end of the fetch command." + "\n"
                    + "Like this: \"`/FFNET [ID_Or_URL] false`\"");

            return b;
        }

        public async Task<FanFictionDotNetObjects.FanFicObject> GetFFData(int id, ICommandContext Context)
        {
            var doc = await GetHtmlDocument($"https://www.fanfiction.net/s/{id}");
            await Context.Channel.TriggerTypingAsync();
            var List = new FFData().ConvertDataToList(doc).FirstOrDefault<KeyValuePair<string, List<string>>>().Value;
            await Context.Channel.TriggerTypingAsync();
            var Author = await new FFData().GetAuthor(doc);
            await Context.Channel.TriggerTypingAsync();
            var Title = new FFData().GetTitle(doc);
            await Context.Channel.TriggerTypingAsync();
            var Summary = await new FFData().GetSummary(doc);
            await Context.Channel.TriggerTypingAsync();
            var TempData = new FFData().CompileTempFFData(id, List, Author, Title, Summary);
            await Context.Channel.TriggerTypingAsync();
            var FanFicObject = new FFData().CreateFanFicObject(TempData);
            await Context.Channel.TriggerTypingAsync();
            return FanFicObject;
        }

        public class FFData
        {
            public struct TempFFData
            {
                public FanFictionDotNetObjects.AuthorObject Author;
                public string Title;
                public int Id;
                public string Url;
                public string Summary;
                public List<string> Metadata;
            }

            public TempFFData CompileTempFFData(int id, List<string> List, FanFictionDotNetObjects.AuthorObject Author, string Title, string Summary)
            {
                var x = new TempFFData
                {
                    Id = id,
                    Metadata = List,
                    Url = $"https://www.fanfiction.net/s/{id}",
                    Author = Author,
                    Title = Title,
                    Summary = Summary
                };
                

                return x;
            }

            public async Task<FanFictionDotNetObjects.AuthorObject> GetAuthor(HtmlDocument doc)
            {
                var nodes = doc.DocumentNode.SelectNodes("//*[@id=\"profile_top\"]/a[1]");
                var node = nodes.FirstOrDefault<HtmlNode>();
                var x = new FanFictionDotNetObjects.AuthorObject();
                x.Author = node.InnerText;
                x.AuthorUrl = "https://www.fanfiction.net" + node.Attributes["href"].Value;
                return x;
            }

            public async Task<String> GetSummary(HtmlDocument doc)
            {
                var nodes = doc.DocumentNode.SelectNodes("//*[@id=\"profile_top\"]/div");
                var node = nodes.FirstOrDefault<HtmlNode>();
                var x = node.InnerText;
                return x;
            }

            public FanFictionDotNetObjects.FanFicObject CreateFanFicObject(TempFFData Data)
            {
                var obj = new FanFictionDotNetObjects.FanFicObject();
                var List = Data.Metadata;
                obj.Author = Data.Author;
                obj.Chapters = new Get().Chapters(List);
                obj.Favs = new Get().Favs(List);
                obj.Follows = new Get().Follows(List);
                obj.Genre = List[2];
                obj.Id = ConvertToInt32(GetAndTrimStart(List, "id: "));
                obj.Language = List[1];
                obj.Pairings = new Get().Pairings(List);
                obj.Published = GetAndTrimStart(List, "Published: ");
                obj.Rating = GetAndTrimStart(List, "Rated: ");
                obj.Reviews = new Get().Reviews(List);
                obj.Summary = Data.Summary;
                obj.Title = Data.Title;
                obj.Updated = new Get().Updated(List);
                obj.Url = Data.Url;
                obj.Words = ConvertToInt32(GetAndTrimStart(List, "Words: "));

                return obj;
            }

            public class Get
            {
                public string Updated(List<string> List)
                {
                    try
                    {
                        return GetAndTrimStart(List, "Updated: ");
                    }
                    catch
                    {
                        return "Never Updated";
                    }
                }

                public int Reviews(List<string> List)
                {
                    try
                    {
                        return ConvertToInt32(GetAndTrimStart(List, "Reviews: "));
                    }
                    catch
                    {
                        return 0;
                    }
                }

                public int Favs(List<string> List)
                {
                    try
                    {
                        return ConvertToInt32(GetAndTrimStart(List, "Favs: "));
                    }
                    catch
                    {
                        return 0;
                    }
                }

                public int Follows(List<string> List)
                {
                    try
                    {
                        return ConvertToInt32(GetAndTrimStart(List, "Follows: "));
                    }
                    catch
                    {
                        return 0;
                    }
                }

                public string Pairings(List<string> List)
                {
                    if (List[3].Contains("Chapters") || List[3].Contains("Words"))
                    {
                        return "No Pairings Listed";
                    }
                    else
                    {
                        return List[3];
                    }
                }

                public int Chapters(List<string> List)
                {
                    try
                    {
                        return ConvertToInt32(GetAndTrimStart(List, "Chapters: "));
                    }
                    catch
                    {
                        return 1;
                    }
                }

                public string GetAndTrimStart(List<string> List, string Prefix)
                {
                    return new FFData().GetAndTrimStart(List, Prefix);
                }
                public int ConvertToInt32(string str)
                {
                    return new FFData().ConvertToInt32(str);
                }
            }

            public HtmlNodeCollection SelectNodesForList(HtmlDocument doc)
            {
                //Exists because oneshots brak the standard system
                HtmlNodeCollection nodes;
                try
                {
                    //normal
                    nodes = doc.DocumentNode.SelectNodes("//*[@id=\"profile_top\"]/span[4]");
                    if (nodes == null)
                        throw new Exception("This Is A OneShot! (I Think...)");
                    return nodes;
                }
                catch
                {
                    //oneshot
                    nodes = doc.DocumentNode.SelectNodes("//*[@id=\"profile_top\"]/span[3]");

                    return nodes;
                }
            }

            public List<KeyValuePair<string, List<string>>> ConvertDataToList(HtmlDocument doc)
            {

                HtmlNodeCollection nodes = SelectNodesForList(doc);
                var dict = new List<KeyValuePair<string, List<string>>>();
                foreach (HtmlNode node in nodes)
                {
                    var list_ = node.InnerText.Split('-').ToList<string>();
                    List<string> list = new List<string>();
                    foreach (string s in list_)
                    {
                        list.Add(s.Trim());
                    }
                    var pairs = new KeyValuePair<string, List<string>>(node.Id, list);
                    dict.Add(pairs);
                }
                return dict;
            }

            public int ConvertToInt32(string str)
            {
                string input = str.Replace(",", "");
                int output = Convert.ToInt32(input);
                return output;
            }

            public string FirstOrDefaultSorE(List<string> List, string StartsOrEndsWith, bool CheckingStart = true)
            {
                if (CheckingStart == false)
                {
                    return List.FirstOrDefault<string>(s => s.EndsWith(StartsOrEndsWith));
                }
                else
                {
                    return List.FirstOrDefault<string>(s => s.StartsWith(StartsOrEndsWith));
                }
            }

            public string GetAndTrimStart(List<string> List, string Prefix)
            {
                string y = FirstOrDefaultSorE(List, Prefix);
                string x = y.Remove(0, Prefix.Length);
                return x;
            }

            public string GetTitle(HtmlDocument doc)
            {
                var nodes = doc.DocumentNode.SelectNodes("//*[@id=\"profile_top\"]/b");
                var node = nodes.FirstOrDefault<HtmlNode>();
                var x = node.InnerText;
                return x;
            }
        }

        public async Task<HtmlDocument> GetHtmlDocument(string url)
        {
            HtmlWeb web = new HtmlWeb()
            {
                PreRequest = request =>
                {
                    // Make any changes to the request object that will be used.
                    request.AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip;
                    return true;
                }
            };
            var doc = await Task.Factory.StartNew(() => web.Load(url));
            return doc;
        }
    }
}
