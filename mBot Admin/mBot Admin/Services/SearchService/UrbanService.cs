﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;
using System.Threading;
using System.Globalization;
using mBot_Admin.Objects.SearchObjects;
using Newtonsoft.Json;

namespace mBot_Admin.Services.SearchService
{
    public class UrbanService
    {
        public async Task UrbanSearch(ICommandContext Context, string search)
        {
            await Context.Channel.TriggerTypingAsync();
            var conf = new mBot_Admin.Modules.ConfigModule.Config();
            if (conf.MashapeKey == null)
                throw new System.Exception("Mashape API Key Not Found");
            string html = string.Empty;
            string url = $@"https://mashape-community-urban-dictionary.p.mashape.com/define?term={search}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Accept = "application/json";
            request.Headers.Add("X-Mashape-Key", conf.MashapeKey);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream stream = response.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            string json = reader.ReadToEnd();
            UrbanObjects.RootObject obj = JsonConvert
                .DeserializeObject<UrbanObjects.RootObject>(json);

            var res = obj.list.FirstOrDefault();
            await Context.Channel.SendMessageAsync("Definition: " + res.definition + "\n"
                                                     + "Example: " + res.example);
        }
    }
}
