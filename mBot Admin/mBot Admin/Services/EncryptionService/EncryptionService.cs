﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Security.Cryptography;
using mBot_Admin.Objects.EncryptionObjects;

namespace mBot_Admin.Services.EncryptionService
{
    /// <summary>
    /// Code adapted from https://stackoverflow.com/a/27484425/7484229
    /// </summary>
    public class EncryptionService
    {
        public string AesEncrypt(string clearText, out AesObjects.Config aesConfig)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            Aes encryptor = Aes.Create();
            encryptor.GenerateKey();
            encryptor.GenerateIV();

            aesConfig = new AesObjects.Config(encryptor);

            MemoryStream ms = new MemoryStream();

            CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write);
            
            cs.Write(clearBytes, 0, clearBytes.Length);
            cs.Close();
            
            clearText = Convert.ToBase64String(ms.ToArray());
            ms.Close();

            encryptor.Clear();

            return clearText;
        }

        public string AesDecrypt(string cipherText, AesObjects.Config aesConfig)
        {
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            Aes encryptor = aesConfig.CreateAes();


            MemoryStream ms = new MemoryStream();

            CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write);
            
            cs.Write(cipherBytes, 0, cipherBytes.Length);
            cs.Close();
            
            cipherText = Encoding.Unicode.GetString(ms.ToArray());
            ms.Close();

            encryptor.Clear();
            
            return cipherText;
        }
    }
}
