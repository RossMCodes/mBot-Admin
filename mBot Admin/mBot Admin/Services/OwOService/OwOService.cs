﻿using Discord;
using Discord.Commands;
using mBot_Admin.Objects.EncryptionObjects;
using mBot_Admin.Objects.OwOObjects;
using mBot_Admin.Services.EncryptionService;
using mBot_Admin.ConfigService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using System.Web;
using mBot_Admin.ExtensionMethods;
using System.Net.Http.Headers;

namespace mBot_Admin.Services.OwOService
{
    public class OwOService
    {
        public async Task SetToken(ICommandContext Context, IUser user, string token)
        {
            string ConfigPath = new PathStrings.UserConfigs().OwOUserConfig(user.Id);
            if (!File.Exists(ConfigPath))
            {
                OwOObjects.Config tconf = new OwOObjects().CreateDefaultConfiguration(user);
                await SaveConf(tconf);
            }
            OwOObjects.Config conf = new OwOObjects.Config(user);
            AesObjects.Config aesConfig;
            string EncryptedToken = new EncryptionService.EncryptionService().AesEncrypt(token, out aesConfig);
            conf.AesConf = aesConfig;
            conf.EncryptedApiKey = EncryptedToken;
            await SaveConf(conf);
            await Context.Channel.SendMessageAsync("Your token has been set and encrypted successfully. Your encrypted token is: " + conf.EncryptedApiKey + "\n"
                + "If you ever feel the need to delete your token and all configuration, you can run \"`/OwO reset`\" to delete them from our servers immediately");
        }

        public async Task SetDefaultShortener(ICommandContext Context, IUser user, string newLink)
        {
            string ConfigPath = new PathStrings.UserConfigs().OwOUserConfig(user.Id);
            OwOObjects.Config conf = new OwOObjects.Config(user);
            conf.ShortenUrl = newLink;
            await SaveConf(conf);
            await Context.Channel.SendMessageAsync("Your new default link shortener is: `" + conf.ShortenUrl + "`");
        }

        public async Task SetDefaultUploader(ICommandContext Context, IUser user, string newLink)
        {
            string ConfigPath = new PathStrings.UserConfigs().OwOUserConfig(user.Id);
            OwOObjects.Config conf = new OwOObjects.Config(user);
            conf.UploadUrl = newLink;
            await SaveConf(conf);
            await Context.Channel.SendMessageAsync("Your new default file uploader is: `" + conf.UploadUrl + "`");
        }

        public async Task ShortenUrl(ICommandContext Context, string UrlToShorten)
        {
            string result = await ShortenAsync(Context, UrlToShorten);
            await Context.Channel.SendMessageAsync(result);
        }

        public async Task UploadFile(ICommandContext Context, string path, string fname)
        {
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                string result = await UploadFileAsync(fs, Context.User, fname);
                await Context.Channel.SendMessageAsync(result);
            }
        }

        public async Task<string> ShortenAsync(ICommandContext Context, string UrlToShorten)
        {
            OwOObjects.Config conf;
            string token = GetToken(Context.User, out conf);

            var QueryStrings = new Dictionary<string, string>();
            QueryStrings.Add("action", "shorten");
            QueryStrings.Add("apikey", token);
            QueryStrings.Add("url", UrlToShorten);
            Uri uri = new Uri(ApiUrlStrings.Intergrations.OwO.ShortenUrlEndpoint)
                .AttachParameters(QueryStrings);
            HttpClient httpClient = new HttpClient();
            var res = await httpClient.GetAsync(uri);
            httpClient.Dispose();
            if (res.StatusCode == HttpStatusCode.OK)
            {
                string configUrl = StripHTTP(conf.ShortenUrl);
                if (configUrl.EndsWith("/"))
                    configUrl = configUrl.TrimEnd('/');
                string text = await res.Content.ReadAsStringAsync();
                string customRepl = text.Replace("awau.moe", configUrl);
                string snipHttps = StripHTTP(customRepl);
                string final = "https://" + snipHttps;
                OwOObjects.ShortenedUrl Log = new OwOObjects.ShortenedUrl
                {
                    InputUrl = UrlToShorten,
                    ResultUrl = final,
                    User = Context.User,
                    StatusCode = res.StatusCode,
                    TimeStamp_Iso = DateTime.UtcNow.ToString()
                };
                await WriteLog(Log);
                return final;
            }
            else
            {
                string error = await res.Content.ReadAsStringAsync();
                OwOObjects.ShortenedUrl Log = new OwOObjects.ShortenedUrl
                {
                    InputUrl = UrlToShorten,
                    ErrorReason = error,
                    User = Context.User,
                    StatusCode = res.StatusCode,
                    TimeStamp_Iso = DateTime.UtcNow.ToString()
                };
                await WriteLog(Log);
                return $"The API returned an error {Convert.ToInt32(res.StatusCode)}: {res.StatusCode}" + "\n"
                    + $"Error Message: \"`{error}`\"";
            }
        }



        public string StripHTTP(string baseUrl)
        {
            string t1 = baseUrl.Replace("https://", "");
            string t2 = t1.Replace("http://", "");
            return t2;
        }

        private string GetToken(IUser user)
        {
            if (!Config.Owners.IsOwner(user))
                throw new System.Exception("Error: You aren't an admin!");
            string ConfigPath = new PathStrings.UserConfigs().OwOUserConfig(user.Id);
            if (!File.Exists(ConfigPath))
            {
                throw new System.Exception("You haven't set a token!");
            }
            OwOObjects.Config conf = new OwOObjects.Config(user);
            string DecryptedToken = new EncryptionService.EncryptionService().AesDecrypt(conf.EncryptedApiKey, conf.AesConf);
            return DecryptedToken;
        }

        private string GetToken(IUser user, out OwOObjects.Config conf)
        {
            if (!Config.Owners.IsOwner(user))
                throw new System.Exception("Error: You aren't an admin!");
            string ConfigPath = new PathStrings.UserConfigs().OwOUserConfig(user.Id);
            if (!File.Exists(ConfigPath))
            {
                throw new System.Exception("You haven't set a token!");
            }
            conf = new OwOObjects.Config(user);
            string DecryptedToken = new EncryptionService.EncryptionService().AesDecrypt(conf.EncryptedApiKey, conf.AesConf);
            return DecryptedToken;
        }
        
        public async Task SaveConf(OwOObjects.Config conf)
        {
            string ConfigPath = new PathStrings.UserConfigs().OwOUserConfig(conf.UserId);
            string json = JsonConvert.SerializeObject(conf);
            File.WriteAllText(ConfigPath, json);
        }

        public async Task WriteLog(OwOObjects.ShortenedUrl Log)
        {
            await InitLog(Log.User);
            string LogPath = new PathStrings.UserConfigs().OwOUserLogs(Log.User.Id);
            string LogContents;
            if(Log.StatusCode == HttpStatusCode.OK)
            {
                LogContents = $"{Log.TimeStamp_Iso}".PadRight(28) + " | "
                + $"SHORTEN {Log.StatusCode}".PadRight(31) + " | "
                + $"{Log.InputUrl.PadRight(27)} => {Log.ResultUrl}" + Environment.NewLine
                ;
            }
            else
            {
                LogContents = $"{Log.TimeStamp_Iso}".PadRight(28) + " | "
                + $"SHORTEN {Log.StatusCode}".PadRight(31) + " | "
                + $"{Log.InputUrl.PadRight(27)} | {Log.ErrorReason}" + Environment.NewLine
                ;
            }
            File.AppendAllText(LogPath, LogContents);
        }

        public async Task WriteLog(OwOObjects.UploadedFile Log)
        {
            await InitLog(Log.User);
            string LogPath = new PathStrings.UserConfigs().OwOUserLogs(Log.User.Id);
            string LogContents;
            if (Log.StatusCode == HttpStatusCode.OK)
            {
                LogContents = $"{Log.TimeStamp_Iso}".PadRight(28) + " | "
                + $"UPLOAD {Log.StatusCode}".PadRight(31) + " | "
                + $"{Log.InputFileName.PadRight(27)} => {Log.ResultUrl}" + Environment.NewLine
                ;
            }
            else
            {
                LogContents = $"{Log.TimeStamp_Iso}".PadRight(28) + " | "
                + $"SHORTEN {Log.StatusCode}".PadRight(31) + " | "
                + $"{Log.InputFileName.PadRight(27)} | {Log.ErrorReason}" + Environment.NewLine
                ;
            }
            File.AppendAllText(LogPath, LogContents);
        }

        public async Task InitLog(IUser User)
        {
            string LogPath = new PathStrings.UserConfigs().OwOUserLogs(User.Id);
            if (!File.Exists(LogPath))
            {
                File.WriteAllText(LogPath,
                    "IF SUCCESS: {TIMESTAMP_UTC}".PadRight(28) + " | " 
                    + "{SHORTEN/UPLOAD} {SUCCESS_CODE}".PadRight(31) + " | " 
                    + "{INPUT_FILE_NAME/INPUT_URL}".PadRight(27) + " => { RESULT_URL}" + Environment.NewLine
                    + "IF ERROR: {TIMESTAMP_UTC}".PadRight(28) + " | " 
                    + "{SHORTEN/UPLOAD} {ERROR_CODE}".PadRight(31) + " | " 
                    + "{INPUT_FILE_NAME/INPUT_URL}".PadRight(27) + " | {ERROR_RESPONSE}" + Environment.NewLine
                    );
            }
        }


        /// <summary>
        /// Asynchronously uploads a file to whats-th.is
        /// </summary>
        /// <param name="s">Stream containing the file to upload.</param>
        /// <param name="filename">Name of the file to upload.</param>
        /// <returns>Response from whats-th.is</returns>
        public async Task<string> UploadFileAsync(Stream s, string filename, IUser user)
        {
            var dl = s.Length - s.Position;
            if (dl > 20 * 1024 * 1024 || dl < 0)
                throw new ArgumentException("The data needs to be less than 20MiB and greather than 0B long.");

            var b64data = new byte[8];
            var rnd = new Random();
            for (var i = 0; i < b64data.Length; i++)
                b64data[i] = (byte)rnd.Next();

            OwOObjects.Config conf;

            var queryStrings = new Dictionary<string, string>
            {
                ["key"] = GetToken(user, out conf)
            };

            var baseUri = new Uri(ApiUrlStrings.Intergrations.OwO.UploadFileEndpoint);
            var uri = baseUri.AttachParameters(queryStrings);

            var req = new HttpRequestMessage(HttpMethod.Post, uri);
            var mpd = new MultipartFormDataContent(string.Concat("---upload-", Convert.ToBase64String(b64data), "---"));

            var bdata = new byte[dl];
            await s.ReadAsync(bdata, 0, bdata.Length);

            var fn = Path.GetFileName(filename);
            var sc = new ByteArrayContent(bdata);
            sc.Headers.ContentType = new MediaTypeHeaderValue(MimeTypeMap.GetMimeType(Path.GetExtension(fn)));

            mpd.Add(sc, "files[]", string.Concat("\"", fn.ToLower(), "\""));

            req.Content = mpd;

            HttpClient client = new HttpClient();

            var res = await client.SendAsync(req);

            var json = await res.Content.ReadAsStringAsync();

            var tfn = JsonConvert.DeserializeObject<OwOObjects.UploadResponse.RootObject>(json);

            if (res.StatusCode == HttpStatusCode.OK)
            {
                string configUrl = StripHTTP(conf.UploadUrl);
                if (!configUrl.EndsWith("/"))
                    configUrl = configUrl + "/";
                string text = tfn.files.FirstOrDefault().url;
                string customRepl = text.Replace(configUrl, text);
                string snipHttps = StripHTTP(customRepl);
                string final = "https://" + configUrl + snipHttps;

                OwOObjects.UploadedFile Log = new OwOObjects.UploadedFile()
                {
                    InputFileName = fn,
                    StatusCode = res.StatusCode,
                    ResultUrl = final,
                    User = user,
                    TimeStamp_Iso = DateTime.UtcNow.ToString(),
                };
                await WriteLog(Log);
                return final;
            }
            else
            {
                string error = await res.Content.ReadAsStringAsync();
                OwOObjects.UploadedFile Log = new OwOObjects.UploadedFile
                {
                    InputFileName = fn,
                    ErrorReason = error,
                    User = user,
                    StatusCode = res.StatusCode,
                    TimeStamp_Iso = DateTime.UtcNow.ToString()
                };
                await WriteLog(Log);
                return $"The API returned an error {Convert.ToInt32(res.StatusCode)}: {res.StatusCode}" + "\n"
                    + $"Error Message: \"`{error}`\"";
            }
                
        }


        /// <summary>
        /// Asynchronously uploads a file to whats-th.is
        /// </summary>
        /// <param name="fs">Stream containing the file to upload.</param>
        /// <returns>Response from whats-th.is</returns>
        public Task<string> UploadFileAsync(FileStream fs, IUser user, string fname) =>
            this.UploadFileAsync(fs, fname, user);
    }
}
