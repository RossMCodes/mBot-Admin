﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;
using System.Threading;
using System.Globalization;
using mBot_Admin.Objects.MemeObjects;
using mBot_Admin.ConfigService;
using Newtonsoft.Json;

namespace mBot_Admin.Services.MemeService
{
    public class MemeService
    {

        /* Load JSON:
         *  string json = File.ReadAllText(@"BotSystem\Data\Memes\memes.json");
         *  MemeObjects.RootObject root = JsonConvert.DeserializeObject<MemeObjects.RootObject>(json);
         *  
         *  Save:
         *  File.WriteAllText(@"BotSystem\Data\Memes\memes.json", JsonConvert.SerializeObject(root));
        */

        public async Task getRequestedMeme(ICommandContext Context, int id)
        {
            await Context.Channel.TriggerTypingAsync();
            if (!File.Exists(PathStrings.Memes.ConfigPath))
                throw new System.Exception("My meme library needs to be updated. Use /support for help if you believe this to be in error.");
            string json = File.ReadAllText(PathStrings.Memes.ConfigPath);
            MemeObjects.RootObject root = JsonConvert.DeserializeObject<MemeObjects.RootObject>(json);
            List<MemeObjects.Meme> list = root.memes;
            var meme = list.Find(f => f.FileID == id);
            if (meme == null)
                throw new System.Exception("Meme Not Found");
            list.Find(f => f.FileID == id).RequestedAccessCount++;
            await Context.Channel.SendFileAsync(meme.Path, $"{Context.User.Mention} Here is the meme matching [ID = {id}]");
            File.WriteAllText(PathStrings.Memes.ConfigPath, JsonConvert.SerializeObject(root));
        }

        public async Task upgradeDB(ICommandContext Context)
        {
            MemeObjects.RootObject root = new MemeObjects.RootObject();
            root.memes = new List<MemeObjects.Meme>();
            root.pendingAdminMemes = new List<MemeObjects.Meme>();
            root.pendingMemes = new List<MemeObjects.Meme>();
            root.lastId = Convert.ToInt32(File.ReadAllText(@"BotSystem\Data\Memes\Numbers.txt"));
            root.lastAdminId = Convert.ToInt32(File.ReadAllText(@"BotSystem\Data\Memes\AdminNumbers.txt"));
            var owner = (await Context.Client.GetApplicationInfoAsync()).Owner;
            if (!File.Exists(@"BotSystem\Data\Memes\memePaths.txt"))
            {
                await Context.Channel.SendMessageAsync("No prod DB found");
                goto pendDB;
            }
            string[] allProdMemePaths = File.ReadAllLines(@"BotSystem\Data\Memes\memePaths.txt");
            foreach (string x in allProdMemePaths)
            {
                var y = x.Split('\\');
                string fname = y[y.Length - 1];
                int id = Convert.ToInt32(fname.Split('.')[0]);
                var meme = createNewMemeObject(owner, id, fname, x);
                root.memes.Add(meme);
            }
            pendDB:;
            if (!File.Exists(@"BotSystem\Res\Memes\Pend\Sent\memePaths.txt"))
            {
                await Context.Channel.SendMessageAsync("No pend DB found");
            }
            string[] allPendMemePaths = File.ReadAllLines(@"BotSystem\Res\Memes\Pend\Sent\memePaths.txt");
            foreach (string x in allPendMemePaths)
            {
                var y = x.Split('\\');
                string fname = y[y.Length - 1];
                int id = Convert.ToInt32(fname.Split('.')[0]);
                var meme = createNewMemeObject(owner, id, fname, x);
                root.pendingMemes.Add(meme);
            }
            File.WriteAllText(PathStrings.Memes.ConfigPath, JsonConvert.SerializeObject(root));
            await Context.Channel.SendMessageAsync("Yay!");
        }

        public async Task memeIndex(ICommandContext Context)
        {
            if (!File.Exists(PathStrings.Memes.ConfigPath))
            {
                await Context.Channel.SendMessageAsync("My meme library needs to be updated. Use /support for help if you believe this to be in error.");
                return;
            }
            string json = File.ReadAllText(PathStrings.Memes.ConfigPath);
            MemeObjects.RootObject root = JsonConvert.DeserializeObject<MemeObjects.RootObject>(json);
            int AvaNum = root.memes.Count;
            int PendNum = root.pendingMemes.Count;
            var application = await Context.Client.GetApplicationInfoAsync();
            EmbedBuilder emMemeIndex = new EmbedBuilder()
            {
                Title = "Memes",
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                //ThumbnailUrl = user.GetAvatarUrl(),
                Color = new Discord.Color(240, 71, 71),
                Description = $"Current status of memes:"
            };

            EmbedFieldBuilder efbMemeTotal = new EmbedFieldBuilder()
            {
                Name = "Total Memes Availiable",
                Value = $"{AvaNum}",
                IsInline = true
            };
            emMemeIndex.AddField(efbMemeTotal);

            EmbedFieldBuilder efbMemeTotalPending = new EmbedFieldBuilder()
            {
                Name = "Total Memes Pending Approval",
                Value = $"{PendNum}",
                IsInline = true
            };
            emMemeIndex.AddField(efbMemeTotalPending);

            await Context.Channel.SendMessageAsync("", embed: emMemeIndex);
        }

        public async Task rndMeme(ICommandContext Context)
        {
            if (!File.Exists(PathStrings.Memes.ConfigPath))
                throw new System.Exception("My meme library needs to be updated. Use /support for help if you believe this to be in error.");

            var post1 = await Context.Channel.SendMessageAsync("Choosing a meme...");
            string json = File.ReadAllText(PathStrings.Memes.ConfigPath);
            MemeObjects.RootObject root = JsonConvert.DeserializeObject<MemeObjects.RootObject>(json);
            List<MemeObjects.Meme> list = root.memes;
            Random rnd1 = new Random();
            int cnt = list.Count;
            int num = rnd1.Next(cnt);
            MemeObjects.Meme chosenMeme = list[num];
            await Context.Channel.SendFileAsync(chosenMeme.Path, $"{Context.User.Mention}, Here is your meme! ID: {chosenMeme.FileID}");
            await post1.DeleteAsync();
            root.memes[num].RandomAccessedCount++;
            File.WriteAllText(PathStrings.Memes.ConfigPath, JsonConvert.SerializeObject(root));
        }

        public async Task addMeme(ICommandContext Context, string imgurl, string fmat = null)
        {
            try
            {
                System.Drawing.Imaging.ImageFormat format = ImgFormat(fmat, Context);
                string extention = ImgExt(format);
                Random rand = new Random();
                WebClient client = new WebClient();
                string json = File.ReadAllText(PathStrings.Memes.ConfigPath);
                MemeObjects.RootObject root = JsonConvert.DeserializeObject<MemeObjects.RootObject>(json);

                int hsh = (root.lastId + 1);
                root.lastId = hsh;

                byte[] data = client.DownloadData(imgurl);
                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var Image = System.Drawing.Image.FromStream(mem))
                    {
                        Image.Save(@"BotSystem\Res\Memes\Pend\Sent\" + hsh.ToString() + $".{extention}", format);
                    }
                }
                string path = @"BotSystem\Res\Memes\Pend\Sent\" + hsh.ToString() + $".{extention}";
                var obj = createNewMemeObject(Context.User, hsh, $"{hsh.ToString()}.{extention}", path);
                root.pendingMemes.Add(obj);
                File.WriteAllText(PathStrings.Memes.ConfigPath, JsonConvert.SerializeObject(root));

                client.Dispose();
                var application = await Context.Client.GetApplicationInfoAsync();
                Console.WriteLine(path);
                await Context.Channel.SendMessageAsync($"Done. Waiting to be approved by {application.Owner.Mention}. Image Name for reference: `{hsh.ToString()}.{extention}`");
            }
            catch (System.Exception e)
            {
                Rollbar.Report(e);
                string str = e.Message;
                if (str.StartsWith("The process cannot access the file"))
                {
                    await Context.Channel.SendMessageAsync("***Bzzt*** An error occurred ***Boop***. Message: Error writing to file. If the problem persists, send an email to support@marens101.com or use /support to join the support server.");
                }
                else
                {
                    await Context.Channel.SendMessageAsync($"***Bzzt*** An error occurred ***Boop***. Message: {e.Message}");
                }
            }
        }

        public async Task uploadMeme(ICommandContext Context)
        {
            var attachments = Context.Message.Attachments;
            foreach (IAttachment a in attachments)
            {

                bool isImg = IsImageUrl(a.Url);
                if (!isImg)
                {
                    await Context.Channel.SendMessageAsync($"{a.Filename} is an invalid image.");
                    continue;
                }
                var names = a.Filename.Split('.');
                var typein = names[names.Length - 1];
                System.Drawing.Imaging.ImageFormat format = ImgFormat(typein, Context);
                string extention = ImgExt(format);

                WebClient client = new WebClient();

                string json = File.ReadAllText(PathStrings.Memes.ConfigPath);
                MemeObjects.RootObject root = JsonConvert.DeserializeObject<MemeObjects.RootObject>(json);

                int hsh = (root.lastId + 1);
                root.lastId = hsh;

                byte[] data = client.DownloadData(a.Url);
                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var Image = System.Drawing.Image.FromStream(mem))
                    {
                        Image.Save(@"BotSystem\Res\Memes\Pend\Sent\" + hsh.ToString() + $".{extention}", format);
                    }
                }
                string path = @"BotSystem\Res\Memes\Pend\Sent\" + hsh.ToString() + $".{extention}";
                var obj = createNewMemeObject(Context.User, hsh, $"{hsh.ToString()}.{extention}", path);
                root.pendingMemes.Add(obj);
                File.WriteAllText(PathStrings.Memes.ConfigPath, JsonConvert.SerializeObject(root));
                client.Dispose();
                var application = await Context.Client.GetApplicationInfoAsync();
                Console.WriteLine(path);
                await Context.Channel.SendMessageAsync($"`{a.Filename}` downloaded with format {extention}. Waiting to be approved by {application.Owner.Mention}. Image Name for reference: `{hsh.ToString()}.{extention}`");
            }
        }

        public async Task addAdminMeme(ICommandContext Context, [Summary("URL of Image")] string imgurl, string fmat = null)
        {
            try
            {
                System.Drawing.Imaging.ImageFormat format = ImgFormat(fmat, Context);
                string extention = ImgExt(format);
                Random rand = new Random();
                WebClient client = new WebClient();

                string json = File.ReadAllText(PathStrings.Memes.ConfigPath);
                MemeObjects.RootObject root = JsonConvert.DeserializeObject<MemeObjects.RootObject>(json);

                int hsh = (root.lastAdminId + 1);
                root.lastAdminId = hsh;

                byte[] data = client.DownloadData(imgurl);
                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var yourImage = System.Drawing.Image.FromStream(mem))
                    {
                        // If you want it as Png
                        yourImage.Save(@"BotSystem\Res\Memes\Pend\" + hsh.ToString() + $".{extention}", format);
                    }
                }
                string path = @"BotSystem\Res\Memes\Pend\" + hsh.ToString() + $".{extention}";
                var obj = createNewMemeObject(Context.User, hsh, $"{hsh.ToString()}.{extention}", path);
                root.pendingAdminMemes.Add(obj);
                File.WriteAllText(PathStrings.Memes.ConfigPath, JsonConvert.SerializeObject(root));
                client.Dispose();
                var application = await Context.Client.GetApplicationInfoAsync();
                Console.WriteLine(path);
                await Context.Channel.SendMessageAsync($"Done. Image: `{hsh.ToString()}.{extention}`");
            }
            catch (System.Exception e)
            {
                Rollbar.Report(e);
                string str = e.Message;
                if (str.StartsWith("The process cannot access the file"))
                {
                    await Context.Channel.SendMessageAsync($"***Bzzt*** An error occurred ***Boop***. Message: Error writing to file. If the problem persists, send an email to support@marens101.com or use `/support` to join the support server.");
                }
                else
                {
                    await Context.Channel.SendMessageAsync($"***Bzzt*** An error occurred ***Boop***. Message: {e.Message}. If the problem persists, send an email to support@marens101.com or use `/support` to join the support server.");
                }
            }

        }

        public async Task uploadAdminMeme(ICommandContext Context)
        {
            var attachments = Context.Message.Attachments;
            foreach (IAttachment a in attachments)
            {

                bool isImg = IsImageUrl(a.Url);
                if (!isImg)
                {
                    await Context.Channel.SendMessageAsync($"{a.Filename} is an invalid image.");
                    continue;
                }


                var names = a.Filename.Split('.');
                var typein = names[names.Length - 1];

                System.Drawing.Imaging.ImageFormat format = ImgFormat(typein, Context);
                string extention = ImgExt(format);



                WebClient client = new WebClient();
                
                string json = File.ReadAllText(PathStrings.Memes.ConfigPath);
                MemeObjects.RootObject root = JsonConvert.DeserializeObject<MemeObjects.RootObject>(json);

                int hsh = (root.lastAdminId + 1);
                root.lastAdminId = hsh;

                byte[] data = client.DownloadData(a.Url);
                using (MemoryStream mem = new MemoryStream(data))
                {
                    using (var Image = System.Drawing.Image.FromStream(mem))
                    {
                        Image.Save(@"BotSystem\Res\Memes\Pend\Sent\" + hsh.ToString() + $".{extention}", format);
                    }
                }
                string path = @"BotSystem\Res\Memes\Pend\Sent\" + hsh.ToString() + $".{extention}";
                var obj = createNewMemeObject(Context.User, hsh, $"{hsh.ToString()}.{extention}", path);
                root.pendingMemes.Add(obj);
                File.WriteAllText(PathStrings.Memes.ConfigPath, JsonConvert.SerializeObject(root));
                client.Dispose();
                var application = await Context.Client.GetApplicationInfoAsync();
                Console.WriteLine(path);
                await Context.Channel.SendMessageAsync($"`{a.Filename}` downloaded as `{hsh.ToString()}.{extention}`");
            }
        }

        public MemeObjects.Meme createNewMemeObject(IUser user, int memeId, string filename, string path)
        {
            MemeObjects.Meme meme = new MemeObjects.Meme();
            meme.Owner = new MemeObjects.Owner();
            meme.Owner.Discrim = user.DiscriminatorValue;
            meme.Owner.Id = user.Id;
            meme.Owner.Username = user.Username;
            meme.FileID = memeId;
            meme.Filename = filename;
            meme.Path = path;
            meme.RandomAccessedCount = 0;
            meme.RequestedAccessCount = 0;
            return meme;
        } 

        public bool IsImageUrl(string URL)
        {
            var req = (HttpWebRequest)WebRequest.Create(URL);
            req.Method = "HEAD";
            using (var resp = req.GetResponse())
            {
                return resp.ContentType.ToLower(CultureInfo.InvariantCulture)
                           .StartsWith("image/");
            }
        }

        public System.Drawing.Imaging.ImageFormat ImgFormat(string fmatIn, ICommandContext Context)
        {
            if (fmatIn == null)
            {
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            }
            string fmat = fmatIn.ToUpper();
            if (fmat == "JPG" || fmat == "JPEG" || fmat == "JOINT PHOTOGRAPHIC EXPERTS GROUP" || fmat == "JOINT PHOTOGRAPHICS EXPERTS GROUP")
            {
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            }
            else if (fmat == "GIF" || fmat == "GRAPHIC INTERCHANGE FORMAT" || fmat == "GRAPHICS INTERCHANGE FORMAT")
            {
                return System.Drawing.Imaging.ImageFormat.Gif;
            }
            else if (fmat == "PNG" || fmat == "PORTABLE NETWORK GRAPHIC" || fmat == "PORTABLE NETWORK GRAPHICS")
            {
                return System.Drawing.Imaging.ImageFormat.Png;
            }
            else if (fmat == "BMP" || fmat == "BITMAP")
            {
                return System.Drawing.Imaging.ImageFormat.Bmp;
            }
            else if (fmat == null)
            {
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            }
            else
            {
                Context.Channel.SendMessageAsync("Invalid Image Format Specified, Defaulting to JPEG");
                return System.Drawing.Imaging.ImageFormat.Jpeg;
            }
        }

        public string ImgExt(System.Drawing.Imaging.ImageFormat ImgFormat)
        {
            if (ImgFormat == System.Drawing.Imaging.ImageFormat.Jpeg)
            {
                return "jpeg";
            }
            else if (ImgFormat == System.Drawing.Imaging.ImageFormat.Gif)
            {
                return "gif";
            }
            else if (ImgFormat == System.Drawing.Imaging.ImageFormat.Png)
            {
                return "png";
            }
            else if (ImgFormat == System.Drawing.Imaging.ImageFormat.Bmp)
            {
                return "bmp";
            }
            else
            {
                return "jpg";
            }
        }
    }

    public class MemeMinerService
    {
        public async Task MineSumMemes(ICommandContext Context, int times, string par1, string par2, string par3, string par4, string par5)
        {
            int doneTimes = 1;
            await Context.Channel.SendMessageAsync("!mine");
            while (times > doneTimes)
            {
                await Task.Delay(65000);
                await Context.Channel.SendMessageAsync("!mine");
                doneTimes++;
            }
            if (par1 != null)
            {
                await Task.Delay(5000);
                await Context.Channel.SendMessageAsync("!" + par1);
            }
            if (par2 != null)
            {
                await Task.Delay(5000);
                await Context.Channel.SendMessageAsync("!" + par2);
            }
            if (par3 != null)
            {
                await Task.Delay(5000);
                await Context.Channel.SendMessageAsync("!" + par3);
            }
            if (par4 != null)
            {
                await Task.Delay(5000);
                await Context.Channel.SendMessageAsync("!" + par4);
            }
            if (par5 != null)
            {
                await Task.Delay(5000);
                await Context.Channel.SendMessageAsync("!" + par5);
            }
        }

        public async Task CheckMemes(ICommandContext Context)
        {
            await Context.Channel.SendMessageAsync("!memes");
        }

        public async Task CheckUnits(ICommandContext Context)
        {
            await Context.Channel.SendMessageAsync("!units");
        }

        public async Task CheckMine(ICommandContext Context)
        {
            await Context.Channel.SendMessageAsync("!check");
        }

        public async Task CollectMine(ICommandContext Context)
        {
            await Context.Channel.SendMessageAsync("!collect");
        }

        public async Task GiveMemes(ICommandContext Context, long Amnt, IUser usr)
        {
            await Context.Channel.SendMessageAsync($"!tip {Amnt.ToString()} {usr.Mention}");
        }

        public async Task Buy(ICommandContext Context, string itm, int amnt)
        {
            var amnnt = Convert.ToString(amnt);
            string amt = IsMax(amnnt);
            await Context.Channel.SendMessageAsync($"!buy {amt} {itm}");
        }



        static string IsMax(string amnt)
        {
            if (amnt == "0")
            {
                return "max";
            }
            else
            {
                return amnt;
            }
        }
    }
}
