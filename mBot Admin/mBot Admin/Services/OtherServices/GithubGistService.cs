﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mBot_Admin.Objects.OtherObjects.GitHubObjects;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace mBot_Admin.Services.OtherServices
{
    public class GithubGistService
    {

        public async Task<GistObjects.InBoundGistObjects.RootObject> CreateAuthenticatedGist(GistObjects.GistCreationObjects.RootObject GistCreationObject)
        {
            var OutBoundGistObject = new GistObjects.OutBoundGistObjects.RootObject(GistCreationObject);

            String url = "https://api.github.com/gists/";
            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;

            var creds = new GitHubCredentialsObject(false);
            req.Method = "POST";
            req.ContentType = "application/json";
            req.Headers.Add($"Authorization: token {creds.PasswordOrAccessToken}");
            //req.Headers.Add("Authorization", "Basic " + creds.AuthString);
            HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
            StreamReader reader = new StreamReader(resp.GetResponseStream());
            var json = reader.ReadToEnd();
            var outp = JsonConvert.DeserializeObject<GistObjects.InBoundGistObjects.RootObject>(json);
            return outp;

        }
    }
}
