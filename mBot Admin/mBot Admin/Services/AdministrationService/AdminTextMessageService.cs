﻿using Discord;
using Discord.Commands;
using mBot_Admin._System;
using mBot_Admin._System.ExtensionMethods;
using mBot_Admin.ConfigService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TextmagicRest;
using TextmagicRest.Model;

namespace mBot_Admin.Services.AdministrationService
{
    class AdminTextMessageService
    {
        /// <summary>
        /// Sends a text to the mobile numbers provided in the `string[] to` paramater
        /// </summary>
        /// <param name="Context">An ICommandContext object</param>
        /// <param name="msg">The message to send</param>
        /// <param name="to">An array of strings representing the numbers to send to</param>
        /// <param name="usr">The user initiating the action, if blank defaults to Context.User</param>
        /// <returns>A SendingResult object from the server. Don't forget to check bool ErrorResult.Success!</returns>
        public async Task<SendingResult> SendTextAsync(ICommandContext Context, string msg, string[] to, IUser usr = null)
        {
            IUser performer = usr ?? Context.User;

            await LogSendRequest(Context, performer, to, msg);

            if (!performer.IsOwner())
                throw new System.Exception("You aren't authorised to initiate the TextingService! Only Bot Administrators can do that!");

            string Token = TokenService.TextMagicToken();
            TextmagicRest.Client TxtClient = new TextmagicRest.Client("marens101", Token);
            SendingOptions optns = new SendingOptions
            {
                From = "M101",
                Text = msg,
                Phones = to,
            };
            var result = TxtClient.SendMessage(optns);
            if (result.Success)
            {
                Console.WriteLine($"Message ID {result.Id} has been successfully sent");
                return result;
            }
            else
            {
                await ErrLog.WriteLine($"Message was not sent due to following exception: {result.ClientException.Message}");
                ErrorHandling.Rollbar.Report(result.ClientException);
                return result;
                
            }
        }

        public async Task LogSendRequest(ICommandContext Context, IUser performer, string[] Array, string msg)
        {
            string csv = ArrayToCSV(Array);

            string alrt =
                $"    !ALERT!                                 !ALERT!: Text Sending by {performer.Username}#{performer.Discriminator}" + Environment.NewLine
                + $"    !ALERT!                                 !ALERT!: Text Sending to {csv}" + Environment.NewLine
                + $"    !ALERT!                                 !ALERT!: Text Sending content \"{msg}\""
                ;

            Console.Beep();
            Console.WriteLine(alrt);
            Console.Beep();
            await SysLog.WriteLine(alrt);
            Console.Beep();
            //await Sys.Log.Texting($"    !ALERT!                                 !ALERT!: Text Sending by {Context.User.Username}#{Context.User.Discriminator}");
            await new NotificationService().OnSendTextRequest(Context, csv, msg, performer);
        }

        string ArrayToCSV(string[] array)
        {
            string to = "";
            foreach (string s in array)
            {
                to = to + s + ", ";
            }
            string rtn = to.Trim();
            rtn = rtn.Trim(',');
            rtn = rtn.Trim();
            return rtn;
        }

        
    }
    public static class MobileAddressBook
    {
        public static Dictionary<string, Entry> Load()
        {
            if (File.Exists(PathStrings.MobileAddressBook))
            {
                string json = File.ReadAllText(PathStrings.MobileAddressBook);
                var dict = JsonConvert.DeserializeObject<Dictionary<string, Entry>>(json);
                return dict;
            }
            else
                return new Dictionary<string, Entry>();
        }

        public static void Save(Dictionary<string, Entry> dict)
        {
            string json = JsonConvert.SerializeObject(dict);
            File.WriteAllText(PathStrings.MobileAddressBook, json);
        }

        public static Entry CreateEntry(string number, IUser user)
        {
            Entry entry = new Entry
            {
                Number = number,
                UserDiscrim = user.DiscriminatorValue,
                UserId = user.Id,
                UserName = user.Username
            };
            return entry;
        }

        public class Entry
        {
            public string Number;
            public ulong UserId;
            public string UserName;
            public int UserDiscrim;
        }

        public static bool TryGetEntry(this Dictionary<string, Entry> dict, IUser user, out Entry entry)
        {
            Entry x;
            bool res = dict.TryGetValue($"{user.Id}", out entry);
            return res;
        }
    }
}
