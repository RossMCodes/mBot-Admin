﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Services.AdministrationService
{
    public class NotificationService
    {
        public async Task OnReady(DiscordSocketClient client)
        {
            string msg = new NotifMsgService().BotStartMessage(client);
            await SendDM(client, msg);
        }

        public async Task OnJoinGuild(DiscordSocketClient client, SocketGuild JoinedGuild)
        {
            string msg = new NotifMsgService().JoinGuildMessage(client, JoinedGuild);
            await SendDM(client, msg);
        }

        public async Task OnSendTextRequest(ICommandContext Context, string CSVTo, string msg, IUser initiator)
        {
            DiscordSocketClient client = Context.Client as DiscordSocketClient;
            EmbedBuilder eb = new NotifMsgService().SendTextRequestMesssage(Context, CSVTo, msg, initiator);
            await SendDM(client, "", eb.Build());
        }

        public async Task SendDM(DiscordSocketClient client, string msg, Embed embed = null)
        {
            var application = await client.GetApplicationInfoAsync();
            var DMChannel = await application.Owner.GetOrCreateDMChannelAsync();

            await DMChannel.SendMessageAsync(msg, embed: embed);
        }
    }

    public class NotifMsgService
    {
        public string BotStartMessage(DiscordSocketClient client)
        {
            SocketSelfUser BotUser = client.CurrentUser;
            string rtn =
                "<:statusGreen:317202376991703041> Bot Started Up :thumbsup:" + "\n"
                + "Username#Discrim: " + BotUser.Username + "#" + BotUser.Discriminator + "\n"
                + "Client ID: " + BotUser.Id + "\n"
                + "Guild Count: " + client.Guilds.Count + "\n"
                + "System Time: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm.ss") // + "\n"

                //+ "" + "\n"
                //+ ""
                ;

            return rtn;
        }

        public string JoinGuildMessage(DiscordSocketClient client, SocketGuild Guild)
        {
            IUser Owner = Guild.Owner;
            string rtn =
                "<:statusGreen:317202376991703041> Bot Joined Guild :thumbsup:" + "\n"
                + "Guild Name: " + Guild.Name + "\n"
                + "Guild Owner Name: " + Owner.Username + "#" + Owner.Discriminator + "\n"
                + "Guild Owner Id: " + Owner.Id + "\n"
                + "Guild ID: " + Guild.Id + "\n"
                + "Member Count: " + Guild.MemberCount + "\n"
                + "Created At: " + Guild.CreatedAt.ToLocalTime().ToString("yyyy-MM-dd HH:mm.ss") + "\n"
                + "System Time: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm.ss") // + "\n"

                //+ "" + "\n"
                //+ ""
                ;

            return rtn;
        }

        public EmbedBuilder SendTextRequestMesssage(ICommandContext Context, string CSVTo, string msg, IUser initiator)
        {
            EmbedFieldBuilder EFB(string Name, object Value, bool Inline = false)
            {
                return new EmbedFieldBuilder
                {
                    Name = Name,
                    Value = Value,
                    IsInline = Inline
                };
            }

            EmbedBuilder b = new EmbedBuilder()
            {
                Title = ":warning: User Requested Send Text Message :warning:",
                Timestamp = DateTime.UtcNow,
                Fields = new List<EmbedFieldBuilder>
                {
                    EFB("User Name", initiator.Username + "#" + initiator.Discriminator),
                    EFB("User Id", initiator.GetAvatarUrl()),
                    EFB("User Avatar", initiator.Id),
                    EFB("Guild Name", Context.Guild.Name),
                    EFB("Guild ID", Context.Guild.Id),
                    EFB("Target Numbers", CSVTo),
                    EFB("Message", msg),
                    EFB("System Time", DateTime.Now.ToString("yyyy-MM-dd HH:mm.ss")),
                },
                Color = Color.Red,
            };
            return b;
        }
    }
}
