﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Concurrent;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.AdminModule;
//using mBot_Admin.Modules.Audio;
using mBot_Admin.Modules.SystemModule;
using mBot_Admin.Modules.EvalModule;
//using mBot_Admin.Modules.Guild;
using mBot_Admin.Modules.HelpModule;
using mBot_Admin.Modules.MainModule;
//using mBot_Admin.Modules.Memes;
//using mBot_Admin.Modules.Moderation;
//using mBot_Admin.Modules.Starboard;
//using mBot_Admin.Modules.Tags;
//using mBot_Admin.Modules.Test;
using mBot_Admin.Services.AdministrationService;
using System.Web.Script.Serialization;
using SharpRaven;
using SharpRaven.Data;
using mBot_Admin.Services.OnlineStatsService;
using mBot_Admin.Services.SetupService;
using mBot_Admin.EventHandlers;
using mBot_Admin.ConfigService;
using mBot_Admin._System;
using mBot_Admin._System.ExtensionMethods;
using System.Web.Http.SelfHost;
using System.Web.Http;

namespace mBot_Admin
{

    class Program
    {
        private DiscordSocketClient client;
        private CommandService commands;
        //private DependencyMap map;
        private IGuildUser iguildusr;
        //private MusicService musicService;
        Random rand;


        private IServiceProvider _services;
        private CommandService _commands;


        public string runTimeDateSort = DateTime.Now.ToString("yyyy-MM-dd t HH-mm-ss tt");
        public static DateTime runDateTime = DateTime.Now;
        static void Main(string[] args) => new Program().Run().GetAwaiter().GetResult();


        //File.WriteAllText("SomeFile.Txt", JsonConvert.SerializeObject(dictionary));

        public async Task Run()
        {
            ErrorHandling.Rollbar.Init();
            ErrorHandling.Sentry.Init();
            var ravenClient = ErrorHandling.Sentry.Client;

            SessionConfigs.Init();

            await new SetupFolderService().SetupFolders();
            await new SetupFileService().SetupFiles();

            await new InitConfigsService().InitConfigs();

            if (!File.Exists(@"BotSystem\Sys\Token.token"))
            {
                var TokenNotFoundException = new System.Exception("Token Not Found");
                var level = ErrorHandling.ErrorLevel.Critical;
                ErrorHandling.Sentry.Report(TokenNotFoundException, level);
                ErrorHandling.Rollbar.Report(TokenNotFoundException, level);
                throw TokenNotFoundException;
            }


            var WebApiConfig = new HttpSelfHostConfiguration("http://0.0.0.0:8080");
            WebApiConfig.Routes.MapHttpRoute(
                "API Default", "api/{controller}/{action}/",
                new { id = RouteParameter.Optional });
            HttpSelfHostServer WebApiServer = new HttpSelfHostServer(WebApiConfig);
            WebApiServer.OpenAsync().Wait();

            await SysLog.WriteLine($"Bot started up at {DateTime.Now}");
            await SesLog.WriteLine($"Bot started up at {DateTime.Now}");
            client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                LogLevel = LogSeverity.Verbose,
                //AudioMode = AudioMode.Outgoing,
            });

            
            client.Log += async (message) =>
            {
                Console.WriteLine(message.ToString());
                await SesLog.WriteLine("	" + DateTime.Now + " | " + message.ToString());

                if (message.Severity == LogSeverity.Warning || message.Severity == LogSeverity.Error || message.Severity == LogSeverity.Critical)
                    await ErrLog.WriteLine("	" + DateTime.Now + " | " + message.ToString());

                //return Task.CompletedTask;
            };



            commands = new CommandService();
            string token = TokenService.Token();
            Console.WriteLine(token);
            await client.LoginAsync(TokenType.Bot, token);
            await client.StartAsync();
            //var map = new DependencyMap();
            await InstallCommands();
            //map.Add(musicService);
            //await client.SetGameAsync("support@marens101.com", null, StreamType.Twitch);
            //await client.SetGameAsync("Admin Stuff");
            //await client.SetStatusAsync(UserStatus.Online);

            client.Ready += async () =>
            {
                SysLog.WriteLine("  " + $"----> Connected at {DateTime.Now} <----");
                SesLog.WriteLine("  " + $"----> Connected at {DateTime.Now} <----");
                await client.SetGameAsync("/help | Made by @marens101#4158");
                await new Config().Init(client);

                await new NotificationService().OnReady(client);

                //await new DiscordBotsDotOrgService().PostStats(client);

            };

            client.GuildAvailable += async (GuildAvailable) =>
            {
                var tryguildid = Convert.ToString(GuildAvailable.Id);
                if (tryguildid == "291819411814219776")
                {
                    await client.SetGameAsync("/help | Made by @marens101#4158");
                }

            };

            client.UserJoined += async (UserJoined) =>
            {
                ulong guildId = UserJoined.Guild.Id;
                if (guildId == 283884399177760768)
                {
                    //The Meme Vault
                    var role = UserJoined.Guild.GetRole(283888076067962880);
                    var syswebhook = new DiscordWebhookClient(333836607629295617, "OZ24AnpzBxaBhZgpqQ2dIgF6GueiwnyTlPF9fC89-noB5eIC5fKH9MqP6TwyXYI7KjCt");
                    var webhook = new DiscordWebhookClient(301660242297749514, "NnwbEQvvpZQSbVl1C8e0dbFh-I9GpoE87ptxtcsL3vdWJ3ZTqaiZhKkYUskHri5Ix_0V");
                    await UserJoined.AddRoleAsync(role);
                    await webhook.SendMessageAsync($"User Joined: {UserJoined.Mention} ({UserJoined.Id})");
                    var dms = await UserJoined.GetOrCreateDMChannelAsync();
                    await dms.SendMessageAsync($"It looks like you joined {UserJoined.Guild.Name}. Send `/login` in the login channel to gain acces to the server.");

                    await syswebhook.SendMessageAsync("AutoRole Performed:" + "\n"
                                                         + $"User: \"{UserJoined.Username}#{UserJoined.Discriminator}\" ({UserJoined.Id})" + "\n"
                                                          + $"Guild: \"{UserJoined.Guild.Name}\" ({UserJoined.Guild.Id})" + "\n"
                                                           + $"Role: \"{role.Name}\" ({role.Id})");
                }
                if (guildId == 349371223291068416)
                {
                    //The Burning Forest
                    var role = UserJoined.Guild.GetRole(350825784514052106);
                    var syswebhook = new DiscordWebhookClient(333836607629295617, "OZ24AnpzBxaBhZgpqQ2dIgF6GueiwnyTlPF9fC89-noB5eIC5fKH9MqP6TwyXYI7KjCt");
                    var webhook = UserJoined.Guild.GetTextChannel(350840970411114496);
                    await UserJoined.AddRoleAsync(role);
                    await webhook.SendMessageAsync($"User Joined: {UserJoined.Mention} ({UserJoined.Id})");
                    var dms = await UserJoined.GetOrCreateDMChannelAsync();
                    await dms.SendMessageAsync($"It looks like you joined {UserJoined.Guild.Name}. Send `/bf login [Grade, e.g. 9] [Full Name e.g. John Smith]` in the login channel to gain acces to the server.");

                    await syswebhook.SendMessageAsync("AutoRole Performed:" + "\n"
                                                         + $"User: \"{UserJoined.Username}#{UserJoined.Discriminator}\" ({UserJoined.Id})" + "\n"
                                                          + $"Guild: \"{UserJoined.Guild.Name}\" ({UserJoined.Guild.Id})" + "\n"
                                                           + $"Role: \"{role.Name}\" ({role.Id})");
                }
                else if (File.Exists(@"BotSystem\Data\Srv\autoRoles.dict"))
                {
                    var dict = new JavaScriptSerializer()
                                    .Deserialize<Dictionary<string, ulong>>(File.ReadAllText(@"BotSystem\Data\Srv\autoRoles.dict"));
                    ulong roleid;
                    bool exists = dict.TryGetValue(guildId.ToString(), out roleid);
                    if (exists)
                    {
                        var role = UserJoined.Guild.GetRole(roleid);
                        await UserJoined.AddRoleAsync(role);
                        var webhook = new DiscordWebhookClient(333836607629295617, "OZ24AnpzBxaBhZgpqQ2dIgF6GueiwnyTlPF9fC89-noB5eIC5fKH9MqP6TwyXYI7KjCt");
                        var guild = UserJoined.Guild;
                        await webhook.SendMessageAsync("AutoRole Performed:" + "\n"
                                                         + $"User: \"{UserJoined.Username}#{UserJoined.Discriminator}\" ({UserJoined.Id})" + "\n"
                                                          + $"Guild: \"{guild.Name}\" ({guild.Id})" + "\n"
                                                           + $"Role: \"{role.Name}\" ({role.Id})");
                    }
                }
            };

            

            client.JoinedGuild += async (SocketGuild JoinedGuild) =>
            {

                await new NotificationService().OnJoinGuild(client, JoinedGuild);

                //await new DiscordBotsDotOrgService().PostStats(client);

            };

            client.Disconnected += async (System.Exception exc) =>
            {
                //Rollbar Report
                try
                {
                    ErrorHandling.ErrorEvent e = new ErrorHandling.ErrorEvent(exc, ErrorHandling.ErrorLevel.Critical);
                    e.RollbarReport();
                }
                catch
                {

                }
                File.Copy(SessionConfigs.SessionLogPath, $@"BotSystem\Logs\Error\SesLog.ErrorAt_{DateTime.Now.ToFileNameFormat()}.log");

                // Starts a new instance of the program itself
                System.Diagnostics.Process.Start(Assembly.GetExecutingAssembly().Location);

                // Closes the current process
                Environment.Exit(0);
            };

            /*client.ReactionAdded += async (ReactionAdded) =>
            {
                await Console.WriteLine("ReactionAdded")
            };*/


            //await client.SetGameAsync("marens101 Admin ver 0.2");

            //await new ReadyEventHandler().Ready();
            //await new ReadyEventHandler().JoinedGuild();

            Console.Title = "marens101 Admin Bot";


            await Task.Delay(-1);
        }



        public async Task InstallCommands()
        {
            // Hook the MessageReceived Event into our Command Handler
            client.MessageReceived += HandleCommand;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }
        public async Task HandleCommand(SocketMessage messageParam)
        {
            var message = messageParam as SocketUserMessage;
            if (message == null) return;
            int argPos = 0;
            if (!(message.HasCharPrefix('/', ref argPos) || message.HasMentionPrefix(client.CurrentUser, ref argPos))) return;
            var context = new CommandContext(client, message);
            var result = await commands.ExecuteAsync(context, argPos, _services);
            if (!result.IsSuccess)
            {
                if (result.ErrorReason != "Unknown command.")
                    await context.Channel.SendMessageAsync($"{result.ErrorReason}");
            }


        }

        public async Task InstallAsync(DiscordSocketClient client)
        {
            // Here, we will inject the ServiceProvider with 
            // all of the services our client will use.
            _services = new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(_commands)
                // You can pass in an instance of the desired type
                .AddSingleton(new RavenClient("https://27607dc410534888b87e2bd78cc23a4c:01939de73d9c411b9cbb7afd75b366e3@sentry.io/232252"))
                // ...or by using the generic method.
                .BuildServiceProvider();
            // ...
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly());
        }

    }



}