﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using mBot_Admin.Templates.EmbedTemplates;

namespace mBot_Admin.Templates.EmbedTemplates
{
    public class JokeEmbed
    {
        public EmbedBuilder newJokeEmbed(IApplication application)
        {
            var auth = new EmbedTemplates.EmbedAuthor();
            EmbedBuilder builder = new EmbedBuilder
            {
                Author = new EmbedAuthor().DefaultEmbedAuthor(application),
                Color = Color.Purple,
                Timestamp = DateTime.Now,


            };
            var emb = builder.Build();
            return builder;
        }
    }
}
