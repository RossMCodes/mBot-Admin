﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;

namespace mBot_Admin.Templates.EmbedTemplates
{
    public class EmbedAuthor
    {
        public Discord.EmbedAuthorBuilder DefaultEmbedAuthor(IApplication application)
        {
            EmbedAuthorBuilder builder = new EmbedAuthorBuilder
            {
                Name = application.Name,
                IconUrl = application.IconUrl,
                Url = "http://rb.marens101.com/m101bot"
            };
            //var auth = builder.Build();
            return builder;
        }
    }
}
