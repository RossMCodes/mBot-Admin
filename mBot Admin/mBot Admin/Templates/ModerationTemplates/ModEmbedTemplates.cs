﻿using Discord;
using Discord.Commands;
using mBot_Admin.ConfigService;
using mBot_Admin.Objects.ModerationObjects;
using mBot_Admin.Templates.EmbedTemplates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Templates.ModerationTemplates
{
    public class ModEmbedTemplates
    {
        public EmbedBuilder PurgeEmbed(IUser mod, int MsgsPurged, string Reason)
        {
            var Application = Config.Application;
            EmbedBuilder emPurge = new EmbedBuilder()
            {
                Title = "Posts Bulk Deleted",
                Author = new EmbedTemplates.EmbedAuthor().DefaultEmbedAuthor(Application),
                ThumbnailUrl = mod.GetAvatarUrl(),
                Color = new Discord.Color(240, 71, 71),
                Description = $"Responsible Moderator: {mod.Username}#{mod.Discriminator}" + "\n" +
                                $"Message Qty: {MsgsPurged}" + "\n" +
                                $"Reason: {Reason}"
            };
            return emPurge;
        }

        public EmbedBuilder BanEmbed(LogObject Obj)
        {
            EmbedBuilder embed = new EmbedBuilder
            {
                Title = "Moderation Ban",
                Author = new EmbedAuthorBuilder
                {
                    Name = Config.Application.Name,
                    IconUrl = Config.Application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                ThumbnailUrl = Obj.UserAvatarUrl,
                Color = new Discord.Color(240, 71, 71),
                Description = $"User: {Obj.User}" + "\n" +
                                $"User ID: {Obj.UserId}" + "\n" +
                                $"Responsible Moderator: {Obj.Mod}" + "\n" +
                                $"Case No#: {Obj.CaseNo}" + "\n" +
                                $"Global Case ID#: {Obj.SysId}" + "\n" +
                                $"Reason: \"{Obj.Reason}\""
            };

            return embed;
        }

    }
}
