﻿using Discord;
using Discord.WebSocket;
using mBot_Admin.ConfigService;
using mBot_Admin.Intergrations.RLME;
using mBot_Admin.mBotAPI.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace mBot_Admin.mBotAPI.Controllers
{
    public class RLMEController : ApiController
    {
        [HttpGet]
        [ActionName("CheckUserIsInGuild")]
        public HttpResponseMessage CheckUserIsInGuild(ulong id, string token)
        {
            TokenProperties tokenprops;
            if (TokenMgmnt.GetToken(token, out tokenprops))
            {
                if (!tokenprops.Perms.RLME.CheckUserIsInGuild)
                    return AccessDenied;
                var usr = Config.Client
                    .GetGuild(RLMEConfig.StaticSav.PublicGuildId)
                    .GetUser(id);
                if(usr == null)
                {
                    return new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
                }
                else
                {
                    return new HttpResponseMessage(System.Net.HttpStatusCode.Found);
                }
            }
            else
            {
                return new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }
        }

        [HttpGet]
        [ActionName("ApproveToken")]
        public async Task<HttpResponseMessage> ApproveToken(ulong id, ulong granter, string token)
        {
            TokenProperties tokenprops;
            if (TokenMgmnt.GetToken(token, out tokenprops))
            {
                if (!tokenprops.Perms.RLME.ApproveDenyTokens)
                    return AccessDenied;
                var guild = Config.Client
                    .GetGuild(RLMEConfig.StaticSav.PublicGuildId);
                var usr = guild.GetUser(id);
                if (usr == null)
                {
                    return new HttpResponseMessage(System.Net.HttpStatusCode.NotFound);
                }
                else
                {
                    var role = guild.GetRole(RLMEConfig.StaticSav.HasRoleId);
                    await usr.AddRoleAsync(role);
                    var modlog = guild.TextChannels.FirstOrDefault(c => c.Id == RLMEConfig.StaticSav.ModLogChanID);
                    await modlog.SendMessageAsync(
                        "A token was approved!" + '\n'
                        + $"Requestor: {usr.Mention}" + '\n'
                        + $"Granter: <@{granter}>");
                    return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                }
            }
            else
            {
                return new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }
        }

        [HttpGet]
        [ActionName("DenyToken")]
        public async Task<HttpResponseMessage> DenyToken(ulong id, string username, int discrim, ulong denier, string token, string reason = null)
        {
            TokenProperties tokenprops;
            if (TokenMgmnt.GetToken(token, out tokenprops))
            {
                if (!tokenprops.Perms.RLME.ApproveDenyTokens)
                    return AccessDenied;
                var guild = Config.Client
                    .GetGuild(RLMEConfig.StaticSav.PublicGuildId);

                var modlog = guild.TextChannels.FirstOrDefault(c => c.Id == RLMEConfig.StaticSav.ModLogChanID);
                await modlog.SendMessageAsync(
                    "A token was denied!" + '\n'
                    + $"Requestor: {username}#{discrim} ({id})" + '\n'
                    + $"Denier: <@{denier}>" + '\n'
                    + $"Reason: {reason ?? "*No reason provided*"}"
                    );
                return new HttpResponseMessage(System.Net.HttpStatusCode.OK);
                
            }
            else
            {
                return AccessDenied;
            }
        }

        public static HttpResponseMessage AccessDenied { get; }
            = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
    }
}
