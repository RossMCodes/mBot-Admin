﻿using mBot_Admin.ConfigService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace mBot_Admin.mBotAPI.Controllers
{
    public class AdministrationController : ApiController
    {
        [HttpGet]
        [ActionName("CreateToken")]
        public string CreateToken(string MasterToken, string Description)
        {
            if (!File.Exists(PathStrings.APIMasterToken))
            {
                return "{\"Success\":\"false\",\"message\":\"Error: The master token could not be found!\"}";
            }

            if (MasterToken == File.ReadAllText(PathStrings.APIMasterToken))
                return JsonConvert.SerializeObject(mBotAPI.Auth.TokenMgmnt.CreateToken(Description));
            return "{\"Success\":\"false\",\"message\":\"Error: Invalid Master Token!\"}";
        }

        [HttpGet]
        [ActionName("RefreshTokenstore")]
        public string CreateToken(string MasterToken)
        {
            if (!File.Exists(PathStrings.APIMasterToken))
            {
                return "{\"Success\":\"false\",\"message\":\"Error: The master token could not be found!\"}";
            }

            if (MasterToken == File.ReadAllText(PathStrings.APIMasterToken))
            {
                mBotAPI.Auth.TokenMgmnt.RefreshTokenStore();
                return "{\"Success\":\"true\",\"message\":\"DB Refreshed Successfully!\"}";
            }
                
            return "{\"Success\":\"false\",\"message\":\"Error: Invalid Master Token!\"}";
        }
    }
}
