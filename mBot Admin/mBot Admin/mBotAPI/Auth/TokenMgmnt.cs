﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using mBot_Admin.ConfigService;
using Newtonsoft.Json;

namespace mBot_Admin.mBotAPI.Auth
{
    public class TokenMgmnt
    {
        /// <summary>
        /// Checks to see if a token exists for mBot API, and if it does return the properties of it.
        /// </summary>
        /// <param name="token">Input token to check</param>
        /// <param name="properties">Output token properties</param>
        /// <returns>Returns true if the token exists, and false if it doesn't</returns>
        public static bool GetToken(string token, out TokenProperties properties)
        {
            if (!File.Exists(PathStrings.APITokens))
            {
                properties = null;
                return false;
            }
                
            string json = File.ReadAllText(PathStrings.APITokens);
            Dictionary<string, TokenProperties> Tokens = JsonConvert
                .DeserializeObject<Dictionary<string, TokenProperties>>(json);
            return Tokens.TryGetValue(token, out properties);
        }

        /// <summary>
        /// Creates a new token with no perms
        /// </summary>
        /// <returns>KeyValuePair with token and perms</returns>
        public static KeyValuePair<string, TokenProperties> CreateToken(string Description)
        {
            Dictionary<string, TokenProperties> Tokens;
            if (File.Exists(PathStrings.APITokens))
            {
                string json = File.ReadAllText(PathStrings.APITokens);
                Tokens = JsonConvert
                    .DeserializeObject<Dictionary<string, TokenProperties>>(json);
            }
            else
            {
                Tokens = new Dictionary<string, TokenProperties>();
            }
            var ntoken = Guid.NewGuid().ToString();
            TokenProperties props = new TokenProperties();
            props.Description = Description;
            Tokens.Add(ntoken, props);
            File.WriteAllText(PathStrings.APITokens, JsonConvert.SerializeObject(Tokens));
            return new KeyValuePair<string, TokenProperties>(ntoken, props);
        }

        /// <summary>
        /// Refreshes the tokenstore, used for when new perms are added to add them to existing tokens with the default values.
        /// </summary>
        public static void RefreshTokenStore()
        {
            string json = File.ReadAllText(PathStrings.APITokens);
            var Tokens = JsonConvert
                .DeserializeObject<Dictionary<string, TokenProperties>>(json);
            File.WriteAllText(PathStrings.APITokens, JsonConvert.SerializeObject(Tokens));
        }
    }
}
