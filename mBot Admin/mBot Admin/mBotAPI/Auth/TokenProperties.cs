﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.mBotAPI.Auth
{
    public class TokenProperties
    {
        public TokenProperties()
        {
            Perms = new TokenPerms();
        }
        public class TokenPerms
        {
            public TokenPerms()
            {
                RLME = new ForRLME();
            }
            public class ForRLME
            {
                public ForRLME()
                {
                    CheckUserIsInGuild = false;
                    ApproveDenyTokens = false;
                }
                public bool CheckUserIsInGuild;
                public bool ApproveDenyTokens;
            }
            public ForRLME RLME;
        }
        public TokenPerms Perms;
        public string Description;
    }
}
