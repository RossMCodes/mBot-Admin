﻿using Discord;
using Discord.Commands;
using mBot_Admin.ConfigService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Preconditions.UserPermissionPreconditions
{
    public class RequireManageGuildAttribute : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            var perms = (Context.User as IGuildUser).GuildPermissions;
            if (perms.ManageGuild)
            {
                return PreconditionResult.FromSuccess();
            }
            else if (Config.Owners.IsOwner(Context.User))
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("You lack the required permission \"Manage Server\"");
                //User requires guild permission ManageGuild
            }
        }
    }

    public class RequireManageChannelsAttribute : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            var perms = (Context.User as IGuildUser).GuildPermissions;
            if (perms.ManageChannels)
            {
                return PreconditionResult.FromSuccess();
            }
            else if (Config.Owners.IsOwner(Context.User))
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("You lack the required permission \"Manage Server\"");
                //User requires guild permission ManageGuild
            }
        }
    }

    public class RequireBanMembersAttribute : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            var perms = (Context.User as IGuildUser).GuildPermissions;
            if (perms.BanMembers)
            {
                return PreconditionResult.FromSuccess();
            }
            else if (Config.Owners.IsOwner(Context.User))
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("You lack the required permission \"Ban Members\"");
                //User requires guild permission ManageGuild
            }
        }
    }

    public class RequireManageRolesAttribute : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            var perms = (Context.User as IGuildUser).GuildPermissions;
            if (perms.ManageRoles)
            {
                return PreconditionResult.FromSuccess();
            }
            else if (Config.Owners.IsOwner(Context.User))
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("You lack the required permission \"Manage Roles\"");
                //User requires guild permission ManageGuild
            }
        }
    }
}
