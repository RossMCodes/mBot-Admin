﻿using Discord.Commands;
using mBot_Admin.ConfigService;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Preconditions.IntergrationPreconditions.OwOPreconditions
{
    public class RequireOwOAccount : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (File.Exists(new PathStrings.UserConfigs().OwOUserConfig(Context.User.Id)))
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("Error: You have not configured and OwO Account! Get started with `/OwO set token [YOUR_TOKEN_HERE]`." + "\n"
                    + "Don't worry! Your token is stored encrypted using a different key and salt for every user!" + "\n"
                    + "Note: Tokens can only be set in DMs for security purposes.");
            }

        }
    }
}
