﻿using Discord;
using Discord.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Modules.RelationshipModules
{
    public class IShipItModule : ModuleBase
    {
        [Command("ship")]
        public async Task Ship(IUser person1, IUser person2 = null)
        {
            // Adapted from https://github.com/ChristopherBThai/Discord-OwO-Bot/blob/master/methods/ship.js
            person2 = person2 ?? Context.User;
            Username u1, u2;
            if(Context.Channel is IGuildChannel)
            {
                u1 = new Username((person1 as IGuildUser).Nickname ?? person1.Username);
                u2 = new Username((person2 as IGuildUser).Nickname ?? person2.Username);
            }
            else
            {
                u1 = new Username(person1.Username);
                u2 = new Username(person2.Username);
            }
            ShipName s = new ShipName(u1, u2);
            await ReplyAsync(u1.Name + " + " + u2.Name + " = " + s.Name + '\n'
                 + "I SHIP IT!!!");
        }
        class Username
        {
            public Username(string username)
            {
                this.Name = username;
                char[] vowels = { 'a', 'e', 'i', 'o', 'u', 'y' };
                mid = username.Length / 2 - 1;
                int i;
                c1 = -1;
                c2 = -1;
                for (i = mid; i >= 0; i--)
                {
                    c1++;
                    if (vowels.Contains(username.ToLower()[i]))
                    {
                        i = -1;
                    }
                    else if (i == 0)
                    {
                        nv1 = true;
                    }
                }
                for (i = mid; i < username.Length; i++)
                {
                    c2++;
                    if (vowels.Contains(username.ToLower()[i]))
                    {
                        i = username.Length; ;
                    }
                    else if (i == username.Length - 1)
                    {
                        nv2 = true;
                    }
                }

            }
            public string Name;
            public bool nv1;
            public bool nv2;
            public int mid;
            public int c1;
            public int c2;
        }
        class ShipName
        {
            public ShipName(Username p1, Username p2)
            {
                if (p1.nv1 && p2.nv2)
                {
                    Part1 = p1.Name.Substring(0, p1.mid + 1);
                    Part2 = p2.Name.Substring(p2.mid);
                }
                else if (p1.c1 <= p2.c2)
                {
                    Part1 = p1.Name.Substring(0, p1.mid - p1.c1 + 1);
                    Part2 = p2.Name.Substring(p2.mid);
                }
                else
                {
                    Part1 = p1.Name.Substring(0, p1.mid + 1);
                    Part2 = p2.Name.Substring(p2.mid + p2.c2);
                }
                Name = Part1 + Part2;
            }
            public string Name;
            public string Part1;
            public string Part2;
        }
    }
}
