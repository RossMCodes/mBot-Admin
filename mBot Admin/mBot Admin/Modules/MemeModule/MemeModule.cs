﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;
using mBot_Admin.Services.MemeService;
using System.Globalization;

namespace mBot_Admin.Modules.MemeModule
{
    [Group("meme")]
    [Alias("memes", "maymay", "maymays")]
    public class MemeModule : ModuleBase
    {
        [Command]
        [Priority(1)]
        public async Task MemeIndex()
        {
            await new MemeService().memeIndex(Context);
        }

        [Command]
        public async Task GetRequestedMeme(int id)
        {
            await new MemeService().getRequestedMeme(Context, id);
        }

        [Command("upgradeDB")]
        [RequireOwner]
        [Priority(1)]
        public async Task upgradeMemeDb()
        {
            await new MemeService().upgradeDB(Context);
        }

        [Command("random")]
        [Priority(1)]
        public async Task rndMeme()
        {
            await new MemeService().rndMeme(Context);
        }

        [Command("Add")]
        [Priority(1)]
        public async Task memeDw([Summary("URL of Image")] string imgurl, string fmat = null)
        {
            await new MemeService().addMeme(Context, imgurl, fmat);
        }

        [Command("upload")]
        [Priority(1)]
        public async Task Upload()
        {
            await new MemeService().uploadMeme(Context);
        }

        [Command("AdminAdd")]
        [Alias("AAdd")]
        [Priority(1)]
        [RequireOwner]
        public async Task adminMemeDw([Summary("URL of Image")] string imgurl, string fmat = null)
        {
            await new MemeService().addAdminMeme(Context, imgurl, fmat);
        }

        [Command("AdminUpload")]
        [Alias("AUpload")]
        [Priority(1)]
        [RequireOwner]
        public async Task adminMemeUpload()
        {
            await new MemeService().uploadAdminMeme(Context);
        }

        [Command("mine")]
        [Priority(1)]
        [RequireOwner]
        public async Task mineMemes(int times, string par1 = null, string par2 = null, string par3 = null, string par4 = null, string par5 = null)
        {
            new MemeMinerService().MineSumMemes(Context, times, par1, par2, par3, par4, par5);
        }

        [Command("check")]
        [Priority(1)]
        [RequireOwner]
        public async Task checkMemes()
        {
            new MemeMinerService().CheckMemes(Context);
        }

        [Command("units")]
        [Priority(1)]
        [RequireOwner]
        public async Task checkUnits()
        {
            new MemeMinerService().CheckUnits(Context);
        }

        [Command("give")]
        [Priority(1)]
        [RequireOwner]
        public async Task GiveMemes(long Amnt, IUser user = null)
        {
            var usr = user ?? Context.User;

            new MemeMinerService().GiveMemes(Context, Amnt, usr);
        }

        [Command("collect")]
        [Priority(1)]
        [RequireOwner]
        public async Task CollectMine()
        {
            new MemeMinerService().CollectMine(Context);
        }

        [Command("checkmine")]
        [Priority(1)]
        [RequireOwner]
        public async Task CheckMine()
        {
            new MemeMinerService().CheckMine(Context);
        }

        [Command("buy")]
        [Priority(1)]
        [RequireOwner]
        public async Task CheckMine(string itm, int amnt = 0)
        {
            new MemeMinerService().Buy(Context, itm, amnt);
        }

        

    }

}
