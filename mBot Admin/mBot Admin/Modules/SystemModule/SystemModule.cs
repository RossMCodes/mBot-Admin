﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using UniqueIdGenerator.Net;
using SharpRaven;
using SharpRaven.Data;

namespace mBot_Admin.Modules.SystemModule
{
    class SystemModule
    {
    }

    public class Sys
    {
        public static ulong UniqueID(short genID)
        {
            DateTime projectStartDate = new DateTime(2017, 01, 01);
            var generator = new Generator(genID, projectStartDate);
            ulong ID = generator.NextLong();
            return ID;
            //Generators:
            //    1 = ???
            //    2 = Moderation
            //    3 = MemeWar
            //    4 = ???
            //    5 = ???
            //    6 = ???
            //    7 = ???
            //    8 = ???
            //    9 = ???
        }

        public class Perms
        {
            public static bool HasManageMessages(ICommandContext Context, IApplication app, IUser user = null, IUser guildOwner = null)
            {
                var iuser = user ?? Context.User;
                var iguser = user as IGuildUser;
                if (!(guildOwner == null))
                {
                    if (iguser == guildOwner)
                    {
                        return true;
                    }
                }

                if (iguser.GuildPermissions.ManageMessages)
                {
                    return true;
                }
                else if (iguser == app.Owner)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public static bool HasManageChannels(ICommandContext Context, IApplication app, IUser user = null, IUser guildOwner = null)
            {
                var iuser = user ?? Context.User;
                var iguser = user as IGuildUser;
                if (!(guildOwner == null))
                {
                    if (iguser == guildOwner)
                    {
                        return true;
                    }
                }

                if (iguser.GuildPermissions.ManageChannels)
                {
                    return true;
                }
                else if (iguser == app.Owner)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public static bool HasManageGuild(ICommandContext Context, IApplication app, IUser user = null, IUser guildOwner = null)
            {
                var iuser = user ?? Context.User;
                var iguser = user as IGuildUser;
                if (!(guildOwner == null))
                {
                    if (iguser == guildOwner)
                    {
                        return true;
                    }
                }

                if (iguser.GuildPermissions.ManageGuild)
                {
                    return true;
                }
                else if (iguser == app.Owner)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

            public static bool HasBanMembers(ICommandContext Context, IApplication app, IUser user = null, IUser guildOwner = null)
            {
                var iuser = user ?? Context.User;
                var iguser = user as IGuildUser;
                if (!(guildOwner == null))
                {
                    if (iguser == guildOwner)
                    {
                        return true;
                    }
                }

                if (iguser.GuildPermissions.BanMembers)
                {
                    return true;
                }
                else if (iguser == app.Owner)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public class Log
        {
            public async Task Texting(string ToLog, bool OnScreen = false)
            {
                StreamWriter sw = File.AppendText(@"BotSystem\Logs\TextLog.log");
                await sw.WriteLineAsync(ToLog);
                sw.Close();
            }
        }
        public static string Uptime()
        {
            return GetUptime();
        }

        private static string GetUptime() => (DateTime.Now - Process.GetCurrentProcess().StartTime).ToString(@"dd\.hh\:mm\:ss");

    }
}
