﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;
using mBot_Admin.Objects.ConfigObjects;
using System.Collections;
using mBot_Admin.ConfigService;
using Newtonsoft.Json;

namespace mBot_Admin.Modules.LoginModule
{
    public class LoginModule : ModuleBase
    {
        private DiscordSocketClient client;

        [Command("Login")]
        [Summary("Logs you in to the server if the server owner has arranged it with @marens101#4158")]
        [RequireContext(ContextType.Guild)]
        public async Task Login(string code = null)
        {
            await Context.Channel.TriggerTypingAsync();
            string json = File.ReadAllText(PathStrings.LoginConfigs);
            var dict = JsonConvert.DeserializeObject<Dictionary<string, LoginObjects.GuildConfig>>(json);
            string guildId = Convert.ToString(Context.Guild.Id);
            LoginObjects.GuildConfig conf;
            if (dict.TryGetValue(guildId, out conf))
            {
                if (conf.LoginChannel == Context.Channel.Id)
                {
                    if(conf.type == LoginObjects.LoginCodeType.None)
                    {
                        await LoginAsync(Context, conf.RoleToAdd, conf.RoleToRemove);
                        var webhook = new DiscordWebhookClient(conf.hook.WebhhokId, conf.hook.WebhookToken);
                        await webhook.SendMessageAsync($"User Logged In: {Context.User.Mention} ({Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id}))");
                        await ReplyAsync("Logged in successfully");
                    }
                    if(conf.type == LoginObjects.LoginCodeType.RefCode)
                    {
                        if (code == null)
                        {
                            await LoginAsync(Context, conf.RoleToAdd, conf.RoleToRemove);
                            var webhook = new DiscordWebhookClient(conf.hook.WebhhokId, conf.hook.WebhookToken);
                            await webhook.SendMessageAsync($"User Logged In: {Context.User.Mention} ({Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id}))");
                            await ReplyAsync("Logged in successfully");
                        }
                        else if(conf.Codes.TryGetValue(code, out var CodeObj))
                        {
                            CodeObj.Uses = CodeObj.Uses++;
                            conf.Codes[code] = CodeObj;
                            dict[guildId] = conf;
                            File.WriteAllText(PathStrings.LoginConfigs, JsonConvert.SerializeObject(dict));
                            await LoginAsync(Context, conf.RoleToAdd, conf.RoleToRemove);
                            var webhook = new DiscordWebhookClient(conf.hook.WebhhokId, conf.hook.WebhookToken);
                            await webhook.SendMessageAsync(
                                $"User Logged In: {Context.User.Mention} ({Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id}))" + "\n"
                                + $"Referral Code Used: `{CodeObj.Code}`, owned by {CodeObj.Owner.Mention}, now used {CodeObj.Uses} times.");
                            await ReplyAsync($"Logged in successfully with referral code {CodeObj.Code}");
                        }
                        else
                        {
                            await ReplyAsync("Oh No!!! That code couldn't be found! Please try again, and if you don't have a working code than run `/login` without one.");
                        }

                        
                    }
                    if(conf.type == LoginObjects.LoginCodeType.AuthCode)
                    {
                        if(code == null)
                        {
                            await ReplyAsync("Sorry, but this server requires a valid authorisation code to proceed. Please obtain one, then try again");
                        }
                        else if (conf.Codes.TryGetValue(code, out var CodeObj))
                        {
                            if (CodeObj.Expired == true)
                            {
                                await ReplyAsync("Sorry, but this server requires a valid authorisation code to proceed, and that one has expired. Please obtain a valid one, then try again");
                            }
                            else if (CodeObj.MaxUses == CodeObj.Uses)
                            {
                                await ReplyAsync("Sorry, but this server requires a valid authorisation code to proceed, and that one has been used the maximum number of allowed times. Please obtain a valid one, then try again");
                            }
                            else if (CodeObj.MaxUses == null)
                            {
                                CodeObj.Uses = CodeObj.Uses + 1;
                                if (CodeObj.SingleUse)
                                    CodeObj.Expired = true;

                                conf.Codes[code] = CodeObj;
                                dict[guildId] = conf;
                                File.WriteAllText(PathStrings.LoginConfigs, JsonConvert.SerializeObject(dict));
                                await LoginAsync(Context, conf.RoleToAdd, conf.RoleToRemove);
                                var webhook = new DiscordWebhookClient(conf.hook.WebhhokId, conf.hook.WebhookToken);
                                await webhook.SendMessageAsync(
                                    $"User Logged In: {Context.User.Mention} ({Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id}))" + "\n"
                                    + $"Auth Code Used: `{CodeObj.Code}`, owned by {CodeObj.Owner.Mention}, now used {CodeObj.Uses} times. Code expiry status: {CodeObj.Expired}");
                                await ReplyAsync($"Logged in successfully with auth code {CodeObj.Code}");
                            }
                            else
                            {
                                CodeObj.Uses = CodeObj.Uses + 1;
                                if (CodeObj.SingleUse)
                                    CodeObj.Expired = true;
                                if (CodeObj.MaxUses == CodeObj.Uses)
                                    CodeObj.Expired = true;

                                conf.Codes[code] = CodeObj;
                                dict[guildId] = conf;
                                File.WriteAllText(PathStrings.LoginConfigs, JsonConvert.SerializeObject(dict));
                                await LoginAsync(Context, conf.RoleToAdd, conf.RoleToRemove);
                                var webhook = new DiscordWebhookClient(conf.hook.WebhhokId, conf.hook.WebhookToken);
                                await webhook.SendMessageAsync(
                                    $"User Logged In: {Context.User.Mention} ({Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id}))" + "\n"
                                    + $"Auth Code Used: `{CodeObj.Code}`, owned by {CodeObj.Owner.Mention}, now used {CodeObj.Uses} of {CodeObj.MaxUses} times. Code expiry status: {CodeObj.Expired}");
                                await ReplyAsync($"Logged in successfully with auth code {CodeObj.Code}");
                            }
                        }
                        else
                        {
                            await ReplyAsync("Sorry, but this server requires a valid authorisation code to proceed, and that one couldn't be found. Please obtain a valid one, then try again");
                        }
                    }
                    
                }
                else
                {
                    await ReplyAsync("Whoops! Wrong channel!");
                }
            }
            else
            {
                await ReplyAsync("Whoops! Invalid server!");
            }
        }

        public async Task LoginAsync(ICommandContext Context, ulong RoleToAddId, ulong RoleToRemoveId)
        {
            var iuser = Context.User as IGuildUser;
            if (iuser == null)
            {
                throw new System.Exception("Failed to get User");
            }
            var roletoadd = iuser.Guild.GetRole(RoleToAddId);
            var roletoremove = iuser.Guild.GetRole(RoleToRemoveId);
            await iuser.RemoveRoleAsync(roletoremove);
            //await Task.Delay(5000);
            await iuser.AddRoleAsync(roletoadd);
            //await Context.Guild.FindRoles("").FirstOrDefault();
        }

        [Command("GenLoginCode")]
        public async Task CreateCode(int uses = 1)
        {
            string json = File.ReadAllText(PathStrings.LoginConfigs);
            var dict = JsonConvert.DeserializeObject<Dictionary<string, LoginObjects.GuildConfig>>(json);
            if (dict.TryGetValue(Context.Guild.Id.ToString(), out var GConf))
            {
                if (GConf.type == LoginObjects.LoginCodeType.None)
                    throw new System.Exception("This guild's login configuration does not use codes");
                if (GConf.RequireManageGuildToCreate == true && !(Context.User as IGuildUser).GuildPermissions.ManageGuild)
                    throw new System.Exception("You aren't authorised!");

                if (GConf.Codes == null)
                    GConf.Codes = new Dictionary<string, LoginObjects.LoginCode>();

                LoginObjects.Type type = new LoginObjects.Type();
                switch (GConf.type)
                {
                    case LoginObjects.LoginCodeType.RefCode:
                        type = LoginObjects.Type.RefCode;
                        break;
                    case LoginObjects.LoginCodeType.AuthCode:
                        if (uses == 1)
                            type = LoginObjects.Type.SingleUseAuth;
                        else
                            type = LoginObjects.Type.MultiUseAuth;
                        break;
                }
                LoginObjects.CodeCreationConfig conf = new LoginObjects.CodeCreationConfig
                {
                    MaxUses = uses,
                    type = type,
                    user = Context.User
                };

                LoginObjects.LoginCode code = LoginObjects.GenerateLoginCode(conf);

                GConf.Codes.Add(code.Code, code);
                dict[Context.Guild.Id.ToString()] = GConf;

                File.WriteAllText(PathStrings.LoginConfigs, JsonConvert.SerializeObject(dict));
                string maxuses()
                {
                    if (code.MaxUses == null)
                    {
                        if (code.SingleUse)
                            return "1";
                        else
                            return "null";
                    }
                    else
                        return code.MaxUses.ToString();
                }
                await ReplyAsync($"Success! Code: `{code.Code}`, Max Uses: `{maxuses()}`. Type: `{code.type}`");

            }
            else
            {
                await ReplyAsync("This guild has not arranged a login system. Please contact marens101#4158");
            }

            
        }

    }

    public class LoginObjects
    {
        public class GuildConfig
        {
            public ulong LoginChannel;
            public ulong RoleToAdd;
            public ulong RoleToRemove;
            public WebhookObject hook;
            public LoginCodeType type;
            public Dictionary<string, LoginCode> Codes;
            public bool RequireManageGuildToCreate;
        }

        public enum LoginCodeType
        {
            None,
            RefCode,
            AuthCode,
        }

        public class LoginCode
        {
            public string Code;
            public int Uses;
            public User Owner;
            public int? MaxUses;
            public bool SingleUse;
            public bool Expired;
            public Type type;
        }
        public static LoginCode GenerateLoginCode(CodeCreationConfig conf)
        {
            LoginCode code = new LoginCode();
            Random rand = new Random();
            string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            string str = new string(Enumerable.Repeat(chars, 6)
                  .Select(s => s[rand.Next(s.Length)]).ToArray());
            code.Code = str;
            code.Owner = new User
            {
                Username = conf.user.Username,
                Discrim = conf.user.DiscriminatorValue,
                Id = conf.user.Id,
                Mention = conf.user.Mention
            };
            code.Uses = 0;
            code.type = conf.type;
            code.Expired = false;
            switch (conf.type)
            {
                case Type.SingleUseAuth:
                    code.SingleUse = true;
                    break;
                case Type.MultiUseAuth:
                    code.SingleUse = false;
                    code.MaxUses = conf.MaxUses;
                    break;
                case Type.RefCode:
                    code.SingleUse = false;
                    break;
            }
            return code;
        }
        public class User
        {
            public string Username;
            public int Discrim;
            public ulong Id;
            public string Mention;
        }

        public struct CodeCreationConfig
        {
            public Type type;
            public IUser user;
            public int? MaxUses;

            
        }
        public enum Type
        {
            RefCode,
            SingleUseAuth,
            MultiUseAuth,
        }

    }
}
