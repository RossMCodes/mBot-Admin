﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net;
using Discord.Rest;
using Discord.Rpc;
using mBot_Admin.Modules.ConfigModule;
using mBot_Admin.Modules.SystemModule;
using mBot_Admin.Objects;
using mBot_Admin.Services;
using mBot_Admin.Templates;
using Newtonsoft.Json;
using RollbarDotNet;
using SharpRaven;
using mBot_Admin.Services.FanFictionService;

namespace mBot_Admin.Modules.FanFictionModule
{
    [Group("AO3")]
    public class AO3Module : ModuleBase
    {
        [Command()]
        public async Task GetStoryByID(int id, bool truncate = true)
        {
            await Context.Channel.TriggerTypingAsync();
            new AO3Service().GetFanfic(Context, id, truncate);
        }

        [Command()]
        public async Task GetStoryByID(string url, bool truncate = true)
        {
            await Context.Channel.TriggerTypingAsync();
            int id = UrlToId(url);
            new AO3Service().GetFanfic(Context, id, truncate);
        }

        public int UrlToId(string url)
        {
            url = url.Trim();
            string[] split = url.Split('/');
            try
            {
                string idstr = split[4];
                int id = Convert.ToInt32(idstr.Trim());
                if (id == null)
                    throw new NullReferenceException();
                return id;
            }
            catch
            {
                string idstr = split[2];
                int id = Convert.ToInt32(idstr.Trim());
                return id;
            }
            
        }
    }
}
