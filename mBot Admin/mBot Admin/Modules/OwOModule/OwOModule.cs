﻿using Discord;
using Discord.Commands;
using mBot_Admin.ConfigService;
using mBot_Admin.ExtensionMethods;
using mBot_Admin.Preconditions.IntergrationPreconditions.OwOPreconditions;
using mBot_Admin.Services.OwOService;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace mBot_Admin.Modules.OwOModule
{
    [Group("owo")]
    public class OwOModule : ModuleBase
    {
        [Group("set")]
        public class Set : ModuleBase
        {
            [Command()]
            public async Task ListSetCmds()
            {
                await ReplyAsync("Availiable subcommands under `/OwO Set`: \"token\", \"t\", \"shorten\", \"s\", \"uploader\", \"u\"");
            }


            [Command("Token")]
            [Alias("t")]
            public async Task SetToken(string input)
            {
                if(Context.Channel is IDMChannel)
                    await new OwOService().SetToken(Context, Context.User, input);
                else
                {
                    var x = await ReplyAsync(Context.User.Mention + " Uhhh... For security, we only allow tokens to be set from within DM channels. Sorry!");
                    try
                    {
                        await Context.Message.DeleteAsync();
                    }
                    catch
                    {
                        await x.ModifyAsync(m => m.Content =
                            "Uhhh...For security, we only allow tokens to be set from within DM channels. Sorry!" + "\n"
                            + "Also, it appears I don't have the permissions to delete your message containing your token. You might want to get rid of that!");
                    }
                }
            }

            [Command("DefaultShortener")]
            [RequireOwOAccount]
            [Alias("s", "shorten", "baseshorten")]
            public async Task SetShorten(string input)
            {
                await new OwOService().SetDefaultShortener(Context, Context.User, input);
            }

            [Command("DefaultUploader")]
            [RequireOwOAccount]
            [Alias("u", "uploader", "baseupload", "baseuploader")]
            public async Task SetUpload(string input)
            {
                await new OwOService().SetDefaultUploader(Context, Context.User, input);
            }
        }
        
        [Command("Reset")]
        [RequireOwOAccount]
        public async Task Reset()
        {
            File.Delete(new  PathStrings.UserConfigs().OwOUserConfig(Context.User.Id));
            await ReplyAsync("Your OwO Account and all configuration has been removed from our servers");
        }

        [Command("shorten")]
        [Alias("s")]
        [RequireOwOAccount]
        public async Task Get(string url)
        {
            await new OwOService().ShortenUrl(Context, url);
        }

        [Command("upload")]
        [Alias("u")]
        [RequireOwOAccount]
        public async Task Upload(/*string url = null*/)
        {
            var attachment = Context.Message.Attachments.FirstOrDefault();
            if (attachment.Size > 5 * 1024 * 1024)
                throw new Exception("File Too Large: Must be smaller than 5mb");
            using (WebClient client = new WebClient())
            {
                byte[] data = client.DownloadData(attachment.Url);
                string hash = Convert.ToBase64String(MD5.Create().ComputeHash(data));
                string alphaHash = hash.ToAlphanumeric();
                string shortHash = alphaHash.GetPrefix(6);
                var splitName = attachment.Filename.Split('.');
                string path = $@"BotSystem\Intergrations\OwO\Temp\{shortHash}.{splitName[splitName.Length - 1]}";
                File.WriteAllBytes(path, data);
                await new OwOService().UploadFile(Context, path, attachment.Filename);
                File.Delete(path);
            }
        }
    }
}
