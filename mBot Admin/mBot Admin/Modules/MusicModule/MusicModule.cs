﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using mBot_Admin.Objects.MusicObjects;
using mBot_Admin.Services.MusicService;

namespace mBot_Admin.Modules.MusicModule
{
    public class MusicModule : ModuleBase
    {
        [Command("youtube")]
        [Alias("yt")]
        public async Task YT(string URL)
        {
            GetAndSendYTInfo(Context, URL);
        }

        public async Task GetAndSendYTInfo(ICommandContext Context, string URL)
        {
            await Context.Channel.TriggerTypingAsync();
            YTInfo yt = await MusicService.GetInfo(URL);
            await Context.Channel.SendMessageAsync($"Video URL: {URL}" + "\n"
                                                    + $"Video Title: {yt.Title}" + "\n"
                                                    + $"Video Duration: {yt.Duration}");
        }
    }
}
