﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using mBot_Admin.Objects.FunObjects;
using mBot_Admin.Services.FunService;
using Newtonsoft.Json;
using mBot_Admin.Templates;
using mBot_Admin.Templates.EmbedTemplates;

namespace mBot_Admin.Modules.FunModule
{
    [Group("joke")]
    public class JokeModule : ModuleBase
    {
        [Command()]
        public async Task randomJoke(string id = null)
        {
            if (id == null)
            {
                await Context.Channel.TriggerTypingAsync();
                var joke = await GetRandomJokeAsync(Context);
                var application = await Context.Client.GetApplicationInfoAsync();
                var builder = new JokeEmbed().newJokeEmbed(application);
                builder.Footer = new EmbedFooterBuilder
                {
                    Text = $"Joke ID: {joke.id}"
                };
                builder.Description = joke.joke;
                var build = builder.Build();
                await ReplyAsync("", embed: build);
            }
            else
            {
                await Context.Channel.TriggerTypingAsync();
                var joke = await GetJokeAsync(Context, id);
                var application = await Context.Client.GetApplicationInfoAsync();
                var builder = new JokeEmbed().newJokeEmbed(application);
                builder.Footer = new EmbedFooterBuilder
                {
                    Text = $"Joke ID: {joke.id}"
                };
                builder.Description = joke.joke;
                var build = builder.Build();
                await ReplyAsync("", embed: build);
            }
        }

        public async Task<ICanHazDadJoke> GetRandomJokeAsync(ICommandContext Context)
        {
            string json = string.Empty;
            string url = @"https://icanhazdadjoke.com/";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Accept = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                json = reader.ReadToEnd();
            }

            ICanHazDadJoke obj = JsonConvert.DeserializeObject<ICanHazDadJoke>(json);
            return obj;
        }

        public async Task<ICanHazDadJoke> GetJokeAsync(ICommandContext Context, string id)
        {
            string json = string.Empty;
            string url = $@"https://icanhazdadjoke.com/j/{id}";

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.AutomaticDecompression = DecompressionMethods.GZip;
            request.Accept = "application/json";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                json = reader.ReadToEnd();
            }

            ICanHazDadJoke obj = JsonConvert.DeserializeObject<ICanHazDadJoke>(json);
            return obj;
        }

    }
}
