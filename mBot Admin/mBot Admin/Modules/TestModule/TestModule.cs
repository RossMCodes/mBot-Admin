﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using mBot_Admin.Services.OtherServices;
using mBot_Admin.Objects.OtherObjects.GitHubObjects;

namespace mBot_Admin.Modules.TestModule
{
    public class TestModule : ModuleBase
    {
        [Command("tst")]
        public async Task Tst(string a)
        {
            var Files = new List<GistObjects.GistCreationObjects.File>();
            var file = new GistObjects.GistCreationObjects.File
            {
                FileName = "test.txt",
                FileContent = "Testing..."
            };
            Files.Add(file);
            var CreationObject = new GistObjects.GistCreationObjects.RootObject()
            {
                Description = "test",
                Files = Files,
                IsPublic = false

            };
            var Gist = await new GithubGistService().CreateAuthenticatedGist(CreationObject);
            await ReplyAsync(Gist.url);
        }

        [Command("test")]
        public async Task Test()
        {
            Process youtubedl;
            ProcessStartInfo youtubedlGetTitle = new ProcessStartInfo()
            {
                FileName = "youtube-dl",
                Arguments = $"-s -e --get-duration https://www.youtube.com/watch?v=9Z4GW6Vd6NI",
                CreateNoWindow = true,
                RedirectStandardOutput = true,
                UseShellExecute = false
            };
            youtubedl = Process.Start(youtubedlGetTitle);
            youtubedl.WaitForExit();
            string line1 = youtubedl.StandardOutput.ReadLine();
            string line2 = youtubedl.StandardOutput.ReadLine();
            await ReplyAsync("`line1:` " + line1);
            await ReplyAsync("`line2:` " + line2);


        }
    }
}
