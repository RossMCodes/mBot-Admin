﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;


/// <summary>
/// ALERT: NO INDUVIDUAL HELP REFERENCES STORED
/// </summary>
namespace mBot_Admin.Modules.HelpModule
{
    [Group("help")]
    [Alias("h")]
    public class HelpModule : ModuleBase
    {
        [Command()]
        [Summary("List of all available commands.")]
        [Alias("h", "help")]
        public async Task HelpList()
        {
            DiscordSocketClient _client = Context.Client as DiscordSocketClient;
            var application = await Context.Client.GetApplicationInfoAsync();

            var eb = new EmbedBuilder()
            {
                Color = new Discord.Color(4, 97, 247),
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
            };

            eb.Title = "marens101 Admin Bot Help";
            eb.Description = "All parameters are in order, and cannot be shuffled. Parameters within square brackets [] are mandatory, and parameters within parentheses () are optional";
            var DM = await Context.User.GetOrCreateDMChannelAsync();
            var efbAssistanceCommands = new EmbedFieldBuilder
            {
                Name = "Assistance Commands",
                Value = "`/help`" + "\n" +
                        "`/support`" + "\n" +
                        "`/invite`",
                IsInline = true
            };

            var efbInfoCommands = new EmbedFieldBuilder
            {
                Name = "Informative Commands",
                Value = "`/ping`" + "\n" +
                        "`/info`" + "\n" +
                        "`/whois`",
                IsInline = true
            };
            //"`/`" + "\n" +
            var efbInteractiveCommands = new EmbedFieldBuilder
            {
                Name = "Interactive Commands",
                Value = "`/pingaz`" + "\n" +
                        "`/decide`" + "\n" +
                        "`/rolldice`" + "\n" +
                        "`/flipcoin`",
                IsInline = true
            };

            var efbMemeCommands = new EmbedFieldBuilder
            {
                Name = "Meme Commands",
                Value = "`/meme`" + "\n" +
                        "`/meme random`" + "\n" +
                        "`/meme add`",
                IsInline = true
            };

            var efbModerationCommands = new EmbedFieldBuilder
            {
                Name = "Moderation Commands",
                Value = "`/mod`" + "\n" +
                        "`/mod ban`" + "\n" +
                        "`/mod channel`",
                IsInline = true
            };

            var efbGuildCommands = new EmbedFieldBuilder
            {
                Name = "Server Commands",
                Value = "`/srv`" + "\n" +
                        "`/srv info`" + "\n" +
                        "`/srv approve`",
                IsInline = true
            };

            eb.AddField(efbAssistanceCommands);
            eb.AddField(efbGuildCommands);
            eb.AddField(efbInfoCommands);
            eb.AddField(efbInteractiveCommands);
            eb.AddField(efbMemeCommands);
            eb.AddField(efbModerationCommands);
            //eb.AddField();


            await DM.SendMessageAsync("", false, eb);

            if (!(Context.Channel is IPrivateChannel))
                await ReplyAsync("Check your DMs! :mailbox_with_mail:");
        }
    }
}
