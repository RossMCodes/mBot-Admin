﻿using Discord;
using Discord.Commands;
using mBot_Admin.ConfigService;
using mBot_Admin.Modules.SystemModule;
using mBot_Admin.Objects.ModerationObjects;
using mBot_Admin.Preconditions.UserPermissionPreconditions;
using mBot_Admin.Templates.ModerationTemplates;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//Moderation Cases: Generator 2
namespace mBot_Admin.Modules.ModerationModule
{
    [Group("mod")]
    [Alias("moderation")]
    [RequireContext(ContextType.Guild)]
    public class ModerationModule : ModuleBase
    {
        [Command("prune")]
        [Alias("clearmsgs", "delmsgs")]
        [RequireBotPermission(ChannelPermission.ManageMessages)]
        [RequireUserPermission(ChannelPermission.ManageMessages)]
        [RequireOwner]
        public async Task PrunePostsAsync(int num, [Remainder] string reason = "No Reason Supplied")
        {
            var application = Config.Application;
            if(num > 100)
            {
                throw new System.Exception("Error: You cannot prune more than 100 messages at a time!");
            }

            var msgs = await Context.Channel.GetMessagesAsync(num).FirstOrDefault();
            await Context.Channel.DeleteMessagesAsync(msgs);
            var mod = Context.User;
            Embed emPurge = new ModEmbedTemplates().PurgeEmbed(Context.User, num, reason);
            await ReplyAsync("", embed: emPurge);
        }

        [Command("chan")]
        [Alias("channel")]
        [RequireManageChannels]
        [Priority(1)]
        public async Task ModifyModChannel(ulong id)
        {
            await Context.Channel.TriggerTypingAsync();
            var application = Config.Application;

            Directory.CreateDirectory($@"BotSystem\Data\Mod\{Context.Guild.Id}");
            Directory.CreateDirectory($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data");
            var chan = await Context.Guild.GetTextChannelAsync(id);
            if (chan == null)
                throw new System.Exception("Channel Not Found");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\ChanID.txt", id.ToString());
            await ReplyAsync($"Mod Channel set to \"{chan.Name}\" with id \"{chan.Id}\"");

        }

        [Command("chan")]
        [Alias("channel")]
        [RequireManageChannels]
        [Priority(0)]
        public async Task ModifyModChannel(ITextChannel chan)
        {
            await Context.Channel.TriggerTypingAsync();
            var application = Config.Application;
            Directory.CreateDirectory($@"BotSystem\Data\Mod\{Context.Guild.Id}");
            Directory.CreateDirectory($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\ChanID.txt", chan.Id.ToString());
            await ReplyAsync($"Mod Channel set to \"{chan.Name}\" with id \"{chan.Id}\"");
        }

        [Command("ban")]
        [RequireBanMembers]
        public async Task Ban(IUser user, [Remainder] string reason)
        {
            reason = reason ?? "None Given";
            var application = Config.Application;

            string idstr = IdStr(Context);

            if (idstr == Context.Channel.Id.ToString())
            {
                if (!(File.Exists($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\ChanID.txt")))
                {
                    await Context.Channel.SendMessageAsync("Mod Channel Not Set. Defaulting to current channel. Use `/mod chan [channelmention]` to set it.");
                }
                else if (File.ReadAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\ChanID.txt") == null)
                {
                    await Context.Channel.SendMessageAsync("Mod Channel Not Set. Defaulting to current channel. Use `/mod chan [channelmention]` to set it.");
                }
            }

            ulong id = Convert.ToUInt64(idstr);
            try
            {
                await Context.Guild.GetTextChannelAsync(id);
            }
            catch
            {
                throw new System.Exception("Invalid Mod Channel");
            }
            ITextChannel modchan = await Context.Guild.GetTextChannelAsync(id);
            RequestOptions options = new RequestOptions();
            var ro = new RequestOptions
            {
                AuditLogReason = $"Banned By {Context.User.Username}#{Context.User.Discriminator} ({Context.User.Id})" + "\n"
                                    + "Provided Reason: " + reason
            };
            await Context.Guild.AddBanAsync(user, 0, options: ro);
            await Context.Channel.TriggerTypingAsync();
            ulong sysid = Sys.UniqueID(2);
            int caseno = GetNewCaseNo(Context);
            LogObject Log = BanLog(caseno, sysid, Context, user, reason);
            Embed em = new ModEmbedTemplates().BanEmbed(Log);
            var msg = await modchan.SendMessageAsync("", embed: em);
            await SetCaseNo(Context, caseno);
            Log.MessageId = msg.Id;
            Log.ModChanId = msg.Channel.Id;
            Log.Save();
        }

        public static string IdStr(ICommandContext Context)
        {
            try
            {
                string idstrr = File.ReadAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\ChanID.txt");
                if (idstrr == null)
                    return Context.Channel.Id.ToString();

                return idstrr;
            }
            catch (FileNotFoundException)
            {
                return Context.Channel.Id.ToString();
            }
            catch
            {
                return Context.Channel.Id.ToString();
            }

        }

        public LogObject BanLog(int caseno, ulong sysid, ICommandContext Context, IUser user, string reason)
        {
            LogObject lo = new LogObject
            {
                SysId = sysid,
                CaseType = CaseType.Ban,
                User = $"{user.Username}#{user.Discriminator}",
                UserId = user.Id,
                Mod = $"{Context.User.Username}#{Context.User.Discriminator}",
                ModId = Context.User.Id,
                Reason = reason,
                GuildId = Context.Guild.Id,
                CaseNo = caseno,
            };
            return lo;
        }


        public int GetNewCaseNo(ICommandContext Context)
        {
            if (!File.Exists($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\CaseNo.txt"))
            {
                File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\CaseNo.txt", "1");
                return 1;
            }
            string casestr = File.ReadAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\CaseNo.txt");
            int caseno = Convert.ToInt32(casestr);
            caseno++;
            return caseno;
        }

        public async Task SetCaseNo(ICommandContext Context, int CaseNo)
        {
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\CaseNo.txt", CaseNo.ToString());
        }
    }
}
