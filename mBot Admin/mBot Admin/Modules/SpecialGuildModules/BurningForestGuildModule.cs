﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;
using mBot_Admin.Modules.PreconditionModule;
using UniqueIdGenerator;
using System.Web.Script.Serialization;

namespace mBot_Admin.Modules.SpecialGuildModule
{
    [Group("BurningForest")]
    [Alias("bf")]
    [RequireBurningForest]
    [RequireContext(ContextType.Guild)]
    public class BurningForestGuildModule : ModuleBase
    {
        [Command("Login")]
        [RequireContext(ContextType.Guild)]
        public async Task Login(int grade, [Remainder] string FullName)
        {
            await Context.Channel.TriggerTypingAsync();
            string guildId = Convert.ToString(Context.Guild.Id);
            string channelId = Convert.ToString(Context.Channel.Id);
            if (guildId == "349371223291068416")
            {
                if (channelId == "349439273155559424")
                {
                    var application = await Context.Client.GetApplicationInfoAsync();
                    var iuser = Context.User as IGuildUser;
                    if (iuser == null)
                    {
                        return;
                    }
                    var roletoadd = iuser.Guild.GetRole(350853499342290956);
                    var roletoremove = iuser.Guild.GetRole(350825784514052106);

                    if (!(File.Exists($@"BotSystem\Data\BurningForest\UserStatus.dict")))
                    {
                        var tempdict = new Dictionary<string, string>();
                        File.WriteAllText($@"BotSystem\Data\BurningForest\UserStatus.dict", new JavaScriptSerializer().Serialize(tempdict));
                    }

                    var userStatusDict = new JavaScriptSerializer()
                        .Deserialize<Dictionary<string, string>>(File.ReadAllText($@"BotSystem\Data\BurningForest\UserStatus.dict"));
                    string status;
                    bool statusexists = userStatusDict.TryGetValue(Context.User.Id.ToString(), out status);

                    if (statusexists)
                    {
                        status = status.ToUpper();
                        if (status.Contains("BAN"))
                        {
                            throw new System.Exception("User is banned from the server");
                        }
                        if (status.Contains("OTI"))
                        {
                            userStatusDict[$"{Context.User.Id}"] = "otievasionban";
                            string reason = "AutoBan by mBot Admin for OTI Evasion";
                            RequestOptions options = new RequestOptions
                            {
                                AuditLogReason = reason
                            };
                            await Context.Guild.AddBanAsync(Context.User, options: options);
                            ulong sysid = Sys.UniqueID(2);
                            //Generator 2 = Moderation
                            int caseno = GetCaseNo(Context);
                            ++caseno;
                            Embed em = BanEmbed(Context, Context.User, reason, sysid, caseno, application);
                            var modchan = await Context.Guild.GetTextChannelAsync(351232017364418573);
                            var msg = await modchan.SendMessageAsync("", embed: em);

                            await modLog(caseno, sysid, Context, msg, Context.User, reason, "AutoBan");
                        }
                    }
                    else
                    {
                        userStatusDict.Add($"{Context.User.Id}", "ok");
                    }

                    await iuser.AddRoleAsync(roletoadd);
                    await ReplyAsync("Logged in successfully");

                    var webhook = await Context.Guild.GetTextChannelAsync(350840970411114496);
                    await webhook.SendMessageAsync($"User Approved: {Context.User.Mention} ({Context.User.Id})" + "\n"
                                                                        + $"Grade: {grade}" + "\n"
                                                                        + $"Name: {FullName}");

                    var todo = await Context.Guild.GetTextChannelAsync(350848278147104768);
                    await todo.SendMessageAsync($"Verify User: {Context.User.Mention} ({Context.User.Id})" + "\n"
                                                                        + $"Grade: {grade}" + "\n"
                                                                        + $"Name: {FullName}");


                    await Task.Delay(1000);
                    await iuser.RemoveRoleAsync(roletoremove);

                    var dict = new Dictionary<string, string>()
                    {
                        {"id", $"{Context.User.Id}"},
                        {"grade", $"{grade}"},
                        {"fullname", $"{FullName}"},
                        {"joindatetime", $"{DateTime.Now.ToString()}"},
                        {"status", $"{userStatusDict[$"{Context.User.Id}"]}"},
                        //{"", $"{}"}
                    };
                    File.WriteAllText($@"BotSystem\Data\BurningForest\Users\{Context.User.Id}.dict", new JavaScriptSerializer().Serialize(dict));
                    File.WriteAllText($@"BotSystem\Data\BurningForest\Users\UserStatus.dict", new JavaScriptSerializer().Serialize(userStatusDict));
                }
                else
                {
                    await ReplyAsync("Whoops! Wrong channel!");
                }
            }
            else
            {
                await ReplyAsync("Whoops! Wrong server!");
            }
        }





        public async Task modLog(int caseno, ulong sysid, ICommandContext Context, IUserMessage msg, IUser user, string reason, string casetype)
        {
            Dictionary<string, string> dict = new Dictionary<string, string>()
            {
                {"guild-id", $"{Context.Guild.Id}" },
                {"sys-id", $"{sysid}" },
                {"case-no", $"{caseno}" },
                {"case-type", $"{casetype}" },
                {"user", $"{user.Username}#{user.Discriminator}" },
                {"user-id", $"{user.Id}" },
                {"mod", $"{Context.User.Username}#{Context.User.Discriminator}" },
                {"mod-id", $"{Context.User.Id}" },
                {"reason", $"{reason}" },
                {"msg-id", $"{msg.Id}" },
                {"mod-chan-used-id", $"{msg.Channel.Id}" }
            };
            //File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\.modlog", $"");
            Directory.CreateDirectory($@"BotSystem\Data\Mod\{Context.Guild.Id}");
            Directory.CreateDirectory($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}");
            Directory.CreateDirectory($@"BotSystem\Data\Mod\_All\{sysid}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\sys-id.modlog", $"{sysid}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\case-type.modlog", $"{casetype}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\user.modlog", $"{user.Username}#{user.Discriminator}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\user-id.modlog", $"{user.Id}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\mod.modlog", $"{Context.User.Username}#{Context.User.Discriminator}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\mod-id.modlog", $"{Context.User.Id}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\reason.modlog", $"{reason}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\msg-id.modlog", $"{msg.Id}");
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\{caseno}\all.dict", new JavaScriptSerializer().Serialize(dict));

            //File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\.modlog", $"");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\guild-id.modlog", $"{Context.Guild.Id}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\case-no.modlog", $"{caseno}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\case-type.modlog", $"{casetype}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\user.modlog", $"{user.Username}#{user.Discriminator}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\user-id.modlog", $"{user.Id}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\mod.modlog", $"{Context.User.Username}#{Context.User.Discriminator}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\mod-id.modlog", $"{Context.User.Id}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\reason.modlog", $"{reason}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\msg-id.modlog", $"{msg.Id}");
            File.WriteAllText($@"BotSystem\Data\Mod\_All\{sysid}\all.dict", new JavaScriptSerializer().Serialize(dict));


        }


        public int GetCaseNo(ICommandContext Context)
        {
            if (!File.Exists($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\CaseNo.txt"))
            {
                File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\CaseNo.txt", "1");
                return 1;
            }
            string casestr = File.ReadAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\CaseNo.txt");
            int caseno = Convert.ToInt32(casestr);
            return caseno;
        }

        public async Task SetCaseNo(ICommandContext Context, int CaseNo)
        {
            File.WriteAllText($@"BotSystem\Data\Mod\{Context.Guild.Id}\_Data\CaseNo.txt", CaseNo.ToString());
        }

        public Embed BanEmbed(ICommandContext Context, IUser user, string reason, ulong sysid, int caseno, IApplication application)
        {
            IUser mod = Context.User;
            Embed embed = new EmbedBuilder
            {
                Title = "Moderation Ban",
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                ThumbnailUrl = user.GetAvatarUrl(),
                Color = new Discord.Color(240, 71, 71),
                Description = $"User: {user.Username}#{user.Discriminator}" + "\n" +
                                $"User ID: {user.Id.ToString()}" + "\n" +
                                $"Responsible Moderator: {mod.Username}#{mod.Discriminator}" + "\n" +
                                $"Case No#: {caseno}" + "\n" +
                                $"Global Case ID#: {sysid}" + "\n" +
                                $"Reason: \"{reason}\""
            };

            return embed;
        }
    }
}

