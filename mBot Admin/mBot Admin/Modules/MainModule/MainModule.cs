﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;

namespace mBot_Admin.Modules.MainModule
{
    public class MainModule : ModuleBase
    {
        //StreamWriter syslog = File.AppendText("pubmodulesyslog.log");

        private DiscordSocketClient client;

        [Command("pingaz")]
        [RequireContext(ContextType.Guild)]
        public async Task Pingaz([Remainder, Summary("The user you wish to Pingaz")] IUser user = null)
        {
            try
            {
                var usr = user ?? Context.Client.CurrentUser;
                var usrDMs = await usr.GetOrCreateDMChannelAsync();
                try
                {
                    await usrDMs.SendMessageAsync($"{Context.User.Username} has requested your presence in '{Context.Guild.Name}', channel '{Context.Channel.Name}'");
                    await ReplyAsync($"Hey, {usr.Mention}. Someone ({Context.User.Mention}):eyes: wants you.");
                }
                catch
                {
                    await ReplyAsync($"Error contacting user");
                }
            }
            catch
            {
                await ReplyAsync($"Error finding user: {Convert.ToString(user)}");
            }
        }

        [Command("Decide")]
        [RequireOwner]
        public async Task Decide()
        {
            string[] decideCoin = new string[]
            {
                "http://mbot.gitlab.io/staticresources/decide/do.png",
                "http://mbot.gitlab.io/staticresources/decide/hell.png"
            };
            Random rnd = new Random();
            int num = rnd.Next(decideCoin.Length);
            var application = await Context.Client.GetApplicationInfoAsync();
            var emInvite = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"Decision Maker",
                ImageUrl = decideCoin[num],
                Timestamp = DateTime.UtcNow,
                //ThumbnailUrl = ""
            };
            await ReplyAsync("", embed: emInvite);
        }



        [Command("invite")]
        [Summary("Returns the invite of the bot")]
        public async Task Invite()
        {
            var application = await Context.Client.GetApplicationInfoAsync();

            var emInvite = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"Invite {application.Name} to your server!",
                Url = "http://rb.marens101.com/m101bot",
                Timestamp = DateTime.UtcNow,
                Footer = new EmbedFooterBuilder
                {
                    Text = $"Also, join the support server with /support"
                }
                //ThumbnailUrl = ""
            };
            await ReplyAsync("", embed: emInvite);
        }

        [Command("support")]
        [Summary("Returns an invite for the marens101 Admin Bot Support Server")]
        public async Task Support()
        {
            var msgg = await ReplyAsync("Hang on a moment, getting the deets...");
            var application = await Context.Client.GetApplicationInfoAsync();
            /*var supSrv = client.GetGuild(291819411814219776);
            var supChan = supSrv.GetChannel(291819411814219776);
            var isup = supChan as ITextChannel;
            var isrv = Context.Guild as IGuild;
            var supChans = supSrv.TextChannels;
            var inv = await isup.CreateInviteAsync();
            string supInv = inv.Url;
            Console.WriteLine(supInv);
            Console.WriteLine(supInv);*/
            var emInvite = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"marens101 Admin Bot Support",
                //Url = supSrvInvite,
                Timestamp = DateTime.UtcNow,
                Footer = new EmbedFooterBuilder
                {
                    Text = "Invite links subject to change at any time" //"Support Server invite link valid for 1/2 an hour from time of creation",
                }
                //ThumbnailUrl = ""
            };
            var emInviteCode = new EmbedFieldBuilder
            {
                Name = "marens101 Admin Bot Support Server",
                Value = $"Invite Link: discord.gg/rE7kEcW" + "\n" + "OR" + "\n" + "rb.marens101.com/m101botsupport",
                IsInline = true
            };
            var emInviteEmail = new EmbedFieldBuilder
            {
                Name = "marens101 Admin Bot Support Email",
                Value = $"Send any comments or queries to:" + "\n" + "botsupport@marens101.com",
                IsInline = true
            };
            emInvite.AddField(emInviteCode);
            emInvite.AddField(emInviteEmail);
            await ReplyAsync("", embed: emInvite);
            await msgg.DeleteAsync();
            await ReplyAsync("https://discord.gg/rE7kEcW");
            //await msgg.
        }

        [Command("info")]
        [Alias("i")]
        [RequireOwner]
        [Summary("Returns information about the Bot")]
        public async Task Info()
        {
            await Context.Channel.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            var GuildUsers = Context.Guild.GetUsersAsync();
            EmbedAuthorBuilder emInfoAuth = new EmbedAuthorBuilder()
            {
                Name = application.Name,
                IconUrl = application.IconUrl,
                Url = "http://rb.marens101.com/m101bot"
            };

            EmbedFooterBuilder emInfoFoot = new EmbedFooterBuilder()
            {
                Text = "Bot Created By marens101",
                //IconUrl = "https://lh3.googleusercontent.com/libtouj7OyT7vBGDn519ms_YX5rA_LqfyOoLNpH27IsfMCdy8Q9l9rxoPW-YQebADFXI7cBD3E4xn-aGFXvs8gKIkHUAhMGLZpDE8NKEZxY0Wonock9FozuH6LEq5YZS5zDyyxopgRYvqP4K2UTjNOqcjn0hotT-Ktig-wQrIV-dsBUx4DJZ2ovbs-THZJlf-pyqYBCSJd1IXgjCSA11CjZPGPL7zvv6Ps5dlgMUlm7pJBS4RwDmMvlOIPrg_I6iCdXWzrw9-nxZWdHW-9VQaWCDrOpmFh2r0aNs7K0JUXCVBoytlJ75wdAHCCyIeUWL1s8YWJgANbshZa2BHQ5BddQ3Huclf_3CDGxIorz227PC0uL9C3Sr_fr6fRdzEYBInptAJPutzer0gX5PkXL-JdumrcekU9mpd9daMBKeiyggsmEjtxQSYvuQRtjSkNAWzc3u0YMOr7GHklEcWwrMYeF193g2TAtBS8Rqi6wzgtWj0jB4FfQ4NsKTCse0E5piPkGi7qiDCIzmuNXxz9vYZOWztEn56RqcusuJBCS0ITkkb3wKlbumcqwLn5qcVD_W-FNhQ8Rp=w1366-h589"
            };

            EmbedFieldBuilder emInfoFieldUptime = new EmbedFieldBuilder()
            {
                Name = "UpTime:",
                Value = $"Bot started at {Process.GetCurrentProcess().StartTime.ToLocalTime().ToString(@"MMMM dd, yyyy a\t hh:mm.ss tt zzz")}"
            };

            EmbedFieldBuilder emInfoFieldGuildCreation = new EmbedFieldBuilder()
            {
                Name = "Server Creation Date & Time",
                Value = Context.Guild.CreatedAt.UtcDateTime.ToString(@"MMMM dd, yyyy a\t hh:mm.ss tt UTC")
            };

            EmbedBuilder emInfo = new EmbedBuilder()
            {
                //https://discordapp.com/oauth2/authorize?client_id=284196779044503553&scope=bot&permissions=8
                Author = emInfoAuth,
                Description = "This command will have lots of stats when it is complete!",
                Color = new Discord.Color(114, 137, 218),
                //This SHOULD be Discord Blue. Conversion: http://www.colorhexa.com\/7289da (Without the backslash "\")
                Title = "Info",
                Footer = emInfoFoot,
                Timestamp = DateTimeOffset.UtcNow,
                ThumbnailUrl = application.IconUrl,

            };

            emInfo.AddField(emInfoFieldUptime);
            emInfo.AddField(emInfoFieldGuildCreation);

            //emInfo.AddField(emInfoFieldGuildUsers);

            //emInfo.AddField(emFieldInfo);
            await ReplyAsync("", embed: emInfo);
        }


        /*[Command("BlowMind")]
        public async Task BlowMind()
        {
            await ReplyAsync("true = false");
            await ReplyAsync("false = true");
            await ReplyAsync("if(('true'=true)=false): print('True')");
            await ReplyAsync("else:print('no')");
            EmbedBuilder emSimplifiedBy = new EmbedBuilder()
            {
                Color = new Discord.Color(114, 137, 218),
                Title = "Simplified by marens101"
            };
            await ReplyAsync("", embed: emSimplifiedBy);
        }*/

        [Command("ping")]
        [Summary("Offers a response of 'Pong' with the latency, and a 1/100 chance of a surprise!")]
        //[Alias("")]
        public async Task Ping()
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var emInvite = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"Ping",
                Description = $"Pong! {(Context.Client as DiscordSocketClient).Latency} ms :ping_pong:",
                Timestamp = DateTime.UtcNow,
                //ThumbnailUrl = ""
            };
            await ReplyAsync("", embed: emInvite);
        }

        [Command("hello")]
        [Alias(new string[] { "hello!", "hi", "hi!" })]
        [Summary("Returns the message 'Hi!'")]
        public async Task hello()
        {
            await ReplyAsync($"{Context.User.Mention} Hi!");
        }

        [Command("coinflip")]
        [Alias("flip", "coin", "flipcoin")]
        public async Task coinflip()
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var id = await ReplyAsync("Flipping...");
            await Context.Channel.TriggerTypingAsync();
            Random gen = new Random();
            bool result = gen.Next(100) < 50 ? true : false;
            await Task.Delay(1000);
            var emCoinFlip = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"Coin Flip",
                //Description = $"Pong! {(Context.Client as DiscordSocketClient).Latency} ms :ping_pong:",
                Timestamp = DateTime.UtcNow,
                //ThumbnailUrl = ""
            };
            if (result == true)
            {
                emCoinFlip.Description = "Heads";
                await id.DeleteAsync();
                await ReplyAsync("", embed: emCoinFlip);
            }
            else
            {
                emCoinFlip.Description = "Tails";
                await id.DeleteAsync();
                await ReplyAsync("", embed: emCoinFlip);
            }

        }

        [Command("rolldice")]
        [Alias("roll", "dice", "diceroll")]
        public async Task RollDice()
        {
            var id = await ReplyAsync("Rolling...");
            await Context.Channel.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            var emCoinFlip = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Color = new Discord.Color(114, 137, 218),
                Title = $"Roll Dice",
                Timestamp = DateTime.UtcNow,
            };
            Random gen = new Random();
            int rolled = gen.Next(7);
            int zero = 0;
            if (rolled == zero)
            {
                emCoinFlip.Description = "The dice fell onto the floor";
                await id.DeleteAsync();
                await ReplyAsync("", embed: emCoinFlip);
            }
            else
            {
                emCoinFlip.Description = $"The number {rolled} was rolled.";
                await id.DeleteAsync();
                await ReplyAsync("", embed: emCoinFlip);
            }
        }



        [Command("whois")]
        public async Task whois([Remainder, Summary("The user for the whois query")] IUser userName = null)
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var user = userName ?? Context.User;
            var emWhois = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl
                },
                Title = $"Whois lookup for {user.Mention} ({user.Username}#{user.Discriminator})",
                ThumbnailUrl = user.GetAvatarUrl(),
            };

            var emWhoisFieldUserName = new EmbedFieldBuilder
            {
                Name = "Username",
                Value = user.Username,
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldUserName);

            var emWhoisFieldDiscrim = new EmbedFieldBuilder
            {
                Name = "Discriminator",
                Value = user.Discriminator,
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldDiscrim);

            var emWhoisFieldIsBot = new EmbedFieldBuilder
            {
                Name = "Is Bot?",
                Value = Convert.ToString(user.IsBot),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldIsBot);

            var emWhoisFieldStatus = new EmbedFieldBuilder
            {
                Name = "Status",
                Value = user.Status.ToString(),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldStatus);

            string game = user.Game.Value.Name ?? "Not Set";
            var emWhoisFieldGame = new EmbedFieldBuilder
            {
                Name = "Game",
                Value = game,
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldGame);

            var emWhoisFieldUserID = new EmbedFieldBuilder
            {
                Name = "User ID",
                Value = user.Id,
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldUserID);

            var emWhoisFieldCreatedAt = new EmbedFieldBuilder
            {
                Name = "Created At",
                Value = user.CreatedAt.ToString(),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldCreatedAt);

            var iu = user as IGuildUser;

            var emWhoisFieldJoinedAt = new EmbedFieldBuilder
            {
                Name = "Joined Server At",
                Value = iu.JoinedAt.ToString(),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldCreatedAt);

            var emWhoisFieldVoiceChan = new EmbedFieldBuilder
            {
                Name = "Current Voice Channel",
                Value = iu.VoiceChannel.Name ?? "None",
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldVoiceChan);

            await ReplyAsync("", embed: emWhois);
        }



    }
}
