﻿using Discord;
using Discord.Commands;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mBot_Admin.Modules.TagModule
{
    [Group("tag")]
    [Alias("t")]
    public class TagModule : ModuleBase
    {


        [Command("create"), Summary("Creates a Tag"), Priority(1)]
        [Alias("add")]
        public async Task CreateTag([Summary("tag to create <tag | what to display>"), Remainder] string tagEntry)
        {
            Directory.CreateDirectory($@"BotSystem\Data\Tags\{Context.Guild.Id}");
            if (!(File.Exists($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict")))
            {
                var tmp = new Dictionary<string, TagStruct>();
                File.WriteAllText($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict", JsonConvert.SerializeObject(tmp));
            }

            var dict = JsonConvert.DeserializeObject<Dictionary<string, TagStruct>>(File.ReadAllText($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict"));

            string[] tagstr = tagEntry.Split('|');
            string tagname = tagstr[0].Trim();
            string tagvalue = tagstr[1].Trim().Trim('`');


            if (dict.ContainsKey(tagname))
            {
                throw new System.Exception("Tag Already Exists");
            }



            TagStruct Tag = new TagStruct
            {
                OwnerDiscrim = Context.User.DiscriminatorValue,
                OwnerId = Context.User.Id,
                OwnerName = Context.User.Username,
                OwnerMention = Context.User.Mention,
                UseCount = 0,
                Name = tagname,
                Value = tagvalue
            };

            dict.Add(tagname, Tag);


            File.WriteAllText($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict", JsonConvert.SerializeObject(dict));


            await ReplyAsync($"Tag \"`{Tag.Name}`\" created with value \"`{Tag.Value}`\"");
        }

        [Command("remove"), Summary("Removes specified Tag"), Priority(1)]
        public async Task RemoveTag([Summary("tag to remove"), Remainder] string tag)
        {
            Directory.CreateDirectory($@"BotSystem\Data\Tags\{Context.Guild.Id}");
            if (!(File.Exists($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict")))
            {
                throw new System.Exception("Tags Not Configured on this server");
            }

            var dict = JsonConvert.DeserializeObject<Dictionary<string, TagStruct>>(File.ReadAllText($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict"));
            TagStruct Tag;
            bool exists = dict.TryGetValue(tag, out Tag);
            if (exists)
            {
                dict.Remove(Tag.Name);
                File.WriteAllText($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict", JsonConvert.SerializeObject(dict));
                await Context.Channel.SendMessageAsync($"Tag \"`{Tag.Name}`\" deleted");

            }
            else
            {
                throw new System.Exception("Tag Not Found");
            };
        }

        [Command("taglist"), Summary("Lists all tags"), Priority(1)]
        public async Task ListTags()
        {
            Directory.CreateDirectory($@"BotSystem\Data\Tags\{Context.Guild.Id}");
            if (!(File.Exists($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict")))
            {
                throw new System.Exception("Tags Not Configured on this server");
            }

            var dict = JsonConvert.DeserializeObject<Dictionary<string, TagStruct>>(File.ReadAllText($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict"));
            var application = await Context.Client.GetApplicationInfoAsync();
            var emTagList = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl
                },
                Title = $"List Of All Tags in server: {Context.Guild.Name}",
            };


            foreach (KeyValuePair<string, TagStruct> t in dict)
            {
                IUser tmpuser = await Context.Guild.GetUserAsync(t.Value.OwnerId);
                string usermention = tmpuser.Mention ?? $"{t.Value.OwnerName}#{t.Value.OwnerDiscrim}";
                EmbedFieldBuilder ef = new EmbedFieldBuilder
                {
                    Name = t.Value.Name,
                    Value = $"Created By: {usermention}" + "\n"
                              + $"Times Used: {t.Value.UseCount}",
                    IsInline = true
                };
                emTagList.AddField(ef);
            }

            await ReplyAsync("", embed: emTagList);
        }

        [Command(""), Summary("Displays the value of the Tag specified if it exists"), Priority(0)]
        public async Task SearchTag([Summary("tag to search"), Remainder] string tag)
        {
            Directory.CreateDirectory($@"BotSystem\Data\Tags\{Context.Guild.Id}");
            if (!(File.Exists($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict")))
            {
                throw new System.Exception("Tags Not Configured on this server");
            }

            var dict = JsonConvert.DeserializeObject<Dictionary<string, TagStruct>>(File.ReadAllText($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict"));
            TagStruct Tag;
            bool exists = dict.TryGetValue(tag, out Tag);
            if (exists)
            {
                Tag.UseCount = Tag.UseCount + 1;
                dict[Tag.Name] = Tag;
                File.WriteAllText($@"BotSystem\Data\Tags\{Context.Guild.Id}\tags.dict", JsonConvert.SerializeObject(dict));
                await Context.Channel.SendMessageAsync(Tag.Value);

            }
            else
            {
                throw new System.Exception("Tag Not Found");
            };
        }



    }


    public struct TagStruct
    {
        public ulong OwnerId;
        public string OwnerMention;
        public string OwnerName;
        public ushort OwnerDiscrim;
        public string Name;
        public string Value;
        public int UseCount;
    }
}
