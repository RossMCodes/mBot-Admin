﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System.Web.Script.Serialization;
using System.IO;

namespace mBot_Admin.Modules.PreconditionModule
{
    public class RequirePendingMemeWarAttribute : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            var activeWarDict = new JavaScriptSerializer()
                .Deserialize<Dictionary<string, ulong>>(File.ReadAllText(@"BotSystem\Data\MemeWar\active.dict"));
            ulong activeID;
            bool exists = activeWarDict.TryGetValue(Context.Guild.Id.ToString(), out activeID);
            if (!exists)
            {
                return PreconditionResult.FromError("There is no current MemeWar on this server!");
            }
            else
            {
                return PreconditionResult.FromSuccess();
            };

        }
    }



    public class RequireBurningForestAttribute : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            var ownerID = (await Context.Client.GetApplicationInfoAsync()).Owner.Id;
            if (Context.Guild.Id == 349371223291068416)
                return PreconditionResult.FromSuccess();
            else
            {
                return PreconditionResult.FromError("Command can only be executed from within The Burning Forest");
                //User requires guild permission ManageGuild
            }
        }
    }

    public class RequireNNAAttribute : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            var ownerID = (await Context.Client.GetApplicationInfoAsync()).Owner.Id;
            if (Context.Guild.Id == 366187551557550081) //v2
                return PreconditionResult.FromSuccess();
            if (Context.Guild.Id == 267108276171309076) //Legacy v1
                return PreconditionResult.FromSuccess();
            else
            {
                return PreconditionResult.FromError("Command can only be executed from within The Burning Forest");
                //User requires guild permission ManageGuild
            }
        }
    }

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
    public class RequireUserPermissionAttribute : PreconditionAttribute
    {
        public GuildPermission? GuildPermission { get; }
        public ChannelPermission? ChannelPermission { get; }

        public RequireUserPermissionAttribute(GuildPermission permission)
        {
            GuildPermission = permission;
            ChannelPermission = null;
        }

        public RequireUserPermissionAttribute(ChannelPermission permission)
        {
            ChannelPermission = permission;
            GuildPermission = null;
        }

        public async override Task<PreconditionResult> CheckPermissions(ICommandContext context, CommandInfo command, IServiceProvider services)
        {
            var guildUser = context.User as IGuildUser;

            var ownerID = (await context.Client.GetApplicationInfoAsync()).Owner.Id;

            if (context.User.Id == ownerID)
            {
                return PreconditionResult.FromSuccess();
            }

            if (GuildPermission.HasValue)
            {
                if (guildUser == null)
                    return PreconditionResult.FromError("Command must be used in a guild channel");
                if (!guildUser.GuildPermissions.Has(GuildPermission.Value))
                    return PreconditionResult.FromError($"You lack the required permission \"{GuildPermission.Value}\"");
            }

            if (ChannelPermission.HasValue)
            {
                var guildChannel = context.Channel as IGuildChannel;

                ChannelPermissions perms;
                if (guildChannel != null)
                    perms = guildUser.GetPermissions(guildChannel);
                else
                    perms = ChannelPermissions.All(guildChannel);

                if (!perms.Has(ChannelPermission.Value))
                    return PreconditionResult.FromError($"User requires channel permission {ChannelPermission.Value}");
            }

            return PreconditionResult.FromSuccess();
        }
    }
}




/*
public class RequireManageGuildAttribute : PreconditionAttribute
    {
        public async override Task<PreconditionResult> CheckPermissions(ICommandContext Context, CommandInfo Command, IServiceProvider Services)
        {
            if (true)
            {
                return PreconditionResult.FromSuccess();
            }
            else
            {
                return PreconditionResult.FromError("");
            }
        }
    }
*/

