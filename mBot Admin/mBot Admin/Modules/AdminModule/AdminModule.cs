using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;
using System.Web.Script.Serialization;
using Tweetinvi;
using mBot_Admin.Objects.ConfigObjects;
using Newtonsoft.Json;
using mBot_Admin.Services.AdministrationService;
using mBot_Admin._System;
using mBot_Admin.ConfigService;
using mBot_Admin._System.ExtensionMethods;

namespace mBot_Admin.Modules.AdminModule
{
    [Group("admin")]
    [Alias("a")]
    [RequireOwner]
    public class AdminModule : ModuleBase //<SocketCommandContext>
    {
        private DiscordSocketClient client;

        [Command("GiveEveryone")]
        [Alias("GrantEveryone", "ge")]
        public async Task GiveEveryoneRole(IRole role)
        {
            List<string> errList = new List<string>();
            string basestr = "Running...";
            var msg = await ReplyAsync(basestr);
            var users = await Context.Guild.GetUsersAsync();
            int complete = 0;
            int total = users.Count;
            int errors = 0;
            foreach (IUser u in users)
            {
                
                try
                {
                    await (u as IGuildUser).AddRoleAsync(role);
                    complete++;
                }
                catch
                {
                    errList.Add("An error occured while adding role to user: " + u.Username + "#" + u.Discriminator);
                    basestr = "Running..." + "\n" + errList.JoinAll();
                    errors++;
                }

                string tmsg = basestr + "\n" + " Completed " + complete + " of " + total + ". Total Errors: " + errors
                        + "User was " + u.Username + "#" + u.Discriminator + "(" + u.Id + ")";
                await msg.ModifyAsync(c => c.Content = tmsg);
            }
            await msg.ModifyAsync(c => c.Content = errList.JoinAll() + '\n' + "Complete! Add role to " + (complete - errors) + "of" + total + "users successfully!");
        }

        [Command("SetAutoRole")]
        public async Task SetAutoRole(ulong id)
        {
            if(!(File.Exists(@"BotSystem\Data\Srv\autoRoles.dict")))
            {
                var tmp = new Dictionary<string, ulong>();
                File.WriteAllText(@"BotSystem\Data\Srv\autoRoles.dict", new JavaScriptSerializer().Serialize(tmp));
            }

            var dict = new JavaScriptSerializer()
                                    .Deserialize<Dictionary<string, ulong>>(File.ReadAllText(@"BotSystem\Data\Srv\autoRoles.dict"));

            dict[$"{Context.Guild.Id}"] = id;

            File.WriteAllText(@"BotSystem\Data\Srv\autoRoles.dict", new JavaScriptSerializer().Serialize(dict));

            await ReplyAsync("<:ok1:331993694867554305> " + "AutoRole Set");
        }

        [Command("DeleteAutoRole")]
        [Alias("RemoveAutoRole")]
        public async Task DeleteAutoRole()
        {
            if (!(File.Exists(@"BotSystem\Data\Srv\autoRoles.dict")))
            {
                var tmp = new Dictionary<string, ulong>();
                File.WriteAllText(@"BotSystem\Data\Srv\autoRoles.dict", new JavaScriptSerializer().Serialize(tmp));
            }

            var dict = new JavaScriptSerializer()
                                    .Deserialize<Dictionary<string, ulong>>(File.ReadAllText(@"BotSystem\Data\Srv\autoRoles.dict"));
            ulong outt;
            if(dict.TryGetValue($"{Context.Guild.Id}", out outt))
            {
                dict.Remove($"{Context.Guild.Id}");
            }

            File.WriteAllText(@"BotSystem\Data\Srv\autoRoles.dict", new JavaScriptSerializer().Serialize(dict));

            await ReplyAsync("<:ok1:331993694867554305> " + "AutoRole Removed");
        }

        [Command("SetUsername")]
        public async Task SetUsername([Remainder] string name)
        {
            await Context.Client.CurrentUser.ModifyAsync(f => f.Username = name);
            await Context.Channel.SendMessageAsync("<:ok1:331993694867554305>");
        }

        [Command("Relay")]
        public async Task AdminRelayMessage(ulong srvid, ulong chanid, [Remainder] string msg)
        {
            await Context.Channel.TriggerTypingAsync();
            var srv = await Context.Client.GetGuildAsync(srvid);
            var igchan = await srv.GetTextChannelAsync(chanid);
            await igchan.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            EmbedBuilder emMsgRelay = new EmbedBuilder()
            {
                Title = "Incoming Message from marens101",
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                ThumbnailUrl = application.Owner.GetAvatarUrl(),
                Color = new Discord.Color(240, 71, 71),
                Description = msg
            };
            await igchan.SendMessageAsync("", embed: emMsgRelay);
        }

        [Command("overlordRelay")]
        [Alias("oRelay", "sRelay", "supremeRelay", "supremeOverlordRelay")]
        public async Task SupremeAdminRelayMessage(ulong srvid, ulong chanid, [Remainder] string msg)
        {
            await Context.Channel.TriggerTypingAsync();
            var srv = await Context.Client.GetGuildAsync(srvid);
            var igchan = await srv.GetTextChannelAsync(chanid);
            await igchan.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            EmbedBuilder emMsgRelay = new EmbedBuilder()
            {
                Title = "Incoming Message from marens101, the supreme overlord of these lands.",
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                ThumbnailUrl = application.Owner.GetAvatarUrl(),
                Color = new Discord.Color(240, 71, 71),
                Description = msg
            };
            await igchan.SendMessageAsync("", embed: emMsgRelay);
        }

        [Command("PermCheck")]
        [Alias("PermsCheck", "CheckPerm", "CheckPerms")]
        public async Task CheckPerms(IUser user)
        {
            Console.WriteLine("PermCheck Active...");
            await Context.Channel.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            var iuser = user ?? Context.User;
            var perms = (iuser as SocketGuildUser).GuildPermissions;
            EmbedBuilder emPermCheck = new EmbedBuilder()
            {
                Title = "Permissions Check",
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                //ThumbnailUrl = user.GetAvatarUrl(),
                Color = new Discord.Color(240, 71, 71),
                Description = $"Checking Permissions..."
            };

            await Context.Channel.TriggerTypingAsync();

            var efbPermAdministrator = new EmbedFieldBuilder
            {
                Name = "Permission: Administrator",
                Value = $"Value: {perms.Administrator.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermAdministrator);

            await Context.Channel.TriggerTypingAsync();

            var efbPermManageGuild = new EmbedFieldBuilder
            {
                Name = "Permission: ManageGuild",
                Value = $"Value: {perms.ManageGuild.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermManageGuild);

            await Context.Channel.TriggerTypingAsync();

            var efbPermManageChans = new EmbedFieldBuilder
            {
                Name = "Permission: ManageChannels",
                Value = $"Value: {perms.ManageChannels.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermManageChans);

            await Context.Channel.TriggerTypingAsync();

            var efbPermManageMessages = new EmbedFieldBuilder
            {
                Name = "Permission: ManageMessages",
                Value = $"Value: {perms.ManageMessages.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermManageMessages);

            await Context.Channel.TriggerTypingAsync();

            var efbPermManageRoles = new EmbedFieldBuilder
            {
                Name = "Permission: ManageRoles",
                Value = $"Value: {perms.ManageRoles.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermManageRoles);

            await Context.Channel.TriggerTypingAsync();

            var efbPermBanMembers = new EmbedFieldBuilder
            {
                Name = "Permission: BanMembers",
                Value = $"Value: {perms.BanMembers.ToString()}",
                IsInline = true
            };
            emPermCheck.AddField(efbPermBanMembers);

            await ReplyAsync("", embed: emPermCheck);
        }

        [Command("say")]
        //[Summary("")]
        public async Task Say(string v1, string v2 = null)
        {
            await ReplyAsync(v1);
            if(!(v2 == null))
            {
                await Task.Delay(500);
                await ReplyAsync(v2);
            }
        }

        [Command("edit")]
        public async Task EditMessage(ulong msgId, [Remainder] string newContent)
        {
            //GetAndEditMessageAsync(msgId, newContent);
            throw new NotImplementedException();
        }

        public async Task GetAndEditMessageAsync(ulong msgId, [Remainder] string newContent)
        {
            bool Complete = false;
            var msg = await Context.Channel.GetMessageAsync(msgId) ?? null;
            if (msg != null)
                Complete = true;
            if (!Complete)
            {
                foreach (ITextChannel c in await Context.Guild.GetChannelsAsync())
                {
                    var tmsg = await c.GetMessageAsync(msgId);
                    if (tmsg != null)
                    {
                        msg = tmsg;
                        continue;
                    }
                }
            }
            if (!Complete)
            {
                foreach (IGuild g in await Context.Client.GetGuildsAsync())
                {
                    foreach (ITextChannel c in await Context.Guild.GetChannelsAsync())
                    {
                        var tmsg = await c.GetMessageAsync(msgId);
                        if (tmsg != null)
                        {
                            msg = tmsg;
                            continue;
                        }
                    }
                    if (msg != null)
                    {
                        continue;
                    }
                }

            }
            if (!Complete)
            {
                throw new System.Exception("Could not find message: " + msgId);
            }
            var imsg = msg as IUserMessage;
            await imsg.ModifyAsync(m => m.Content = newContent);
            var x = await ReplyAsync("<:ok1:331993694867554305>");
            await Task.Delay(5000);
            await x.DeleteAsync();
        }

        [Command("text")]
        [Alias("txt")]
        public async Task SendText(string msg, string to)
        {
            SendingResult res = await new AdminTextMessageService().SendTextAsync(Context, msg, new string[] { to });
            if (res.Success)
            {
                await ReplyAsync("Success! Message Id: " + res.Id);
            }
            else
            {
                await ReplyAsync("Error! Exception Message: " + res.ClientException.Message);
            }
        }

        [Command("DMEveryone")]
        public async Task DMEveryone([Remainder] string message)
        {
            var conf = new DMEveryoneConf
            {
                code = GenerationService.GenRandomString(6),
                message = message,
                UserDiscrim = Context.User.DiscriminatorValue,
                UserName = Context.User.Username,
                UsrId = Context.User.Id,
                guildId = Context.Guild.Id,
            };



            var book = MobileAddressBook.Load();
            if(book.TryGetEntry(Context.User, out MobileAddressBook.Entry entry))
            {
                var txt = new AdminTextMessageService().SendTextAsync(Context, 
                    $"Your mBot Admin Verification Code is: {conf.code}",
                    new string[] { entry.Number });
                File.WriteAllText(PathStrings.DMEveryoneConfig, JsonConvert.SerializeObject(conf));
                await ReplyAsync("Do NOT send bulk DMs lightly. Only use under critical circumstances. A verification code has been sent to your phone. Run `/DME [Code]` to send the message.");
            }
            else
            {
                await ReplyAsync("Address Book Entry Not Found!");
            }
            
            
        }

        [Command("AddAddressBook")]
        public async Task AddAdderssBook(string num)
        {
            var conf = new MobileAddressBook.Entry
            {
                Number = num,
                UserDiscrim = Context.User.DiscriminatorValue,
                UserName = Context.User.Username,
                UserId = Context.User.Id,
            };



            var book = MobileAddressBook.Load();
            book[Context.User.Id.ToString()] = conf;
            MobileAddressBook.Save(book);


        }

        [Command("DME")]
        public async Task DME(string code)
        {
            if (!File.Exists(PathStrings.DMEveryoneConfig))
                throw new System.Exception("Not Configured");
            string json = File.ReadAllText(PathStrings.DMEveryoneConfig);
            DMEveryoneConf conf = JsonConvert.DeserializeObject<DMEveryoneConf>(json);
            if(code == "view")
            {
                await ReplyAsync(conf.message);
            }
            else if (code == conf.code && Context.Guild.Id == conf.guildId)
            {
                string basestr = "As you wish...";
                var msg = await ReplyAsync(basestr);
                var users = await Context.Guild.GetUsersAsync();
                int complete = 0;
                int errors = 0;
                int total = users.Count;
                List<string> errList = new List<string>();
                foreach (IUser u in users)
                {
                    try
                    {
                        var DM = await u.GetOrCreateDMChannelAsync();
                        await DM.SendMessageAsync(conf.message);
                    }
                    catch
                    {
                        errList.Add("An error occured while DMing the user: " + u.Username + "#" + u.Discriminator);
                        basestr = "As you wish..." + "\n" + errList.JoinAll();
                        errors++;
                    }
                    
                    complete++;

                    string tmsg = basestr + "\n" + " Completed " + complete + " of " + total + ". Total Errors: " + errors
                        + "User was " + u.Username + "#" + u.Discriminator + "(" + u.Id + ")";
                    await msg.ModifyAsync(c => c.Content = tmsg);
                }
                await msg.ModifyAsync(c => c.Content = errList.JoinAll() + '\n' + "Complete! DM'd " + (complete - errors) + "of" + total + "users successfully!");
            }
            else if (code == conf.code && Context.Guild.Id != conf.guildId)
            {
                await ReplyAsync("Wrong Guild");
            }
            else
            {
                await ReplyAsync("Invalid Code");
            }
        }

        public class DMEveryoneConf
        {
            public string code;
            public string message;
            public ulong guildId;
            public ulong UsrId;
            public string UserName;
            public int UserDiscrim;
        }

        [Command("LeaveAsync()")]
        [RequireOwner]
        public async Task LeaveAsync()
        {
            await ReplyAsync("<:blobcry:333081702387941389>" + " Goodbye " + "<:blobcry:333081702387941389>");
            await ReplyAsync("<:blobcry:333081702387941389>" + " Hope I can come back soon " + "<:blobcry:333081702387941389>");
            await ReplyAsync("<:blobcry:333081702387941389>" + " http://mbot.marens101.com/ " + "<:blobcry:333081702387941389>");
            await Context.Guild.LeaveAsync();
        }

        [Command("whois")]
        public async Task AdminWhois([Remainder, Summary("The user for the whois query")] IUser userName = null)
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var user = userName ?? Context.User;
            var emWhois = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl
                },
                Title = $"Whois lookup for {user.Username}#{user.Discriminator}",
                ThumbnailUrl = user.GetAvatarUrl(),
            };

            var emWhoisFieldUserName = new EmbedFieldBuilder
            {
                Name = "Username",
                Value = user.Username,
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldUserName);

            var emWhoisFieldDiscrim = new EmbedFieldBuilder
            {
                Name = "Discriminator",
                Value = user.Discriminator,
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldDiscrim);

            var emWhoisFieldIsBot = new EmbedFieldBuilder
            {
                Name = "Is Bot?",
                Value = Convert.ToString(user.IsBot),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldIsBot);

            var emWhoisFieldIsWebhook = new EmbedFieldBuilder
            {
                Name = "Is Webhook?",
                Value = Convert.ToString(user.IsWebhook),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldIsWebhook);

            var emWhoisFieldStatus = new EmbedFieldBuilder
            {
                Name = "Status",
                Value = user.Status.ToString(),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldStatus);

            var emWhoisFieldAvatarURL = new EmbedFieldBuilder
            {
                Name = "Avatar URL",
                Value = user.GetAvatarUrl(),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldAvatarURL);

            var emWhoisFieldAvatarID = new EmbedFieldBuilder
            {
                Name = "Avatar ID",
                Value = user.AvatarId,
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldAvatarID);

            var emWhoisFieldUserID = new EmbedFieldBuilder
            {
                Name = "User ID",
                Value = user.Id,
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldUserID);

            var emWhoisFieldCreatedAt = new EmbedFieldBuilder
            {
                Name = "Created At",
                Value = user.CreatedAt.ToString(),
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldCreatedAt);

            var emWhoisFieldGame = new EmbedFieldBuilder
            {
                Name = "Game",
                Value = user.Game.Value.Name ?? "Not Set",
                IsInline = true
            };
            emWhois.AddField(emWhoisFieldGame);

            await ReplyAsync("", embed: emWhois);
        }

        [Command("RoleOrder")]
        [Alias("ro")]
        public async Task RoleOrder()
        {
            await Context.Channel.TriggerTypingAsync();
            var roles = Context.Guild.Roles;
            Dictionary<string, int> roledict = new Dictionary<string, int> { };
            foreach (IRole r in roles)
            {
                if(r.Position == 0)
                {
                    roledict.Add("everyone", r.Position);
                }
                else
                {
                    roledict.Add(r.Name, r.Position);
                }
                
            }
            //Dictionary<int, string> roledictsort = from pair in roledict
            //                                       orderby pair.Key ascending
            //                                       select pair;
            string send = null;

            foreach (KeyValuePair<string, int> i in roledict.OrderByDescending(i => i.Value))
            {
                send = send + "\n" + "{" + i.Value + ":" + i.Key.ToString() + "}";
            }
            send.Trim();
            await ReplyAsync(send);
        }

        [Command("RoleIds")]
        [Alias("ri")]
        public async Task RoleIds()
        {
            await Context.Channel.TriggerTypingAsync();
            var roles = Context.Guild.Roles;
            Dictionary<string, KeyValuePair<ulong, int>> roledict = new Dictionary<string, KeyValuePair<ulong, int>> { };
            foreach (IRole r in roles)

            {
                if (r.Position == 0)
                {
                    roledict.Add("everyone", new KeyValuePair<ulong, int>(r.Id, r.Position));
                }
                else
                {
                    roledict.Add(r.Name, new KeyValuePair<ulong, int>(r.Id, r.Position));
                }

            }
            //Dictionary<int, string> roledictsort = from pair in roledict
            //                                       orderby pair.Key ascending
            //                                       select pair;
            string send = null;

            foreach (KeyValuePair<string, KeyValuePair<ulong, int>> i in roledict.OrderByDescending(i => i.Value.Value))
            {
                send = send + "\n" + "{" + i.Value.Value + ":" + i.Key.ToString() + ":" + i.Value.Key + "}";
            }
            send.Trim();
            await ReplyAsync(send);
        }

        [Command("MyRoles")]
        [Alias("myroleids", "mri", "mr")]
        public async Task MyRoles(IUser user = null)
        {
            user = user ?? Context.User;
            await Context.Channel.TriggerTypingAsync();
            var iguser = user as IGuildUser;
            var roles = iguser.RoleIds;
            string stroles = null;
            foreach (var id in roles)
            {
                var role = Context.Guild.GetRole(id);
                stroles = stroles + "\n" + "{" + role.Name + ":" + role.Id.ToString() + "}";
            }
            stroles.Trim();
            await ReplyAsync(stroles);
        }

        [Command("approve")]
        [RequireContext(ContextType.Guild)]
        public async Task adminApprove(IGuildUser user)
        {
            if (!File.Exists($@"BotSystem\Data\Srv\adminApprove\id.dict")) ;
            {
                throw new System.Exception("Not set.");
            }
            var dictionary = new JavaScriptSerializer()
                                  .Deserialize<Dictionary<string, string>>(File.ReadAllText($@"BotSystem\Data\Srv\adminApprove\id.dict"));
            if (!dictionary.ContainsKey($"{Context.Guild.Id}"));
            {
                throw new System.Exception("Not set.");
            }
            string iid = dictionary[$"{Context.Guild.Id}"];
            ulong id = Convert.ToUInt64(iid);
            var role = Context.Guild.GetRole(id);
            await user.AddRoleAsync(role);
            await ReplyAsync("<:ok1:331993694867554305>" + "\n" +
                                $"{user.Mention} has been approved.");
        }

        [Command("setapprove")]
        public async Task adminSetApprove(ulong id)
        {
            var role = Context.Guild.GetRole(id);
            if (!File.Exists($@"BotSystem\Data\Srv\adminApprove\id.dict"));
            {
                var td = new Dictionary<string, string>();
                File.WriteAllText($@"BotSystem\Data\Srv\adminApprove\id.dict", new JavaScriptSerializer().Serialize(td));
            }
            var dictionary = new JavaScriptSerializer()
                                  .Deserialize<Dictionary<string, string>>(File.ReadAllText($@"BotSystem\Data\Srv\adminApprove\id.dict"));

            dictionary.Add($"{Context.Guild.Id}", $"{id}");

            File.WriteAllText($@"BotSystem\Data\Srv\adminApprove\id.dict", new JavaScriptSerializer().Serialize(dictionary));

            await ReplyAsync("<:ok1:331993694867554305>" + "\n" +
                                $"Approved role set to {role.Mention}");
        }

        [Command("patrol")]
        [RequireContext(ContextType.Guild)]
        public async Task adminPatrol(IGuildUser user, string patrol)
        {
            if (Convert.ToString(Context.Guild.Id) == "290009650534023168")
            {
                if (patrol.ToUpper() == "SCORPION")
                {
                    var role = user.Guild.GetRole(290015277360480257);
                    await user.AddRoleAsync(role);
                    await ReplyAsync($"{user.Mention} has been added to {patrol.ToUpper()}");
                }
                else if (patrol.ToUpper() == "QUOKKA")
                {
                    var role = user.Guild.GetRole(290015301389778945);
                    await user.AddRoleAsync(role);
                    await ReplyAsync($"{user.Mention} has been added to {patrol.ToUpper()}");
                }
                else if (patrol.ToUpper() == "PANTHER")
                {
                    var role = user.Guild.GetRole(290015324068380672);
                    await user.AddRoleAsync(role);
                    await ReplyAsync($"{user.Mention} has been added to {patrol.ToUpper()}");
                }

                else
                {
                    await ReplyAsync("Invalid Patrol");
                }
            }
            else
            {
                await ReplyAsync("Invalid Guild to perform Operation");
            }
        }

    }
}
