﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using System.Drawing;
using System.Net.Http;
using System.Collections.Specialized;
using RollbarDotNet;
using TextmagicRest;
using TextmagicRest.Model;
using mBot_Admin.Modules.SystemModule;
using System.Web.Script.Serialization;



namespace mBot_Admin.Modules.ReleaseNotesModule
{
    [Group("ReleaseNotes")]
    [Alias("relnotes")]
    public class ReleaseNotesModule : ModuleBase
    {
        private DiscordSocketClient client;

        [Command("0.7")]
        [Alias("")]
        public async Task Relnotes0_7()
        {

        }

        [Command("0.6")]
        public async Task RelNotes0_6()
        {

        }



        [Command("0.5")]
        public async Task RelNotes0_5()
        {
            var Chan = Context.Channel;
            await Chan.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            var emRelNotes = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Title = "Release Notes - v0.5",
                //{application.Name} / marens101 Admin
                Description = $"{application.Name} - Beta Release 0.5" + "\n" + "Release Date: 06/07/2017 3:15pm AEST +11:00",
                Color = new Discord.Color(114, 137, 218),
                Timestamp = DateTimeOffset.UtcNow
            };

            var emRelNotesFeatures = new EmbedFieldBuilder
            {
                Name = "New Features",
                Value = " - Random Meme Command now accepts many file types. Append \"gif\", \"png\" etc. to the end of the `/meme add` request." + "\n"
                         + "- Whois command much improved" + "\n"
                         + "- Moderation commands added (ban etc.)" + "\n"
                         + "- Ban reasons appear on Audit Log" + "\n"
                         + "- Guild commands added (info, approval)" + "\n"
                         + "- Help command redesigned from scratch",
                IsInline = false
            };
            emRelNotes.AddField(emRelNotesFeatures);

            var emRelNotesBugs = new EmbedFieldBuilder
            {
                Name = "Bugs Squashed",
                Value = " - No bugs found. Report a bug in the support server, or email botsupport@marens101.com",
                IsInline = false
            };
            //emRelNotes.AddField(emRelNotesBugs);

            var emRelNotesComingSoon = new EmbedFieldBuilder
            {
                Name = "Coming Soon",
                Value = " - Voting with Reactions" + "\n"
                         + "- Kicks (Waiting for release from Discord.NET)" + "\n"
                         + "- Music!!!",
                IsInline = false
            };
            emRelNotes.AddField(emRelNotesComingSoon);

            await Chan.SendMessageAsync("", false, emRelNotes);
        }
    }
}
