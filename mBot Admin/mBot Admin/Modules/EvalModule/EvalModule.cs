﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Audio;
using Discord.Addons.Interactive;
using Discord.Webhook;
using Microsoft.CodeAnalysis.CSharp.Scripting;
using Microsoft.CodeAnalysis.Scripting;

namespace mBot_Admin.Modules.EvalModule
{
    public class EvalModule : ModuleBase
    {
        [Command("eval")]
        [RequireOwner]
        public async Task Eval([Remainder] string script)
        {
            script.Trim();
            
            var globals = new Globals
            {
                Context = Context
            };
            ScriptOptions options = ScriptOptions.Default;
            options.AddImports("mBot_Admin", "Discord", "Discord.Net", "Discord.Commands");
            var evalTask = await CSharpScript.EvaluateAsync(script, globals: globals, options: options);
            await ReplyAsync(evalTask.ToString());
        }


        public class Globals
        {
            public ICommandContext Context;
            public mBot_Admin._System.ErrorHandling.ErrorEvent ErrorEvent;
        }
    }

}
