﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using mBot_Admin.Preconditions.UserPermissionPreconditions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static mBot_Admin._System.ErrorHandling;

namespace mBot_Admin.Modules.GuildModule
{
    [Group("guild")]
    [Alias("server", "srv", "g", "s")]
    [RequireContext(ContextType.Guild)]
    public class GuildModule : ModuleBase //<SocketCommandContext>
    {
        private DiscordSocketClient client;

        [Group("has")]
        public class GuildHasModule : ModuleBase
        {

            [Command("ManageGuild")]
            [Alias("InviteBot", "MG")]
            public async Task ManageGuild(IGuildUser user = null)
            {
                if (user == null)
                {
                    var irousers = await Context.Guild.GetUsersAsync();
                    //List<IGuildUser> list = new List<IGuildUser>();
                    Dictionary<ulong, string> dict = new Dictionary<ulong, string>();
                    foreach (IGuildUser u in irousers)
                    {
                        if (u.GuildPermissions.ManageGuild)
                        {
                            //list.Add(u);
                            dict.Add(u.Id, $"{u.Username}#{u.Discriminator}");
                        }
                    }
                    string send = "";
                    foreach (KeyValuePair<ulong, string> i in dict.OrderBy(i => i.Value))
                    {
                        send = send + "\n" + "{" + i.Value + ":" + i.Key.ToString() + "}";
                    }
                    send.Trim();
                    await ReplyAsync(send);
                }
                else
                {
                    if (user.GuildPermissions.ManageGuild)
                    {
                        await ReplyAsync("True");
                    }
                    else
                    {
                        await ReplyAsync("True");
                    }
                }
            }

            [Command("Ban")]
            public async Task Ban(IGuildUser user = null)
            {
                if (user == null)
                {
                    var irousers = await Context.Guild.GetUsersAsync();
                    //List<IGuildUser> list = new List<IGuildUser>();
                    Dictionary<ulong, string> dict = new Dictionary<ulong, string>();
                    foreach (IGuildUser u in irousers)
                    {
                        if (u.GuildPermissions.BanMembers)
                        {
                            //list.Add(u);
                            dict.Add(u.Id, $"{u.Username}#{u.Discriminator}");
                        }
                    }
                    string send = "";
                    foreach (KeyValuePair<ulong, string> i in dict.OrderBy(i => i.Value))
                    {
                        send = send + "\n" + "{" + i.Value + ":" + i.Key.ToString() + "}";
                    }
                    send.Trim();
                    await ReplyAsync(send);
                }
                else
                {
                    if (user.GuildPermissions.BanMembers)
                    {
                        await ReplyAsync("True");
                    }
                    else
                    {
                        await ReplyAsync("True");
                    }
                }
            }

            [Command("Kick")]
            public async Task Kick(IGuildUser user = null)
            {
                if (user == null)
                {
                    var irousers = await Context.Guild.GetUsersAsync();
                    //List<IGuildUser> list = new List<IGuildUser>();
                    Dictionary<ulong, string> dict = new Dictionary<ulong, string>();
                    foreach (IGuildUser u in irousers)
                    {
                        if (u.GuildPermissions.KickMembers)
                        {
                            //list.Add(u);
                            dict.Add(u.Id, $"{u.Username}#{u.Discriminator}");
                        }
                    }
                    string send = "";
                    foreach (KeyValuePair<ulong, string> i in dict.OrderBy(i => i.Value))
                    {
                        send = send + "\n" + "{" + i.Value + ":" + i.Key.ToString() + "}";
                    }
                    send.Trim();
                    await ReplyAsync(send);
                }
                else
                {
                    if (user.GuildPermissions.KickMembers)
                    {
                        await ReplyAsync("True");
                    }
                    else
                    {
                        await ReplyAsync("True");
                    }
                }
            }

            [Command("Administrator")]
            [Alias("InviteMBot", "A")]
            public async Task Administrator(IGuildUser user = null)
            {
                if (user == null)
                {
                    var irousers = await Context.Guild.GetUsersAsync();
                    //List<IGuildUser> list = new List<IGuildUser>();
                    Dictionary<ulong, string> dict = new Dictionary<ulong, string>();
                    foreach (IGuildUser u in irousers)
                    {
                        if (u.GuildPermissions.Administrator)
                        {
                            //list.Add(u);
                            dict.Add(u.Id, $"{u.Username}#{u.Discriminator}");
                        }
                    }
                    string send = "";
                    foreach (KeyValuePair<ulong, string> i in dict.OrderBy(i => i.Value))
                    {
                        send = send + "\n" + "{" + i.Value + ":" + i.Key.ToString() + "}";
                    }
                    send.Trim();
                    await ReplyAsync(send);
                }
                else
                {
                    if (user.GuildPermissions.Administrator)
                    {
                        await ReplyAsync("True");
                    }
                    else
                    {
                        await ReplyAsync("True");
                    }
                }
            }
        }

        [Command("newchan")]
        [RequireManageChannels]
        public async Task SrvNewChan(string chantype, string channame)
        {
            await Context.Channel.TriggerTypingAsync();

            var application = await Context.Client.GetApplicationInfoAsync();
            var iguser = Context.User as IGuildUser;
            if (iguser.Id == application.Owner.Id)
            {
                Console.WriteLine($"Permission Test Passed for {Context.User.Username}#{Context.User.Discriminator} with Perm ManageGuild as Bot owner");
            }
            else if (iguser.GuildPermissions.ManageChannels)
            {
                Console.WriteLine($"Permission Test Passed for {Context.User.Username}#{Context.User.Discriminator} with Perm ManageGuild");
            }
            else
            {
                throw new System.Exception("User requires guild permission ManageChanels");
            }

            if (chantype.ToUpper() == "TEXT" || chantype.ToUpper() == "TXT" || chantype.ToUpper() == "T")
            {
                var x = await Context.Guild.CreateTextChannelAsync(channame, new RequestOptions
                {
                    AuditLogReason = $"Create Text Channel Requested by {Context.User.Username}#{Context.User.Discriminator}"
                });
                await ReplyAsync("<:ok1:331993694867554305>" + $" Success. Channel {x.Mention} created");
            }
            else if (chantype.ToUpper() == "VOICE" || chantype.ToUpper() == "V")
            {
                var x = await Context.Guild.CreateVoiceChannelAsync(channame, new RequestOptions
                {
                    AuditLogReason = $"Create Voice Channel Requested by {Context.User.Username}#{Context.User.Discriminator}"
                });
                await ReplyAsync("<:ok1:331993694867554305>" + $" Success. Channel {x.Name} created");
            }
            else
            {
                throw new System.Exception("Invalid Channel Type");
            }
        }

        [Command("invite")]
        public async Task Invite(int age = 1800, int uses = 100)
        {
            ulong defchanid = Context.Guild.DefaultChannelId;
            var defchan = await Context.Guild.GetTextChannelAsync(defchanid);
            var invite = await defchan.CreateInviteAsync(isUnique: true, maxUses: uses, maxAge: age * 60);
            await ReplyAsync(invite.Url + "\n"
                                + $"T&C: " + "\n"
                                + $"Max Uses: {invite.MaxUses}" + "\n"
                                + $"Time Until Expiry: {invite.MaxAge / 60}min" + "\n");
        }

        [Command("lookupinvite")]
        [RequireOwner]
        public async Task Invite(string code)
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var invites = await Context.Guild.GetInvitesAsync();

            IInviteMetadata invite = null;
            foreach (IInviteMetadata i in invites)
            {
                if (i.Code == code || i.Url == code)
                {
                    invite = i;
                }
            }

            if (invite == null)
            {
                var ii = await Context.Client.GetInviteAsync(code);

                if (ii == null)
                    throw new System.Exception("Invite Not Found");

                EmbedFieldBuilder efbGuild = new EmbedFieldBuilder
                {
                    IsInline = true,
                    Name = $"Invite Server",
                    Value = $"{ii.GuildName}"
                };
                EmbedFieldBuilder efbChan = new EmbedFieldBuilder
                {
                    IsInline = true,
                    Name = $"Invite Channel",
                    Value = $"{ii.ChannelName}"
                };

                List<EmbedFieldBuilder> list = new List<EmbedFieldBuilder>
                {
                efbGuild,
                efbChan
                };

                Embed em = new EmbedBuilder
                {
                    Color = new Discord.Color(4, 97, 247),
                    Author = new EmbedAuthorBuilder
                    {
                        Name = application.Name,
                        IconUrl = application.IconUrl,
                        Url = "http://rb.marens101.com/m101bot"
                    },
                    Title = "Invite Lookup",
                    ThumbnailUrl = (Context.Guild.IconUrl ?? "https://fakeimg.pl/800x800/?text=Server%20Icon%0A%20%20%20%20Not%20Set")
                };
                await ReplyAsync("", embed: em);

            }
            else
            {
                string expiry = InviteTimeToExpiry(invite);

                EmbedFieldBuilder efbTTE = new EmbedFieldBuilder
                {
                    IsInline = true,
                    Name = $"Time To Expiry",
                    Value = expiry
                };


                EmbedFieldBuilder efbUses = new EmbedFieldBuilder
                {
                    IsInline = true,
                    Name = $"Uses",
                    Value = $""
                };

                EmbedFieldBuilder efbChan = new EmbedFieldBuilder
                {
                    IsInline = true,
                    Name = $"Invite Channel",
                    Value = $"{invite.Channel.Name}"
                };

                List<EmbedFieldBuilder> list = new List<EmbedFieldBuilder>
            {
                efbTTE,
                efbUses,
                efbChan,

            };

                Embed em = new EmbedBuilder
                {
                    Color = new Discord.Color(4, 97, 247),
                    Author = new EmbedAuthorBuilder
                    {
                        Name = application.Name,
                        IconUrl = application.IconUrl,
                        Url = "http://rb.marens101.com/m101bot"
                    },
                    Title = "Invite Lookup",
                    ThumbnailUrl = (Context.Guild.IconUrl ?? "https://fakeimg.pl/800x800/?text=Server%20Icon%0A%20%20%20%20Not%20Set")
                };
                await ReplyAsync("", embed: em);
            }



        }
        static string InviteTimeToExpiry(IInviteMetadata invite)
        {
            if (invite.MaxAge == null)
                return "Never";
            var time = invite.CreatedAt.AddSeconds(Convert.ToDouble(invite.MaxAge)).Subtract(DateTime.Now);
            string str = $"Days: {time.Days}" + "\n"
                            + $"Hours: {time.Hours}" + "\n"
                            + $"Minutes: {time.Minutes}" + "\n";

            return str;
        }
        static string InviteUses(IInviteMetadata invite)
        {
            if (invite.MaxUses == null)
                return "No Limit";
            var time = invite.CreatedAt.AddSeconds(Convert.ToDouble(invite.MaxAge)).Subtract(DateTime.Now);
            string str = $"Used {invite.Uses} times out of {invite.MaxUses} ({(invite.Uses / invite.MaxUses * 100)}% used).";

            return str;
        }

        [Command]
        [Alias("info")]
        public async Task SrvInfo()
        {
            await Context.Channel.TriggerTypingAsync();
            var application = await Context.Client.GetApplicationInfoAsync();
            var ebGuildInfo = new EmbedBuilder()
            {
                Color = new Discord.Color(4, 97, 247),
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl,
                    Url = "http://rb.marens101.com/m101bot"
                },
                Title = "Guild Info",
                ThumbnailUrl = (Context.Guild.IconUrl ?? "https://fakeimg.pl/800x800/?text=Server%20Icon%0A%20%20%20%20Not%20Set"),
            };

            ulong defchanid = Context.Guild.DefaultChannelId;
            var defchan = await Context.Guild.GetTextChannelAsync(defchanid);
            var invite = await defchan.CreateInviteAsync(isUnique: true, maxUses: 5);
            var efbInviteLink = new EmbedFieldBuilder
            {
                IsInline = true,
                Name = "Invite Link",
                Value = invite.Url + "\n"
                         + $"__**Note**__: Invite Expires In: " + "\n"
                         + $" - {DateTime.Now.AddSeconds(Convert.ToDouble(invite.MaxAge)).Subtract(DateTime.Now).TotalMinutes} Minutes" + "\n"
                         + $" - {invite.MaxUses} uses."
            };
            ebGuildInfo.AddField(efbInviteLink);

            var txtchans = await Context.Guild.GetTextChannelsAsync();
            int txtchanscnt = txtchans.ToArray().Length;
            var vchans = await Context.Guild.GetVoiceChannelsAsync();
            int vchanscnt = vchans.ToArray().Length;

            var efbChansCount = new EmbedFieldBuilder
            {
                IsInline = true,
                Name = "Channels",
                Value = $"Text Channels: {txtchanscnt}" + "\n" +
                            $"Voice Channels: {vchanscnt}"
            };
            ebGuildInfo.AddField(efbChansCount);

            string txtchanlist = "";

            foreach (ITextChannel x in txtchans)
            {
                txtchanlist = txtchanlist + "\n" + x.Name;
            };

            var efbTxtChans = new EmbedFieldBuilder
            {
                IsInline = true,
                Name = "Text Channels",
                Value = txtchanlist
            };
            ebGuildInfo.AddField(efbTxtChans);


            string vchanlist = "";

            foreach (IVoiceChannel x in vchans)
            {
                vchanlist = vchanlist + "\n" + x.Name;
            };

            var efbVoiceChans = new EmbedFieldBuilder
            {
                IsInline = true,
                Name = "Voice Channels",
                Value = vchanlist
            };
            ebGuildInfo.AddField(efbVoiceChans);

            var users = await Context.Guild.GetUsersAsync();
            int usersint = users.Count();

            var efbUsers = new EmbedFieldBuilder
            {
                IsInline = true,
                Name = "Total Users",
                Value = usersint.ToString()
            };
            ebGuildInfo.AddField(efbUsers);

            int OnlineUsers = 0;
            int AFKUsers = 0;
            int DNDUsers = 0;
            int IdleUsers = 0;
            int InvisUsers = 0;
            int OfflineUsers = 0;

            foreach (IGuildUser i in users)
            {
                if (i.Status == UserStatus.Online)
                    OnlineUsers = OnlineUsers + 1;
                else if (i.Status == UserStatus.AFK)
                    AFKUsers = AFKUsers + 1;
                else if (i.Status == UserStatus.DoNotDisturb)
                    DNDUsers = DNDUsers + 1;
                else if (i.Status == UserStatus.Idle)
                    IdleUsers = IdleUsers + 1;
                else if (i.Status == UserStatus.Invisible)
                    InvisUsers = InvisUsers + 1;
                else if (i.Status == UserStatus.Offline)
                    OfflineUsers = OfflineUsers + 1;
            }

            var efbUserStatus = new EmbedFieldBuilder
            {
                IsInline = true,
                Name = "Users Status",
                Value = $"Online Users: {OnlineUsers}" + "\n" +
                            $"Idle/AFK: {IdleUsers + AFKUsers}" + "\n" +
                            //$"AFK: {AFKUsers}" + "\n" +
                            $"Do Not Disturb: {DNDUsers}" + "\n" +
                            $"Offline/Invisible Users: {OfflineUsers + InvisUsers}"// + "\n" +
                                                                                   //$"Invisible Users: {InvisUsers}"
            };
            ebGuildInfo.AddField(efbUserStatus);

            await ReplyAsync("", embed: ebGuildInfo);
        }

        [Command("giveeveryone")]
        [Alias("ge", "granteveryone")]
        [RequireOwner]
        public async Task grantEveryone(ulong rid)
        {
            var role = Context.Guild.GetRole(rid);
            if (role == null)
                throw new System.Exception("Role Not Found");
            var usrs = await Context.Guild.GetUsersAsync();
            foreach (IGuildUser i in usrs)
            {
                await i.AddRoleAsync(role);
            }
            await ReplyAsync("<:ok1:331993694867554305>" + " Complete");
        }

        [Command("give")]
        [Alias("g", "grant")]
        [RequireManageRoles]
        public async Task grant(IGuildUser user, ulong rid)
        {
            var role = Context.Guild.GetRole(rid);
            if (role == null)
                throw new System.Exception("Role Not Found");
            await user.AddRoleAsync(role);
            await ReplyAsync("<:ok1:331993694867554305>" + " Complete");
        }

        [Command("take")]
        [Alias("rem", "remove")]
        [RequireManageRoles]
        public async Task Remove(IGuildUser user, ulong rid)
        {
            var role = Context.Guild.GetRole(rid);
            if (role == null)
                throw new System.Exception("Role Not Found");
            await user.RemoveRoleAsync(role);
            await ReplyAsync("<:ok1:331993694867554305>" + " Complete");
        }

        [Command("cr")]
        [Alias("createrole", "crole")]
        [RequireManageRoles]
        public async Task createRole(string name, bool hoist = false, ulong permVal = 0)
        {
            GuildPermissions perms = new GuildPermissions(permVal);

            var role = await Context.Guild.CreateRoleAsync(name, perms, isHoisted: hoist);
            var application = await Context.Client.GetApplicationInfoAsync();
            var emCreateRole = new EmbedBuilder
            {
                Author = new EmbedAuthorBuilder
                {
                    Name = application.Name,
                    IconUrl = application.IconUrl
                },
                Title = $"Role Created By: {Context.User.Username}#{Context.User.Discriminator}",
                Color = role.Color,

            };

            EmbedFieldBuilder efbName = new EmbedFieldBuilder
            {
                Name = "Name",
                Value = role.Name,
                IsInline = true
            };
            emCreateRole.AddField(efbName);

            EmbedFieldBuilder efbID = new EmbedFieldBuilder
            {
                Name = "ID",
                Value = role.Id,
                IsInline = true
            };
            emCreateRole.AddField(efbID);

            EmbedFieldBuilder efbColour = new EmbedFieldBuilder
            {
                Name = "Colour",
                Value = $"RGB: {role.Color.R}, {role.Color.G}, {role.Color.B}",
                IsInline = true
            };
            emCreateRole.AddField(efbColour);

            EmbedFieldBuilder efbHoisted = new EmbedFieldBuilder
            {
                Name = "Shows seperate from other users in the users list?",
                Value = role.IsHoisted,
                IsInline = true
            };
            emCreateRole.AddField(efbHoisted);

            EmbedFieldBuilder efbMentionable = new EmbedFieldBuilder
            {
                Name = "Is Mentionable?",
                Value = role.IsMentionable,
                IsInline = true
            };
            emCreateRole.AddField(efbMentionable);

            EmbedFieldBuilder efbPos = new EmbedFieldBuilder
            {
                Name = "Position",
                Value = role.Position.ToString(),
                IsInline = true
            };
            emCreateRole.AddField(efbPos);

            await ReplyAsync("<:ok1:331993694867554305>" + $" Complete: {role.Mention} ({role.Id})", embed: emCreateRole);
        }

        [Command("approve")]
        [RequireContext(ContextType.Guild)]
        [RequireManageRoles]
        public async Task guildApprove(IGuildUser user)
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var iguser = Context.User as IGuildUser;
            try
            {
                var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(
                    File.ReadAllText($@"BotSystem\Data\Srv\adminApprove\id.dict"));
                /*if (!dictionary.ContainsKey($"{Context.Guild.Id}")) ;
                {
                    throw new System.Exception("Approved Role Not Set");
                }*/
                string iid = dictionary[$"{Context.Guild.Id}"];
                ulong id = Convert.ToUInt64(iid);
                var role = Context.Guild.GetRole(id);
                await user.AddRoleAsync(role);
                await ReplyAsync("<:ok1:331993694867554305>" + "\n" +
                                    $"{user.Mention} has been approved.");
            }
            catch (System.Exception e)
            {
                if (e.Message == "Approved Role Not Set" || e.Message == "The server responded with error 403: Forbidden")
                {
                    await ReplyAsync(e.Message);
                }
                else
                {
                    await ReplyAsync("Error");
                }
                Console.WriteLine(e.Message);
                Rollbar.Report(e);
            }

        }

        [Command("setapprove")]
        [RequireContext(ContextType.Guild)]
        [RequireManageGuild]
        public async Task guildSetApprove(ulong id)
        {
            var application = await Context.Client.GetApplicationInfoAsync();
            var role = Context.Guild.GetRole(id);
            if (!File.Exists($@"BotSystem\Data\Srv\adminApprove\id.dict"))
            {
                var td = new Dictionary<string, string>();
                File.WriteAllText($@"BotSystem\Data\Srv\adminApprove\id.dict", JsonConvert.SerializeObject(td));
            }
            var dictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(
                File.ReadAllText($@"BotSystem\Data\Srv\adminApprove\id.dict"));
            dictionary.Add($"{Context.Guild.Id}", $"{id}");
            File.WriteAllText($@"BotSystem\Data\Srv\adminApprove\id.dict", JsonConvert.SerializeObject(dictionary));
            var msg = await ReplyAsync("<:ok1:331993694867554305>" + $" Approved role set to {role.Mention}");
            try
            {
                if (!((Context.Client.CurrentUser as IGuildUser).GuildPermissions.UseExternalEmojis))
                {
                    await msg.DeleteAsync();
                    await ReplyAsync(":white_check_mark:" + $" Approved role set to {role.Mention}");
                }
            }
            catch (System.Exception e)
            {
                Rollbar.Report(e);
                Console.WriteLine("Error");
            }

        }
    }
}
