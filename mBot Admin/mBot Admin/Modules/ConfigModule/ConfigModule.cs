﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;
using Newtonsoft.Json;

namespace mBot_Admin.Modules.ConfigModule
{
    public class Config
    {
        public Config(bool load = true)
        {
            /*if (!(File.Exists(@"BotSystem\Sys\config.json")))
            {
                Config tconf = new Config();
                tconf.environment = "dev";
                tconf.version = new ConfigObjects.Version();
                string tjson = JsonConvert.SerializeObject(tconf);
                File.WriteAllText(@"BotSystem\Sys\config.json", tjson);
            }*/

            string json = File.ReadAllText(@"BotSystem\Sys\config.json");
            
            Config conf = JsonConvert.DeserializeObject<Config>(json);
            this.MashapeKey = conf.MashapeKey;
            this.version = conf.version;
            this.environment = conf.environment;
        }



        public ConfigObjects.Version version { get; set; }
        public string environment { get; set; }
        public string MashapeKey { get; set; }


    }

    public class ConfigObjects
    {
        public class Version
        {
            int major = 0;
            int minor = 7;
            int build = 0;
            int revision = 0;
            string note = "";
            string str 
            {
                get { return GetString(); }
            }

            private string GetString()
            {
                string str = $"{major}.{minor}";
                if (tru())
                {
                    str = $"{str}.{build}";
                    if (tru2())
                    {
                        str = $"{str}.{revision}";
                    }
                }
                if (note != "")
                    str = str + " " + note;

                return str;
            }

            private bool tru()
            {
                if (build != 0)
                    return true;
                else if (revision != 0)
                    return true;
                else
                    return false;
            }

            private bool tru2()
            {
                if (revision != 0)
                    return true;
                else
                    return false;
            }
        }
    }
    


}
