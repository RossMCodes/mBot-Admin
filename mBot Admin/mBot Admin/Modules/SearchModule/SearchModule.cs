﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.Net;
using Discord.Rest;
using Discord.Rpc;
using mBot_Admin.Modules.ConfigModule;
using mBot_Admin.Modules.SystemModule;
using mBot_Admin.Objects;
using mBot_Admin.Services;
using mBot_Admin.Templates;
using Newtonsoft.Json;
using RollbarDotNet;
using SharpRaven;
using mBot_Admin.Services.SearchService;

namespace mBot_Admin.Modules.SearchModule
{
    public class SearchModule : ModuleBase
    {
        [Command("Urban")]
        public async Task UrbanDict(string search)
        {
            await new UrbanService().UrbanSearch(Context, search);
        }
    }
}
